import 'package:car_user_project/preferences/student_pref.dart';
import 'package:car_user_project/screens/ads/add_ads.dart';
import 'package:car_user_project/screens/ads/ads_details.dart';
import 'package:car_user_project/screens/ads/show_ads.dart';
import 'package:car_user_project/screens/auth/agreement_user_screen.dart';
import 'package:car_user_project/screens/auth/change_password.dart';
import 'package:car_user_project/screens/controller_panel/car/add_car.dart';
import 'package:car_user_project/screens/controller_panel/controlle_panel.dart';
import 'package:car_user_project/screens/controller_panel/home_control/home_controller.dart';
import 'package:car_user_project/screens/controller_panel/home_control/order_details.dart';
import 'package:car_user_project/screens/controller_panel/home_control/order_home.dart';
import 'package:car_user_project/screens/controller_panel/manger/mangers.dart';
import 'package:car_user_project/screens/controller_panel/subscribe/subscribe_home.dart';
import 'package:car_user_project/screens/home/chat/chat_detailes_screen.dart';
import 'package:car_user_project/screens/home/details/detials_screen.dart';
import 'package:car_user_project/screens/home/details/shop_detials.dart';
import 'package:car_user_project/screens/home/merchant_adders/east.dart';
import 'package:car_user_project/screens/home/merchant_adders/north.dart';
import 'package:car_user_project/screens/home/merchant_adders/south.dart';
import 'package:car_user_project/screens/home/merchant_adders/west.dart';
import 'package:car_user_project/screens/home/more/more_adders.dart';
import 'package:car_user_project/screens/home/more/more_car.dart';
import 'package:car_user_project/screens/home/more/more_marechent.dart';
import 'package:car_user_project/screens/home/profile/setting_screen.dart';
import 'package:car_user_project/screens/home/search/search_screen.dart';
import 'package:car_user_project/screens/home/user_main_screen.dart';
import 'package:car_user_project/screens/launch/launch_screen.dart';
import 'package:car_user_project/screens/login/login_screen.dart';
import 'package:car_user_project/screens/map_screen.dart';
import 'package:car_user_project/screens/sign_up/select_sign.dart';
import 'package:car_user_project/screens/sign_up/sign_up_merchant.dart';
import 'package:car_user_project/screens/sign_up/sign_up_user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:flutter/material.dart';
import 'package:flutter_localizations/flutter_localizations.dart';

void main() async{
  WidgetsFlutterBinding.ensureInitialized();
  await AppPreferences().initPreferences();
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  runApp(MyApp());
}
class MyApp extends StatefulWidget {
  @override
  _MyAppState createState() => _MyAppState();
}

class _MyAppState extends State<MyApp> {
  @override
  Widget build(BuildContext context) {
    Locale _locale = Locale('ar');

    return MaterialApp(
      localizationsDelegates: [
        GlobalMaterialLocalizations.delegate,
        GlobalCupertinoLocalizations.delegate,
        GlobalWidgetsLocalizations.delegate,
      ],
      supportedLocales: [
        Locale('ar'),
      ],
      debugShowCheckedModeBanner: false,
      initialRoute: 'launch_screen',
      // home: ExpireSubscribe(),
      routes: {
        'launch_screen': (context) => LaunchScreen(),
        'login_screen': (context) => LoginScreen(),
        'map_screen': (context) => MapScreen(),
        'sign_up_merchant': (context) => SignUpMerchant(),
        'sign_up_user': (context) => SignUpUser(),
        'select_sign': (context) => SelectSign(),
        // 'agreement_user_screen': (context) => AgreementUserScreen(),
        'user_main_screen': (context) => UserMainScreen(),
        //'change_password_screen': (context) => ChangePasswordScreen(),
        // 'home_controller_screen': (context) => HomeController(),
        // 'controller_select': (context) => ControllerSelect(),
        'change_password_screen': (context) => ChangePasswordScreen(),
        'ads_home_screen': (context) => AdsHomeScreen(),
        'search_screen': (context) => SearchScreen(),
        'order_home': (context) => OrderHome(),
        // 'ads_details': (context) => AdsDetailsScreen(),
        'more_car': (context) => MoreCar(),
        'more_marechent': (context) => MoreMarechent(),
        'more_adders_Screenn': (context) => MoreAdders(),
        'south_screen': (context) => South(),
        'north_screen': (context) => North(),
        'west_screen': (context) => West(),
        'east_screen': (context) => East(),
        // 'subscribe_screen': (context) => SubscribeScreen(),
        'manger_screen': (context) => MangerScreen(),
        // 'order_details': (context) => OrderDetails(),
        // 'chat_screen_details': (context) => ChatScreenDetails(),
        // 'add_ads_screen': (context) => AddAdsScreen(),
        // 'details_shop_screen': (context) => DetailsShopScreen(),
        // 'details_screen': (context) => DetailsCarScreen(),
        // 'add_car_screen': (context) => AddCarScreen(),
      },
    );  }
}

