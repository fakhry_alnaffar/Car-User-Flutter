import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/favorite.dart';
import 'package:car_user_project/screens/home/details/shop_detials.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
class MoreAdders extends StatefulWidget {
  const MoreAdders({Key? key}) : super(key: key);

  @override
  _MoreAddersState createState() => _MoreAddersState();
}

class _MoreAddersState extends State<MoreAdders> {
  List<String> addresses = <String>[
    'جنوب غزة',
    'شمال غزة',
    'غرب غزة',
    'شرق غزة'
  ];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: Color(0xff1DB854),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            'المحافظات',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
        ),
        backgroundColor: Colors.white,

        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15,
                ),
                Expanded(
                  child: GridView.builder(
                    scrollDirection: Axis.vertical,
                    physics: BouncingScrollPhysics(),
                    clipBehavior: Clip.antiAlias,
                    itemCount: addresses.length,
                    gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                      crossAxisCount: 2,
                      mainAxisSpacing: 5,
                    ),
                    itemBuilder: (context, index) {
                      return GestureDetector(
                        onTap: () {
                          if(index==0){
                            print('south');
                            Navigator.pushNamed(context, 'south_screen');
                          } if(index==1){
                            Navigator.pushNamed(context, 'north_screen');
                            print('north');
                          } if(index==2){
                            Navigator.pushNamed(context, 'west_screen');
                            print('west');
                          } if(index==3){
                            Navigator.pushNamed(context, 'east_screen');
                            print('east');
                          }
// Navigator.push(
//     context,
//     MaterialPageRoute(
//         builder: (context) => ContentScreen(
//             FirebaseFirestoreController().readProductByCategory(documents[index].id))));
                        },
                        child: showItemHomeAdders(
                          name: addresses[index],
                        ),
                      );
                    },
//         );
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

