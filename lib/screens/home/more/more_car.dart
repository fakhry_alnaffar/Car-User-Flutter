import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/car.dart';
import 'package:car_user_project/models/favorite.dart';
import 'package:car_user_project/screens/home/details/detials_screen.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
class MoreCar extends StatefulWidget {
  const MoreCar({Key? key}) : super(key: key);

  @override
  _MoreCarState createState() => _MoreCarState();
}

class _MoreCarState extends State<MoreCar> {
  String? type;
  String? model;
  String? state;
  double? price;
  String carId = '';
  String id = '';
  double? price2;
  List<bool> favorite = <bool>[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: Color(0xff1DB854),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            'السيارات',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
        ),
        backgroundColor: Colors.white,

        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15,
                ),
                StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestoreController().readCar(),
                    builder: (context, snapshot) {
                      if(snapshot.hasData && snapshot.data!.docs.length > 0){
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        return Expanded(
                          child: GridView.builder(
                            scrollDirection: Axis.vertical,
                            physics: BouncingScrollPhysics(),
                            clipBehavior: Clip.antiAlias,
                            itemCount: documents.length,
                            gridDelegate:
                            SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisSpacing: 5,
                            ),
                            itemBuilder: (context, index) {
                              // for(int i = 0; i < documents.length; i++){
                              //
                              // }
                              // ids.add(documents[index].id);
                              carId = documents[index].id;
                              isFavorite();
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DetailsCarScreen(
                                              getCar(documents[index]))));
                                },
                                child: Card(
                                  color: Colors.white,
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  clipBehavior: Clip.antiAlias,
                                  child: Column(
                                    children: [
                                      Container(
                                        width: double.infinity,
                                        height: 120,
                                        child: Image.network(
                                          List.from(
                                              documents[index].get('images'))[0],
                                          fit: BoxFit.cover,
                                          height: double.infinity,
                                          width: double.infinity,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsetsDirectional.only(
                                            start: 10, end: 5, top: 0, bottom: 0),
                                        child: Container(
                                          color: Colors.white,
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Column(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    textBaseline:
                                                    TextBaseline.ideographic,
                                                    children: [
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Text(
                                                        documents[index]
                                                            .get('name'),
                                                        maxLines: 1,
                                                        overflow:
                                                        TextOverflow.ellipsis,
                                                        style: TextStyle(
                                                          fontSize: 17,
                                                          fontWeight:
                                                          FontWeight.bold,
                                                          textBaseline:
                                                          TextBaseline
                                                              .ideographic,
                                                        ),
                                                      ),
                                                      Text(
                                                        '${documents[index].get('price').toString()}\$',
                                                        style: TextStyle(
                                                            color:
                                                            Color(0xff026b28),
                                                            fontSize: 14,
                                                            fontWeight:
                                                            FontWeight.bold),
                                                      ),
                                                    ],
                                                  ),
                                                  Spacer(),
                                                  // IconButton(
                                                  //   onPressed: () async {
                                                  //     if (favorite[index]) {
                                                  //       await getFavoriteId();
                                                  //     }
                                                  //     print(index);
                                                  //     await favoriteProses(
                                                  //         documents[index],
                                                  //         index);
                                                  //   },
                                                  //   icon: favorite[index] == true
                                                  //       ? Icon(
                                                  //     Icons.favorite,
                                                  //     size: 23,
                                                  //     color: Colors.red,
                                                  //   )
                                                  //       : Icon(
                                                  //     Icons.favorite_border,
                                                  //     size: 23,
                                                  //     color: Colors.red,
                                                  //   ),
                                                  //   padding: EdgeInsetsDirectional
                                                  //       .zero,
                                                  //   alignment:
                                                  //   AlignmentDirectional
                                                  //       .centerEnd,
                                                  // )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                            //         );
                          ),
                        );
                      }else if(snapshot.connectionState == ConnectionState.waiting){
                        return Loading();
                      }else{
                        return NoData();
                      }

                    }
                ),
              ],
            ),
          ),
        ));
  }

  Car getCar(DocumentSnapshot snapshot) {
    Car car = Car();
    car.images = List.from(snapshot.get('images'));
    car.name = snapshot.get('name');
    car.price = snapshot.get('price');
    car.description = snapshot.get('description');
    car.model = snapshot.get('model');
    car.state = snapshot.get('state');
    car.path = snapshot.id;
    car.color = List.from(snapshot.get('color'));
    car.merchant = snapshot.get('merchant');
    return car;
  }
  Future<void> isFavorite() async {
    bool state = await FirebaseFirestoreController().isFavorite(carId);
    favorite.add(state);
  }

  Future<void> addFavorite(DocumentSnapshot snapshot, int index) async {
    bool state = await FirebaseFirestoreController()
        .addFavorite(setCarFavorite(snapshot));
    if (state) {
      setState(() {
        favorite[index] = true;
      });
    }
  }
  Favorite setCarFavorite(DocumentSnapshot snapshot) {
    Favorite favorite = Favorite();
    favorite.name = snapshot.get('name');
    favorite.merchant = snapshot.get('merchant');
    favorite.color = List.from(snapshot.get('color'));
    favorite.state = snapshot.get('state');
    favorite.price = snapshot.get('price');
    favorite.description = snapshot.get('description');
    favorite.model = snapshot.get('model');
    favorite.images = List.from(snapshot.get('images'));
    favorite.email = FirebaseAuth.instance.currentUser!.email!;
    favorite.carId = snapshot.id;
    return favorite;
  }

  Future<void> deleteFavorite(int index) async {
    print('id = ' + id);
    bool state = await FirebaseFirestoreController().deleteFavorite(path: id);
    if (state) {
      setState(() {
        favorite[index] = false;
      });
    }
  }

  Future<void> getFavoriteId() async {
    print(carId);
    String state = await FirebaseFirestoreController().getFavoriteId(carId);
    setState(() {
      id = state;
    });
  }

  Future<void> favoriteProses(DocumentSnapshot snapshot, int index) async {
    if (favorite[index]) {
      await deleteFavorite(index);
    } else {
      await addFavorite(snapshot, index);
    }
  }
}
