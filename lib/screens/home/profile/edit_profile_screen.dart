import 'dart:io';

import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/firebase/firebase_storage.dart';
import 'package:car_user_project/models/merchant.dart';
import 'package:car_user_project/models/user.dart';
import 'package:car_user_project/preferences/student_pref.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/widgets/app_text_filed_profile.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:image_picker/image_picker.dart';

class EditProfileScreen extends StatefulWidget {
 final Users? user;
 final Merchant? merchant;
 final String type;

  EditProfileScreen(this.user, this.merchant, this.type);

  @override
  _EditProfileScreenState createState() => _EditProfileScreenState();
}

class _EditProfileScreenState extends State<EditProfileScreen> {
  XFile? pickedImage;
  XFile? pickedImage2;
  ImagePicker imagePicker = ImagePicker();
  String selectedImage = '';
  String name = '';
  String path = '';
  bool isNameEdit = false;
  bool isShopNameEdit = false;
  bool isCountryEdit = false;
  bool isAddersEdit = false;
  bool isEmailEdit = false;
  bool isPhoneEdit = false;
  bool isImageEdit = false;
  IconData iconEditImage = Icons.edit;
  late TextEditingController _name;
  late TextEditingController _shopName;
  late TextEditingController _country;
  late TextEditingController _adders;
  late TextEditingController _email;
  late TextEditingController _phone;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _name = TextEditingController(text: widget.type == 'user' ? widget.user!.name : widget.merchant!.name);
    if(widget.type == 'merchant'){
      _adders = TextEditingController(text: widget.type == 'merchant' ? widget.merchant!.address : '');
      _shopName = TextEditingController(text: widget.type == 'merchant' ? widget.merchant!.marketName : '');
    }
    _country = TextEditingController(text: widget.type == 'user' ? widget.user!.place : widget.merchant!.place);
    _email = TextEditingController(text: widget.type == 'user' ? widget.user!.email : widget.merchant!.email);
    _phone = TextEditingController(text: widget.type == 'user' ? widget.user!.phoneNumber : widget.merchant!.phoneNumber);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    if(widget.type == 'merchant'){
      _shopName.dispose();
      _adders.dispose();
    }
    _name.dispose();
    _country.dispose();
    _email.dispose();
    _phone.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    if(FirebaseAuth.instance.currentUser != null){
      print('Type is ' + widget.type);
      if(widget.type == 'user'){
        return Scaffold(
          backgroundColor: Color(0xffF1F2F3),
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            leading: IconButton(
              icon: Icon(
                Icons.arrow_back_ios_outlined,
                color: Color(0xff1DB854),
              ),
              onPressed: () {
                Navigator.pop(context);
              },
            ),
            iconTheme: IconThemeData(color: Colors.green),
            title: Container(
              margin: EdgeInsetsDirectional.only(top: 10),
              child: Text(
                'الصفحة الشخصية',
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: SafeArea(
            child: SingleChildScrollView(
              clipBehavior: Clip.antiAlias,
              physics: BouncingScrollPhysics(),
              child: StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestoreController().readUser(widget.type),
                  builder: (context, snapshot) {
                    if(snapshot.hasData && snapshot.data!.docs.length > 0){
                      List<DocumentSnapshot> documents = snapshot.data!.docs;
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(height: 20),
                          Center(
                            child: Stack(
                              alignment: AlignmentDirectional.bottomEnd,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(80),
                                    border:
                                    Border.all(width: 8, color: Colors.white),
                                  ),
                                  child: Container(
                                    clipBehavior: Clip.antiAlias,
                                    width: 80,
                                    height: 80,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: Colors.deepOrange),
                                    child: widget.user != null
                                        ? pickedImage2 != null
                                        ? Image.file(
                                      File(pickedImage2!.path),
                                      fit: BoxFit.cover,
                                    )
                                        : Image.network(
                                      widget.user!.image,
                                      fit: BoxFit.cover,
                                    )
                                        : pickedImage2 != null
                                        ? Image.file(
                                      File(pickedImage2!.path),
                                      fit: BoxFit.cover,
                                    )
                                        : Icon(
                                      Icons.add_photo_alternate_outlined,
                                      color: Colors.white,
                                      size: 50,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsetsDirectional.only(
                                      bottom: 12, end: 5, top: 0, start: 0),
                                  child: Container(
                                    // padding: EdgeInsetsDirectional.only(bottom: 50),
                                    child: CircleAvatar(
                                      backgroundColor: Colors.green,

                                      radius: 13,
                                      child: IconButton(
                                        padding: EdgeInsetsDirectional.zero,
                                        onPressed: () {
                                          isImageEdit = !isImageEdit;
                                          if (isImageEdit == true) {
                                            setState(() {
                                              pickImage();
                                              iconEditImage =
                                                  Icons.check_outlined;
                                              print('go to check');
                                            });
                                          }
                                          if (isImageEdit == false) {
                                            setState(() {
                                              print('back to edit');
                                              iconEditImage = Icons.edit;
                                              ///method update image
                                              updateUserImage(documents[0].id);
                                            });
                                          }
                                        },
                                        icon: Icon(iconEditImage,color: Colors.white,size: 18,),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(20),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 32),
                            child: Column(
                              children: [
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: !isNameEdit,
                                  showCursor: isNameEdit,
                                  controller: _name,
                                  prefix: Icons.person,
                                  suffix: isNameEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    setState(() {
                                      isNameEdit = !isNameEdit;
                                      if (isNameEdit == false) {
                                        updateUserName(documents[0].id);
                                      }
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(10),
                                ),
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: true,
                                  showCursor: isCountryEdit,
                                  controller: _country,
                                  prefix: Icons.location_on_outlined,
                                  suffix: isCountryEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    setState(() {
                                      isCountryEdit = !isCountryEdit;
                                      if (isCountryEdit == true) {
                                        showBottomSheet();
                                      }
                                      if (isCountryEdit == false) {
                                        updateUserPlace(documents[0].id);
                                      }
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(10),
                                ),
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: !isEmailEdit,
                                  showCursor: isEmailEdit,
                                  controller: _email,
                                  prefix: Icons.email_outlined,
                                  suffix: isEmailEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    // setState(() {
                                    //   isNameEdit = !isNameEdit;
                                    //   if (isNameEdit == true) {
                                    //     ///update name method
                                    //   }
                                    // });
                                  },
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(10),
                                ),
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: !isPhoneEdit,
                                  showCursor: isPhoneEdit,
                                  controller: _phone,
                                  prefix: Icons.phone_android_rounded,
                                  suffix: isPhoneEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    setState(() {
                                      isPhoneEdit = !isPhoneEdit;
                                      if (isPhoneEdit == false) {
                                        updateUserPhone(documents[0].id);
                                      }
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    }else if(snapshot.connectionState == ConnectionState.waiting){
                      return Loading();
                    }else{
                      return NoData();
                    }

                  }
              ),
            ),
          ),
        );
      }else if(widget.type == 'merchant'){
        return Scaffold(
          backgroundColor: Color(0xffF1F2F3),
          extendBodyBehindAppBar: true,
          appBar: AppBar(
            iconTheme: IconThemeData(color: Colors.green),
            title: Container(
              margin: EdgeInsetsDirectional.only(top: 10),
              child: Text(
                'الصفحة الشخصية',
                style: TextStyle(
                    color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
              ),
            ),
            centerTitle: true,
            backgroundColor: Colors.transparent,
            elevation: 0,
          ),
          body: SafeArea(
            child: SingleChildScrollView(
              clipBehavior: Clip.antiAlias,
              physics: BouncingScrollPhysics(),
              child: StreamBuilder<QuerySnapshot>(
                  stream: FirebaseFirestoreController().readUser(widget.type),
                  builder: (context, snapshot) {
                    if(snapshot.hasData && snapshot.data!.docs.length != 0){
                      List<DocumentSnapshot> documents = snapshot.data!.docs;
                      path = documents[0].id;
                      return Column(
                        mainAxisAlignment: MainAxisAlignment.start,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisSize: MainAxisSize.min,
                        children: [
                          SizedBox(height: 5),
                          Center(
                            child: Stack(
                              alignment: AlignmentDirectional.bottomEnd,
                              children: [
                                Container(
                                  decoration: BoxDecoration(
                                    borderRadius: BorderRadius.circular(80),
                                    border:
                                    Border.all(width: 8, color: Colors.white),
                                  ),
                                  child: Container(
                                    clipBehavior: Clip.antiAlias,
                                    width: 100,
                                    height: 100,
                                    decoration: BoxDecoration(
                                        borderRadius: BorderRadius.circular(50),
                                        color: Colors.deepOrange),
                                    child: widget.merchant != null
                                        ? pickedImage2 != null
                                        ? Image.file(
                                      File(pickedImage2!.path),
                                      fit: BoxFit.cover,
                                    )
                                        : Image.network(
                                      widget.merchant!.image,
                                      fit: BoxFit.cover,
                                    )
                                        : pickedImage2 != null
                                        ? Image.file(
                                      File(pickedImage2!.path),
                                      fit: BoxFit.cover,
                                    )
                                        : Icon(
                                      Icons.add_photo_alternate_outlined,
                                      color: Colors.white,
                                      size: 50,
                                    ),
                                  ),
                                ),
                                Padding(
                                  padding: const EdgeInsetsDirectional.only(
                                      bottom: 12, end: 5, top: 0, start: 0),
                                  child: Container(
                                    // padding: EdgeInsetsDirectional.only(bottom: 50),
                                    child: CircleAvatar(
                                      backgroundColor: Colors.green,

                                      radius: 13,
                                      child: IconButton(
                                        padding: EdgeInsetsDirectional.zero,
                                        onPressed: () {
                                          isImageEdit = !isImageEdit;
                                          if (isImageEdit == true) {
                                            setState(() {
                                              pickImage();
                                              iconEditImage =
                                                  Icons.check_outlined;
                                              print('go to check');
                                            });
                                          }
                                          if (isImageEdit == false) {
                                            setState(() {
                                              print('back to edit');
                                              iconEditImage = Icons.edit;
                                              ///method update image
                                              updateMerchantImage(documents[0].id);
                                            });
                                          }
                                        },
                                        icon: Icon(iconEditImage,color: Colors.white,size: 18,),
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                          SizedBox(
                            height: SizeConfig().scaleHeight(10),
                          ),
                          Padding(
                            padding: const EdgeInsets.symmetric(horizontal: 32),
                            child: Column(
                              children: [
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: !isNameEdit,
                                  showCursor: isNameEdit,
                                  controller: _name,
                                  prefix: Icons.person,
                                  suffix: isNameEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    setState(() {
                                      isNameEdit = !isNameEdit;
                                      if (isNameEdit == false) {
                                        updateMerchantName(documents[0].id);
                                      }
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(10),
                                ),
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: !isShopNameEdit,
                                  showCursor: isShopNameEdit,
                                  controller: _shopName,
                                  prefix: Icons.directions_car_outlined,
                                  suffix: isShopNameEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    setState(() {
                                      isShopNameEdit = !isShopNameEdit;
                                      if (isShopNameEdit == false) {
                                        updateMerchantMarketName(documents[0].id);
                                      }
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(10),
                                ),
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: true,
                                  showCursor: isCountryEdit,
                                  controller: _country,
                                  prefix: Icons.location_on_outlined,
                                  suffix: isCountryEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    setState(() {
                                      isCountryEdit = !isCountryEdit;
                                      if (isCountryEdit == true) {
                                        showBottomSheet();
                                      }
                                      if (isCountryEdit == false) {
                                        updateMerchantPlace(documents[0].id);
                                      }
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(10),
                                ),
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: true,
                                  showCursor: isAddersEdit,
                                  controller: _adders,
                                  prefix: Icons.map_outlined,
                                  suffix: isAddersEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    setState(() {
                                      isAddersEdit = !isAddersEdit;
                                      if (isAddersEdit == true) {
                                        Navigator.pushNamed(context, 'map_screen');
                                      }
                                      if (isAddersEdit == false) {
                                        updateMerchantAddress(documents[0].id);
                                      }
                                    });
                                  },
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(10),
                                ),
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: !isEmailEdit,
                                  showCursor: isEmailEdit,
                                  controller: _email,
                                  prefix: Icons.email_outlined,
                                  suffix: isEmailEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    // setState(() {
                                    //   isNameEdit = !isNameEdit;
                                    //   if (isNameEdit == true) {
                                    //     ///update name method
                                    //   }
                                    // });
                                  },
                                ),
                                SizedBox(
                                  height: SizeConfig().scaleHeight(10),
                                ),
                                AppTextFiledProfile(
                                  textInputType: TextInputType.text,
                                  labelText: '',
                                  readOnly: !isPhoneEdit,
                                  showCursor: isPhoneEdit,
                                  controller: _phone,
                                  prefix: Icons.phone_android_rounded,
                                  suffix: isPhoneEdit ? Icons.check : Icons.edit,
                                  functionSuffixPressed: () {
                                    setState(() {
                                      isPhoneEdit = !isPhoneEdit;
                                      if (isPhoneEdit == false) {
                                        updateMerchantPhone(documents[0].id);
                                      }
                                    });
                                  },
                                ),
                              ],
                            ),
                          ),
                        ],
                      );
                    }else if(snapshot.connectionState == ConnectionState.waiting){
                      return Loading();
                    }else{
                      return NoData();
                    }
                  }
              ),
            ),
          ),
        );
      }else{
        return Loading();
      }
    }else{
      return NoData();
    }
  }

  Future<void> updateMerchantName(String path) async{
    await FirebaseFirestoreController().updateMerchantName(path: path, name: _name.text);
  }

  Future<void> updateMerchantImage(String path) async{
    await FirebaseFirestoreController().updateMerchantImage(path: path, image: selectedImage);
  }

  Future<void> updateMerchantMarketName(String path) async{
    await FirebaseFirestoreController().updateMerchantMarketName(path: path, marketName: _shopName.text);
  }

  Future<void> updateMerchantAddress(String path) async{
    await FirebaseFirestoreController().updateMerchantAddress(path: path, address: AppPreferences().getPlace());
  }

  Future<void> updateMerchantPlace(String path) async{
    await FirebaseFirestoreController().updateMerchantPlace(path: path, place: _country.text);
  }

  Future<void> updateMerchantPhone(String path) async{
    await FirebaseFirestoreController().updateMerchantPhone(path: path, phone: _phone.text);
  }

  Future<void> updateUserName(String path) async{
    await FirebaseFirestoreController().updateUserName(path: path, name: _name.text);
  }

  Future<void> updateUserImage(String path) async{
    await FirebaseFirestoreController().updateUserImage(path: path, image: selectedImage);
  }

  Future<void> updateUserPhone(String path) async{
    await FirebaseFirestoreController().updateUserPhone(path: path, phone: _phone.text);
  }

  Future<void> updateUserPlace(String path) async{
    await FirebaseFirestoreController().updateUserPlace(path: path, place: _country.text);
  }

  void getUserData(DocumentSnapshot snapshot) {
    name = snapshot.get('name');
  }

  Future<void> pickImage() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage(),
            message: Text('جاري تحميل الصورة...')),
      );
      setState(() {
        pickedImage2 = pickedImage;
      });
    }
  }

  Future<void> uploadImage() async {
    selectedImage = await FirebaseStorageController().uploadImage(File(pickedImage!.path), 'car');
  }

  void showBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RadioListTile(
                  title: Text('شمال غزة'),
                  value: 'شمال غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('غرب غزة'),
                  value: 'غرب غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('شرق غزة'),
                  value: 'شرق غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('جنوب غزة'),
                  value: 'جنوب غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });
                    Navigator.pop(context);
                  }),
            ],
          ),
        );
      },
    );
  }

}