import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/car.dart';
import 'package:car_user_project/models/favorite.dart';
import 'package:car_user_project/screens/home/details/detials_screen.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:car_user_project/widgets/search_bar.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';


class SearchScreen extends StatefulWidget {
  const SearchScreen({Key? key}) : super(key: key);

  @override
  _SearchScreenState createState() => _SearchScreenState();
}

class _SearchScreenState extends State<SearchScreen> {
  String? type;
  String? model;
  String? state;
  String? merchant;
  String? merchant2;
  double? price;
  String carId = '';
  String id = '';
  double? price2;
  TextEditingController _search = TextEditingController();
  List<bool> favorite = <bool>[];
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: Color(0xff1DB854),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            'البحث',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
        ),

        backgroundColor: Colors.white,
        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Container(
                  decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(50),
                    boxShadow: [
                      BoxShadow(
                        color: Colors.black.withOpacity(0.16),
                        blurRadius: 1,
                      )
                    ],
                  ),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [
                      SizedBox(
                        width: 15,
                      ),
                      GestureDetector(
                        onTap: (){setState(() {
                          type = _search.text;
                        });},
                        child: Icon(
                          Icons.search,
                          color: Colors.grey,
                        ),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                      Container(
                        width: 200,
                        child: TextField(
                          controller: _search,
                          decoration: InputDecoration(
                            disabledBorder: InputBorder.none,
                            hintText: 'بحث ...',
                            hintStyle: TextStyle(fontSize: 14),
                            enabledBorder: InputBorder.none,
                            focusedBorder: InputBorder.none,
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    InkWell(child: GestureDetector(onTap: (){showStateBottomSheet();}, child: customSearch(title: state != null ? state! : 'الحالة'))),
                    Spacer(),
                    InkWell(child: GestureDetector(onTap: (){showNameBottomSheet();}, child: customSearch(title: type != null ? type! : 'النوع'))),
                    Spacer(),
                    InkWell(child: GestureDetector(onTap: (){showMerchantBottomSheet();}, child: customSearch(title: merchant2 != null ? merchant2! : 'المعرض'))),
                  ],
                ),
                SizedBox(
                  height: 8,
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    GestureDetector(onTap: (){showModelBottomSheet();}, child: InkWell(child: customSearch(title: model != null ? model! : 'الموديل'))),
                    Spacer(),
                    InkWell(child: customSearch(title: 'اللون')),
                    Spacer(),
                    InkWell(child: GestureDetector(onTap: (){showPriceBottomSheet();}, child: customSearch(title: price.toString() != null ? price.toString() : 'السعر'))),
                  ],
                ),
                SizedBox(
                  height: 15,
                ),
                StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestoreController().searchCar(name: type, model: model, state: state, merchant: merchant, price: price),
                    builder: (context, snapshot) {
                      if(snapshot.hasData && snapshot.data!.docs.length > 0){
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        return Expanded(
                          child: GridView.builder(
                            scrollDirection: Axis.vertical,
                            physics: BouncingScrollPhysics(),
                            clipBehavior: Clip.antiAlias,
                            itemCount: documents.length,
                            gridDelegate:
                            SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisSpacing: 5,
                            ),
                            itemBuilder: (context, index) {
                              carId = documents[index].id;
                              //isFavorite();
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => DetailsCarScreen(
                                              getCar(documents[index]))));
                                },
                                child: Card(
                                  color: Colors.white,
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  clipBehavior: Clip.antiAlias,
                                  child: Column(
                                    children: [
                                      Container(
                                        width: double.infinity,
                                        height: 125,
                                        child: Image.network(
                                          List.from(
                                              documents[index].get('images'))[0],
                                          fit: BoxFit.cover,
                                          height: double.infinity,
                                          width: double.infinity,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsetsDirectional.only(
                                            start: 10, end: 5, top: 0, bottom: 0),
                                        child: Container(
                                          color: Colors.white,
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Column(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    textBaseline:
                                                    TextBaseline.ideographic,
                                                    children: [
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Text(
                                                        documents[index]
                                                            .get('name'),
                                                        maxLines: 1,
                                                        overflow:
                                                        TextOverflow.ellipsis,
                                                        style: TextStyle(
                                                          fontSize: 17,
                                                          fontWeight:
                                                          FontWeight.bold,
                                                          textBaseline:
                                                          TextBaseline
                                                              .ideographic,
                                                        ),
                                                      ),
                                                      Text(
                                                        '${documents[index].get('price').toString()}\$',
                                                        style: TextStyle(
                                                            color:
                                                            Color(0xff026b28),
                                                            fontSize: 16,
                                                            fontWeight:
                                                            FontWeight.bold),
                                                      ),
                                                    ],
                                                  ),
                                                  Spacer(),
                                                  IconButton(
                                                    onPressed: () async {
                                                      // if (favorite[index]) {
                                                      //   await getFavoriteId();
                                                      // }
                                                      // print(index);
                                                      // await favoriteProses(
                                                      //     documents[index],
                                                      //     index);
                                                    },
                                                    icon: Icon(
                                                      Icons.favorite_border,
                                                      size: 23,
                                                      color: Colors.red,
                                                    ),
                                                    padding: EdgeInsetsDirectional
                                                        .zero,
                                                    alignment:
                                                    AlignmentDirectional
                                                        .centerEnd,
                                                  )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                            //         );
                          ),
                        );
                      }else if(snapshot.connectionState == ConnectionState.waiting){
                        return Loading();
                      }else{
                        return NoData();
                      }

                    }
                ),
              ],
            ),
          ),
        ));
  }

  Widget customSearch({required String title}) {
    return Container(
      height: 50,
      width: 100,
      decoration: BoxDecoration(
        color: Color(0xff1DB854),
        borderRadius: BorderRadius.circular(50),
      ),
      child: Center(
          child: Row(
            children: [
              // SizedBox(width: 18,),
              Spacer(),
              Spacer(),

              Center(
                  child: Text(
                    title,
                    style: TextStyle(
                        color: Colors.white, fontSize: 18, fontWeight: FontWeight.bold),
                  )),
              Icon(
                Icons.arrow_drop_down,
                size: 28,
                color: Colors.white,
              ),
              Spacer(),
            ],
          )),
    );
  }

  void showNameBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestoreController().readCarName(),
          builder: (context, snapshot) {
            if(snapshot.hasData){
              List<DocumentSnapshot> documents = snapshot.data!.docs;
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: ListView.builder(
                  itemCount: documents.length,
                  itemBuilder: (context, index) {
                    return RadioListTile(
                      title: Text(documents[index].get('name')),
                      value: documents[index].get('name').toString(),
                      groupValue: type,
                      onChanged: (String? value) {
                        if (value != null)
                          setState( () {
                            type = value;
                            Navigator.pop(context);
                          },
                          );
                      },
                    );
                  },
                ),
              );
            }else{
              return Container();
            }
          },
        );
      },

    );
  }
  void showModelBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestoreController().readCarName(),
          builder: (context, snapshot) {
            if(snapshot.hasData){
              List<DocumentSnapshot> documents = snapshot.data!.docs;
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: ListView.builder(
                  itemCount: documents.length,
                  itemBuilder: (context, index) {
                    return RadioListTile(
                      title: Text(documents[index].get('model')),
                      value: documents[index].get('model').toString(),
                      groupValue: model,
                      onChanged: (String? value) {
                        if (value != null)
                          setState( () {
                            model = value;
                            Navigator.pop(context);
                          },
                          );
                      },
                    );
                  },
                ),
              );
            }else{
              return Container();
            }
          },
        );
      },

    );
  }
  void showStateBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RadioListTile(
                  title: Text('جديدة'),
                  value: 'جديدة',
                  groupValue: state,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        state = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('مستعملة'),
                  value: 'مستعملة',
                  groupValue: state,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        state = value;
                      });

                    Navigator.pop(context);
                  }),
            ],
          ),
        );
      },
    );
  }
  void showMerchantBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestoreController().readMerchant(),
          builder: (context, snapshot) {
            if(snapshot.hasData){
              List<DocumentSnapshot> documents = snapshot.data!.docs;
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: ListView.builder(
                  itemCount: documents.length,
                  itemBuilder: (context, index) {
                    return RadioListTile(
                      title: Text(documents[index].get('marketName')),
                      value: documents[index].id,
                      groupValue: model,
                      onChanged: (String? value) {
                        if (value != null)
                          setState( () {
                            merchant = value;
                            merchant2 = documents[index].get('marketName');
                            Navigator.pop(context);
                          },
                          );
                      },
                    );
                  },
                ),
              );
            }else{
              return Container();
            }
          },
        );
      },

    );
  }
  void showPriceBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestoreController().readCar2(),
          builder: (context, snapshot) {
            if(snapshot.hasData){
              List<DocumentSnapshot> documents = snapshot.data!.docs;
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: ListView.builder(
                  itemCount: documents.length,
                  itemBuilder: (context, index) {
                    return RadioListTile(
                      title: Text(documents[index].get('price').toString()),
                      value: documents[index].get('price').toString(),
                      groupValue: model,
                      onChanged: (String? value) {
                        if (value != null)
                          setState( () {
                            price = double.parse(value);
                            Navigator.pop(context);
                          },
                          );
                      },
                    );
                  },
                ),
              );
            }else{
              return Container();
            }
          },
        );
      },

    );
  }

  Car getCar(DocumentSnapshot snapshot) {
    Car car = Car();
    car.images = List.from(snapshot.get('images'));
    car.name = snapshot.get('name');
    car.price = snapshot.get('price');
    car.description = snapshot.get('description');
    car.model = snapshot.get('model');
    car.state = snapshot.get('state');
    car.path = snapshot.id;
    car.color = List.from(snapshot.get('color'));
    car.merchant = snapshot.get('merchant');
    return car;
  }

// Future<void> isFavorite() async {
//   bool state = await FirebaseFirestoreController().isFavorite(carId);
//   favorite.add(state);
// }

// Future<void> addFavorite(DocumentSnapshot snapshot, int index) async {
//   bool state = await FirebaseFirestoreController()
//       .addFavorite(setCarFavorite(snapshot));
//   if (state) {
//     setState(() {
//       favorite[index] = true;
//     });
//   }
// }
// Favorite setCarFavorite(DocumentSnapshot snapshot) {
//   Favorite favorite = Favorite();
//   favorite.name = snapshot.get('name');
//   favorite.merchant = snapshot.get('merchant');
//   favorite.color = List.from(snapshot.get('color'));
//   favorite.state = snapshot.get('state');
//   favorite.price = snapshot.get('price');
//   favorite.description = snapshot.get('description');
//   favorite.model = snapshot.get('model');
//   favorite.images = List.from(snapshot.get('images'));
//   favorite.email = FirebaseAuth.instance.currentUser!.email!;
//   favorite.carId = snapshot.id;
//   return favorite;
// }
//
// Future<void> deleteFavorite(int index) async {
//   print('id = ' + id);
//   bool state = await FirebaseFirestoreController().deleteFavorite(path: id);
//   if (state) {
//     setState(() {
//       favorite[index] = false;
//     });
//   }
// }
//
// Future<void> getFavoriteId() async {
//   print(carId);
//   String state = await FirebaseFirestoreController().getFavoriteId(carId);
//   setState(() {
//     id = state;
//   });
// }
//
// Future<void> favoriteProses(DocumentSnapshot snapshot, int index) async {
//   if (favorite[index]) {
//     await deleteFavorite(index);
//   } else {
//     await addFavorite(snapshot, index);
//   }
// }
}
