import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/room.dart';
import 'package:car_user_project/models/user.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:car_user_project/widgets/profile_item.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'chat_detailes_screen.dart';

class ChatScreen extends StatefulWidget {
  const ChatScreen({Key? key}) : super(key: key);

  @override
  _ChatScreenState createState() => _ChatScreenState();
}

class _ChatScreenState extends State<ChatScreen> {
  String type = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserType();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    if(FirebaseAuth.instance.currentUser != null){
      return Scaffold(
        backgroundColor: Color(0xffF1F2F3),
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: Container(
            margin: EdgeInsetsDirectional.only(top: 10),
            child: Text(
              'الرسائل',
              style: TextStyle(
                  color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: SafeArea(
          child: Padding(
            padding: const EdgeInsets.symmetric(
                horizontal: 20, vertical: 0),
            child: StreamBuilder<QuerySnapshot>(
              // stream: FirebaseFirestoreController().readRoom(),
              stream: type == 'user' ? FirebaseFirestoreController().readAllMerchant() : FirebaseFirestoreController().readAllUser(),
              builder: (context, snapshot) {
                if(snapshot.hasData && snapshot.data!.docs.length > 0){
                  List<DocumentSnapshot> documents = snapshot.data!.docs;
                  return ListView.separated(
                    padding: EdgeInsetsDirectional.only(top: 10),
                    itemBuilder: (context, index) {
                      return InkWell(
                        child: showChat(
                          context: context,
                          title: documents[index].get('name'),
                          subtitle: documents[index].get('phoneNumber'),
                          url: documents[index].get('image'),
                        ),
                        onTap: () {
                          Navigator.push(context, MaterialPageRoute(builder: (context) => ChatScreenDetails(getUser(documents[index])),));
                        },
                      );
                    },
                    separatorBuilder: (BuildContext context, int index) {
                      return SizedBox(
                        height: SizeConfig().scaleHeight(14),
                      );
                    },
                    itemCount: documents.length,
                    shrinkWrap: false,
                    physics: BouncingScrollPhysics(),
                    clipBehavior: Clip.antiAlias,
                  );
                }else if(snapshot.connectionState == ConnectionState.waiting){
                  return Loading();
                }else{
                  return NoData();
                }

              }
            ),
          ),
        ),
      );
    }else{
      return Scaffold(
        appBar: AppBar(
          title: Container(
            margin: EdgeInsetsDirectional.only(top: 10),
            child: Text(
              'الرسائل',
              style: TextStyle(
                  color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
        ),
        body: ProfileSetting(
          title: 'تسجيل الدخول',
          firstIcon: Icons.login,
          onPressed: () {
            Navigator.pushReplacementNamed(context, 'login_screen');
          },
        ),
      );

    }
  }

  Users getUser(DocumentSnapshot snapshot){
    Users users = Users();
    users.name = snapshot.get('name');
    users.email = snapshot.get('email');
    users.image = snapshot.get('image');
    return users;
  }

  Future<void> getUserType() async{
    String ds = await FirebaseFirestoreController().getUserType();
    print(ds);
    setState(() {
      type = ds;
    });
  }

  Room getRoom(DocumentSnapshot snapshot){
   Room room = Room();
   room.name = snapshot.get('name');
   room.email = snapshot.get('email');
   room.image = snapshot.get('image');
   room.path = snapshot.id;
   return room;
  }
}