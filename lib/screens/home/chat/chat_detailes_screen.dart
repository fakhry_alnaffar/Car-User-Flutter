import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/chat.dart';
import 'package:car_user_project/models/room.dart';
import 'package:car_user_project/models/user.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class ChatScreenDetails extends StatefulWidget {
  Users users;
  // Room room;
  ChatScreenDetails(this.users);

  @override
  _ChatScreenDetailsState createState() => _ChatScreenDetailsState();
}

class _ChatScreenDetailsState extends State<ChatScreenDetails> {
  late TextEditingController _message;
  DocumentSnapshot? documentSnapshotUser;
  String type = '';
  bool state2 = false;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _message = TextEditingController();
    getUserType();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _message.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Color(0xffF1F2F3),
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          widget.users.name,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
      ),
      body: SafeArea(
        child: GestureDetector(
          onTap: (){
            FocusScope.of(context).unfocus();
          },
          child: Column(
            children: [
              Expanded(
                child: Container(
                  height: 100,
                  padding: EdgeInsets.symmetric(horizontal: 20),
                  decoration: BoxDecoration(
                    color: Color(0xffF1F2F3),
                    borderRadius: BorderRadius.only(
                      topLeft: Radius.circular(30),
                      topRight: Radius.circular(30),
                    ),
                  ),
                  child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestoreController().readChat(FirebaseAuth.instance.currentUser!.email! + " & " +  widget.users.email , widget.users.email + " & " + FirebaseAuth.instance.currentUser!.email!),
                    builder: (context, snapshot) {
                      if(snapshot.hasData && snapshot.data!.docs.length > 0){
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        return ListView.builder(
                          itemBuilder: (context, index) {
                            return Container(
                              margin: EdgeInsets.only(top: 8),
                              child: documents[index].get('sender') == FirebaseAuth.instance.currentUser!.email ? me(context,documents[index].get('message'), documents[index].get('created').toString().substring(10, 16)) : other(context,documents[index].get('message'), documents[index].get('created').toString().substring(10, 16), documents[index].get('senderImage')),
                            );
                          },
                          reverse: true,
                          shrinkWrap: true,
                          physics: BouncingScrollPhysics(),
                          clipBehavior: Clip.antiAliasWithSaveLayer,
                          itemCount: documents.length,
                          padding: EdgeInsetsDirectional.only(top: 10),);
                      }else if(snapshot.connectionState == ConnectionState.waiting){
                        return Loading();
                      }else{
                        return NoData();
                      }

                    }
                  ),
                ),
              ),
              Container(
                height: 100,
                decoration: BoxDecoration(
                    color: Color(0xffF1F2F3),
                    borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(0),
                        topRight: Radius.circular(0))),
                child: Row(
                  children: [
                    Expanded(
                      child: Container(
                        height: 60,
                        margin: EdgeInsetsDirectional.all(20),
                        decoration: BoxDecoration(
                            color: Colors.grey.shade300,
                            borderRadius: BorderRadius.circular(50)),
                        child: Row(
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Expanded(
                              child: TextField(
                                controller: _message,
                                cursorColor: Color(0xff1DB854),
                                decoration: InputDecoration(
                                    border: InputBorder.none,
                                    hintText: 'اكتب شيئاً...',
                                hintStyle:
                                    TextStyle(color: Colors.grey.shade500)),
                              ),
                            ),
                            IconButton(
                                onPressed: () {sendMessage();},
                                icon: Icon(
                                  Icons.send,
                                  color: Color(0xff1DB854),
                                  textDirection: TextDirection.rtl,
                                )),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }

  bool checkData(){
    if(_message.text.isNotEmpty){
      return true;
    }else{
      return false;
    }
  }

  // Future<void> sendMessage() async{
  //   if(checkData()){
  //     await getUserData();
  //     bool state = await FirebaseFirestoreController().addChat(getChat());
  //     setState(() {
  //       state2 = state;
  //     });
  //     _message.clear();
  //   }
  // }

  Future<void> sendMessage() async{
    if(checkData()){
      await getUserData();
      bool state = await FirebaseFirestoreController().addChat(getChat());
      setState(() {
        state2 = state;
      });
      _message.clear();
    }
  }

  Chat getChat(){
    Chat chat = Chat();
    chat.senderName = documentSnapshotUser!.get('name');
    chat.senderImage = documentSnapshotUser!.get('image');
    chat.sender = documentSnapshotUser!.get('email');
    chat.message = _message.text;
    chat.between = documentSnapshotUser!.get('email') + " & " +  widget.users.email;
    chat.recever = widget.users.email;
    chat.receverImage = widget.users.image;
    chat.receverName = widget.users.name;
    return chat;
  }

  // Chat getChat(){
  //   Chat chat = Chat();
  //   chat.senderName = documentSnapshotUser!.get('name');
  //   chat.senderImage = documentSnapshotUser!.get('image');
  //   chat.sender = documentSnapshotUser!.get('email');
  //   chat.message = _message.text;
  //   chat.between = documentSnapshotUser!.get('email') + " & " +  widget.room.email;
  //   chat.recever = widget.room.email;
  //   chat.receverImage = widget.room.image;
  //   chat.receverName = widget.room.name;
  //   return chat;
  // }

  Future<void> getUserData() async{
    print('Type = ' + type);
    DocumentSnapshot ds = await FirebaseFirestoreController().getUserData(type);
    print('snapshot = ' + ds.toString());
    setState(() {
      documentSnapshotUser = ds;
    });
  }

  Future<void> getUserType() async{
    String ds = await FirebaseFirestoreController().getUserType();
    print(ds);
    setState(() {
      type = ds;
    });
  }

}
