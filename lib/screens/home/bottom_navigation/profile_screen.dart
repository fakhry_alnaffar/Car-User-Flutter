import 'package:car_user_project/firebase/firebase_auth_controller.dart';
import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/merchant.dart';
import 'package:car_user_project/models/user.dart';
import 'package:car_user_project/screens/controller_panel/controlle_panel.dart';
import 'package:car_user_project/screens/home/profile/setting_screen.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:car_user_project/widgets/profile_item.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../chat/chat_detailes_screen.dart';
import '../profile/edit_profile_screen.dart';

class ProfileScreen extends StatefulWidget {
  const ProfileScreen({Key? key}) : super(key: key);

  @override
  _ProfileScreenState createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  String type = '';
  String _language = 'ar';
  late String name = '';
  String about = '';
  String adders = '';
  String phone = '';
  String email = '';
  bool isMana = false;

  @override
  void initState() {
    super.initState();
    getUserType();
    getAbout();
    getAdders();
    getPhone();
    getEmail();
    isManager();

  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'الصفحة الشخصية',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        child: Padding(
          padding: EdgeInsets.symmetric(horizontal: 10),
          child: StreamBuilder<QuerySnapshot>(
              stream: FirebaseFirestoreController().readUser(type),
              builder: (context, snapshot) {
                if (snapshot.hasData && snapshot.data!.docs.isNotEmpty) {
                  List<DocumentSnapshot> documents = snapshot.data!.docs;
                  String imageUrl = documents[0].get('image');
                  return Column(
                    children: [
                      SizedBox(
                        height: 10,
                      ),
                      Container(
                        clipBehavior: Clip.antiAlias,
                        width: 80,
                        height: 80,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(200),
                        ),
                        child: Stack(
                          children: [
                            Container(
                              width: 80,
                              height: 80,
                              child: Image.network(
                                documents[0].get('image'), fit: BoxFit.cover,),
                              decoration: BoxDecoration(
                                borderRadius: BorderRadius.circular(50),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Text(
                        documents[0].get('name'),
                        style: TextStyle(
                            fontSize: 18, fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        height: 3,
                      ),
                      Text(
                        documents[0].get('email'),
                        style: TextStyle(fontSize: 15, color: Colors.grey),
                      ),
                      SizedBox(
                        height: 30,
                      ),
                      SizedBox(
                        height: 20,
                      ),
                      Padding(
                        padding: EdgeInsets.symmetric(horizontal: 15),
                        child: Column(
                          children: [
                            ProfileSetting(
                              title: 'تعديل الحساب',
                              firstIcon: Icons.person_outline,
                              onPressed: () {
                                if (type == 'user') {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) =>
                                          EditProfileScreen(
                                              getUserData(documents[0]), null,
                                              type)));
                                } else if (type == 'merchant') {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) =>
                                          EditProfileScreen(null,
                                              getMerchantData(documents[0]),
                                              type)));
                                }
                              },
                            ),
                            Visibility(
                              visible: (type == 'user' && isMana == true) || type == 'merchant'? true : false,
                              child: ProfileSetting(
                                title: 'لوحة التحكم',
                                firstIcon: Icons.keyboard_control,
                                onPressed: () {
                                  Navigator.push(context, MaterialPageRoute(
                                      builder: (context) =>
                                          ControllerSelect(imageUrl)));
                                },
                              ),
                            ),
                            Visibility(
                              visible: type == 'user' ? true : false,
                              child: ProfileSetting(
                                title: 'اعلاناتي',
                                firstIcon: Icons.add_box,
                                onPressed: () {
                                  Navigator.pushNamed(
                                      context, 'ads_home_screen');
                                },
                              ),
                            ),
                            ProfileSetting(
                              title: 'تغيير كلمة المرور',
                              firstIcon: Icons.lock_outline,
                              onPressed: () {
                                Navigator.pushNamed(
                                    context, 'change_password_screen');
                              },
                            ),
                            ProfileSetting(
                              title: 'الاعدادات',
                              firstIcon: Icons.settings,
                              onPressed: () {
                                Navigator.push(context, MaterialPageRoute(
                                    builder: (context) =>
                                        SettingScreen(about,adders,phone,email)));
                              },
                            ),
                            ProfileSetting(
                              title: 'تواصل معنا',
                              firstIcon: Icons.contact_support_outlined,
                              onPressed: () {},
                            ),
                            ProfileSetting(
                              title: 'تسجيل الخروج',
                              firstIcon: Icons.logout,
                              onPressed: () {
                                signOut();
                              },
                            ),
                          ],
                        ),
                      ),
                    ],
                  );
                }
                else if (FirebaseAuthController().isLoggedIn == false) {
                  return ProfileSetting(
                    title: 'تسجيل الدخول',
                    firstIcon: Icons.login,
                    onPressed: () {
                      Navigator.pushReplacementNamed(context, 'login_screen');
                    },
                  );
                }
                else {
                  return Center(
                    child: Loading(
                    ),
                  );
                }
              }
          ),
        ),
      ),
    );
  }

  void showAlertDialog() {
    showDialog(
      context: context,
      barrierColor: Colors.black.withOpacity(0.25),
      builder: (context) {
        return AlertDialog(
          title: Text('تسجيل خروج'),
          content: Text(
            'هل انت متاكد من تسجيل الخروج !',
            style: TextStyle(color: Colors.grey),
          ),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
          actions: [
            TextButton(
              onPressed: () {
                signOut();
              },
              child: Text(
                'نعم',
                style: TextStyle(color: Color(0xff1DB854)),
              ),
            ),
            TextButton(
              onPressed: () {
                Navigator.pop(context);
              },
              child: Text(
                'لا',
                style: TextStyle(color: Colors.grey),
              ),
            ),
          ],
        );
      },
    );
  }

  void showBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RadioListTile(
                  title: Text('Arabic'),
                  value: 'ar',
                  groupValue: _language,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _language = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('English'),
                  value: 'en',
                  groupValue: _language,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _language = value;
                      });

                    Navigator.pop(context);
                  }),
            ],
          ),
        );
      },
    );
  }

  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushReplacementNamed(context, 'login_screen');
  }

  Users getUserData(DocumentSnapshot snapshot) {
    Users user = Users();
    user.path = snapshot.id.toString();
    user.name = snapshot.get('name');
    user.place = snapshot.get('place');
    user.phoneNumber = snapshot.get('phoneNumber');
    user.email = snapshot.get('email');
    user.image = snapshot.get('image');
    return user;
  }

  Merchant getMerchantData(DocumentSnapshot snapshot) {
    Merchant merchant = Merchant();
    merchant.path = snapshot.id.toString();
    merchant.name = snapshot.get('name');
    merchant.place = snapshot.get('place');
    merchant.phoneNumber = snapshot.get('phoneNumber');
    merchant.email = snapshot.get('email');
    merchant.image = snapshot.get('image');
    merchant.marketName = snapshot.get('marketName');
    merchant.address = snapshot.get('address');
    return merchant;
  }

  Future<void> getUserType() async {
    String typee = await FirebaseFirestoreController().getUserType();
    setState(() {
      type = typee;
    });
  }

  Future<void> getAbout() async {
    String data = await FirebaseFirestoreController().aboutApp();
    setState(() {
      about = data;
    });
  }
  Future<void> isManager() async{
    bool state = await FirebaseFirestoreController().isManager();
    setState(() {
      isMana = state;
    });
  }

  Future<void> getAdders() async {
    String data = await FirebaseFirestoreController().adders();
    setState(() {
      adders = data;
    });
  }

  Future<void> getPhone() async {
    String data = await FirebaseFirestoreController().phone();
    setState(() {
      phone = data;
    });
  }

  Future<void> getEmail() async {
    String data = await FirebaseFirestoreController().email();
    setState(() {
      email = data;
    });
  }
}



class ProfileIcon extends StatelessWidget {
  final String title;
  final IconData icon;

  ProfileIcon({
    required this.title,
    required this.icon,
  });

  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        Icon(
          icon,
          color: Color(0xff1DB854),
        ),
        SizedBox(height: 10),
        Text(title),
      ],
    );
  }
}
