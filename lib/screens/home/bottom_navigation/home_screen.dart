import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/ads.dart';
import 'package:car_user_project/models/car.dart';
import 'package:car_user_project/models/favorite.dart';
import 'package:car_user_project/models/merchant.dart';
import 'package:car_user_project/screens/ads/ads_details.dart';
import 'package:car_user_project/utils/helpers.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/current_page.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import '../details/detials_screen.dart';
import '../details/shop_detials.dart';

class HomeScreen extends StatefulWidget {
  const HomeScreen({Key? key}) : super(key: key);

  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> with Helpers{
  int _currentPage = 0;
  List<bool> favorite = <bool>[];
  List<String> ids = <String>[];
  DocumentSnapshot? documentSnapshot;
  bool selected = false;
  List<Widget> adss = <Widget>[];
  List<Widget> adss2 = <Widget>[];
  List<Widget> selectedPage = <Widget>[];
  List<Widget> selectedPage2 = <Widget>[];
  late PageController _pageController;
  IconData icon = Icons.favorite_outline_rounded;
  bool selectedIcon = false;
  String id = '';
  String carId = '';
  List<String> addresses = <String>[
    'جنوب غزة',
    'شمال غزة',
    'غرب غزة',
    'شرق غزة'
  ];
  String name = '';
  String name2 = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    // isFavorite();
    _pageController = PageController();
    getUserName();
    getMarchentName();
    getAllAds();
    pageSelected();
    check();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController.dispose;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    String greeting() {
      var hour = DateTime
          .now()
          .hour;
      // var hour =5;
      if (hour < 13 && hour > 4) {
        return ' صباح الخير $name$name2 🌞 ';
      }
      if (hour >= 13 && hour < 23) {
        return ' مساء الخير $name$name2 💚';
      }
      return ' ليلة سعيدة $name$name2 🌸';
    }
    String greetingMes = greeting();
    return Scaffold(
      appBar: AppBar(
        automaticallyImplyLeading: false,
        backgroundColor: Color(0xffF1F2F3),
        title: Container(
            margin: EdgeInsetsDirectional.only(top: 0),
            child: Text(
              '$greetingMes',
              style: TextStyle(color: Colors.black),
            )),
        elevation: 0,
        actions: [
          IconButton(
            onPressed: () {
              Navigator.pushNamed(context, 'search_screen');
            },
            icon: Icon(
              Icons.search_rounded,
              color: Colors.grey.shade700,
            ),
          ),
        ],
      ),
      backgroundColor: Color(0xffF1F2F3),
      body: SingleChildScrollView(
        padding: EdgeInsetsDirectional.zero,
        clipBehavior: Clip.antiAlias,
        physics: BouncingScrollPhysics(),
        child: Container(
          clipBehavior: Clip.antiAlias,
          decoration: BoxDecoration(),
          margin: EdgeInsets.symmetric(horizontal: 5, vertical: 10),
          child: Column(
            children: [
              SizedBox(
                width: double.infinity,
                height: 200,
                child: Stack(
                  children: [
                    Container(
                      clipBehavior: Clip.antiAlias,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(15),
                      ),
                      child: PageView(
                        controller: _pageController,
                        onPageChanged: (int page) {
                          setState(() {
                            _currentPage = page;
                          });
                        },
                        scrollDirection: Axis.horizontal,
                        children: adss2,
                      ),
                    ),
                    // Positioned(
                    //   bottom: 0,
                    //   right: 0,
                    //   left: 0,
                    //   child: Row(
                    //     mainAxisAlignment: MainAxisAlignment.center,
                    //     children: selectedPage2,
                    //   ),
                    // )
                  ],
                ),
              ),
              SizedBox(
                height: 10,
              ),
              SizedBox(
                child: Row(
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'حديثا',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Spacer(),
                    TextButton(
                      onPressed: () {
                        Navigator
                            .pushNamed(context, 'more_car');

                        },
                      child: Text(
                        'المزيد',
                        style: TextStyle(
                            color: Colors.green.shade700,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                  ],
                ),
                width: double.infinity,
              ),
              SizedBox(
                height: 195,
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestoreController().readCar(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        return GridView.builder(
                          scrollDirection: Axis.horizontal,
                          physics: BouncingScrollPhysics(),
                          clipBehavior: Clip.antiAlias,
                          itemCount: documents.length,
                          gridDelegate:
                          SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 1,
                            mainAxisSpacing: 5,
                          ),
                          itemBuilder: (context, index) {
                            // for(int i = 0; i < documents.length; i++){
                            //
                            // }
                            // ids.add(documents[index].id);
                            carId = documents[index].id;
                            // isFavorite();
                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            DetailsCarScreen(
                                                getCar(documents[index]))));
                              },
                              child: Card(
                                color: Colors.white,
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                clipBehavior: Clip.antiAlias,
                                child: Column(
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      height: 125,
                                      child: Image.network(
                                        List.from(
                                            documents[index].get('images'))[0],
                                        fit: BoxFit.cover,
                                        height: double.infinity,
                                        width: double.infinity,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          start: 10, end: 5, top: 0, bottom: 0),
                                      child: Container(
                                        color: Colors.white,
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Column(
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.center,
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.start,
                                                  textBaseline:
                                                  TextBaseline.ideographic,
                                                  children: [
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      documents[index]
                                                          .get('name'),
                                                      maxLines: 1,
                                                      overflow:
                                                      TextOverflow.ellipsis,
                                                      style: TextStyle(
                                                        fontSize: 17,
                                                        fontWeight:
                                                        FontWeight.bold,
                                                        textBaseline:
                                                        TextBaseline
                                                            .ideographic,
                                                      ),
                                                    ),
                                                    Text(
                                                      '${documents[index]
                                                          .get('price')
                                                          .toString()}\$',
                                                      style: TextStyle(
                                                          color:
                                                          Color(0xff026b28),
                                                          fontSize: 16,
                                                          fontWeight:
                                                          FontWeight.bold),
                                                    ),
                                                  ],
                                                ),
                                                Spacer(),
                                                // IconButton(
                                                //   onPressed: () async {
                                                //     if (favorite[index]) {
                                                //       await getFavoriteId();
                                                //     }
                                                //     print(index);
                                                //     await favoriteProses(
                                                //         documents[index],
                                                //         index);
                                                //   },
                                                //   icon: favorite[index] == true
                                                //       ? Icon(
                                                //     Icons.favorite,
                                                //     size: 23,
                                                //     color: Colors.red,
                                                //   )
                                                //       : Icon(
                                                //     Icons.favorite_border,
                                                //     size: 23,
                                                //     color: Colors.red,
                                                //   ),
                                                //   padding: EdgeInsetsDirectional
                                                //       .zero,
                                                //   alignment:
                                                //   AlignmentDirectional
                                                //       .centerEnd,
                                                // )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          //         );
                        );
                      } else if (snapshot.connectionState ==
                          ConnectionState.waiting) {
                        return Loading();
                      } else {
                        return NoData();
                      }
                    }),
              ),
              SizedBox(height: 10),
              SizedBox(
                child: Row(
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'المعارض',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Spacer(),
                    TextButton(
                      onPressed: () {
                        Navigator.pushNamed(context, 'more_marechent');

                        },
                      child: Text(
                        'المزيد',
                        style: TextStyle(
                            color: Colors.green.shade700,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                  ],
                ),
                width: double.infinity,
              ),
              SizedBox(
                height: 190,
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestoreController().readMerchant(),
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        return GridView.builder(
                          scrollDirection: Axis.horizontal,
                          physics: BouncingScrollPhysics(),
                          clipBehavior: Clip.antiAlias,
                          itemCount: documents.length,
                          gridDelegate:
                          SliverGridDelegateWithFixedCrossAxisCount(
                            crossAxisCount: 1,
                            mainAxisSpacing: 5,
                          ),
                          itemBuilder: (context, index) {
                            id = documents[index].id;
                            getMerchantData();
                            return GestureDetector(
                              onTap: () {
                                Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                        builder: (context) =>
                                            DetailsShopScreen(
                                                (documents[index]))));


                              },
                              child: Card(
                                color: Colors.white,
                                elevation: 2,
                                shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(10),
                                ),
                                clipBehavior: Clip.antiAlias,
                                child: Column(
                                  children: [
                                    Container(
                                      width: double.infinity,
                                      height: 120,
                                      child: Image.network(
                                        documents[index].get('image'),
                                        fit: BoxFit.cover,
                                        height: double.infinity,
                                        width: double.infinity,
                                      ),
                                    ),
                                    Padding(
                                      padding: EdgeInsetsDirectional.only(
                                          start: 10, end: 5, top: 0, bottom: 5),
                                      child: Container(
                                        color: Colors.white,
                                        child: Column(
                                          children: [
                                            Row(
                                              children: [
                                                Column(
                                                  mainAxisAlignment: MainAxisAlignment
                                                      .center,
                                                  crossAxisAlignment: CrossAxisAlignment
                                                      .start,
                                                  textBaseline: TextBaseline
                                                      .ideographic,
                                                  children: [
                                                    SizedBox(
                                                      height: 5,
                                                    ),
                                                    Text(
                                                      documents[index].get(
                                                          'marketName'),
                                                      maxLines: 1,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          fontSize: 15,
                                                          fontWeight: FontWeight
                                                              .bold,
                                                          textBaseline: TextBaseline
                                                              .ideographic),
                                                    ),
                                                    Text(
                                                      documents[index].get(
                                                          'place').toString(),
                                                      maxLines: 1,
                                                      overflow: TextOverflow
                                                          .ellipsis,
                                                      style: TextStyle(
                                                          color: Color(
                                                              0xff026b28),
                                                          fontSize: 15,
                                                          fontWeight: FontWeight
                                                              .bold,
                                                          textBaseline: TextBaseline
                                                              .ideographic),
                                                    ),
                                                  ],
                                                ),
                                                Spacer(),
                                                // IconButton(
                                                //   onPressed: () async {
                                                //     if (favorite[index]) {
                                                //       await getFavoriteId();
                                                //     }
                                                //     print(index);
                                                //     await favoriteProses(
                                                //         documents[index],
                                                //         index);
                                                //   },
                                                //   icon: favorite[index] == true
                                                //       ? Icon(
                                                //     Icons.favorite,
                                                //     size: 23,
                                                //     color: Colors.red,
                                                //   )
                                                //       : Icon(
                                                //     Icons.favorite_border,
                                                //     size: 23,
                                                //     color: Colors.red,
                                                //   ),
                                                //   padding: EdgeInsetsDirectional
                                                //       .zero,
                                                //   alignment: AlignmentDirectional
                                                //       .centerEnd,
                                                // )
                                              ],
                                            ),
                                          ],
                                        ),
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            );
                          },
                          //         );
                        );
                      } else if (snapshot.connectionState ==
                          ConnectionState.waiting) {
                        return Loading();
                      } else {
                        return NoData();
                      }
                    }),
              ),
              SizedBox(height: 10),
              SizedBox(
                child: Row(
                  children: [
                    SizedBox(
                      width: 10,
                    ),
                    Text(
                      'المحافظات',
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 16,
                      ),
                    ),
                    Spacer(),
                    TextButton(
                      onPressed: () {
                      Navigator.pushNamed(context, 'more_adders_Screenn');
                      },
                      child: Text(
                        'المزيد',
                        style: TextStyle(
                            color: Colors.green.shade700,
                            fontWeight: FontWeight.bold,
                            fontSize: 16),
                      ),
                    ),
                  ],
                ),
                width: double.infinity,
              ),
              SizedBox(
                height: 190,
                child: GridView.builder(
                  scrollDirection: Axis.horizontal,
                  physics: BouncingScrollPhysics(),
                  clipBehavior: Clip.antiAlias,
                  itemCount: addresses.length,
                  gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 1,
                    mainAxisSpacing: 5,
                  ),
                  itemBuilder: (context, index) {
                    return GestureDetector(
                      onTap: () {
                        if(index==0){
                          print('south');
                          Navigator.pushNamed(context, 'south_screen');
                        } if(index==1){
                          Navigator.pushNamed(context, 'north_screen');
                          print('north');
                        } if(index==2){
                          Navigator.pushNamed(context, 'west_screen');
                          print('west');
                        } if(index==3){
                          Navigator.pushNamed(context, 'east_screen');
                          print('east');
                        }
                        // Navigator.push(
                        //     context,
                        //     MaterialPageRoute(
                        //         builder: (context) => ContentScreen(
                        //             FirebaseFirestoreController().readProductByCategory(documents[index].id))));
                      },
                      child: showItemHomeAdders(
                        name: addresses[index],
                      ),
                    );
                  },
                  //         );
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  Future<void> getMerchantData() async {
    DocumentSnapshot ds =
    await FirebaseFirestoreController().getMerchantData(id);
    setState(() {
      documentSnapshot = ds;
    });
  }

  Car getCar(DocumentSnapshot snapshot) {
    Car car = Car();
    car.images = List.from(snapshot.get('images'));
    car.name = snapshot.get('name');
    car.price = snapshot.get('price');
    car.description = snapshot.get('description');
    car.model = snapshot.get('model');
    car.state = snapshot.get('state');
    car.path = snapshot.id;
    car.color = List.from(snapshot.get('color'));
    car.merchant = id;
    return car;
  }
  Merchant getMerchant(DocumentSnapshot snapshot) {
    Merchant car = Merchant();
    car.path = snapshot.id;
    return car;
  }
  // Future<void> isFavorite() async{
  //   List<bool> favoritee = <bool>[];
  //   for(int i = 0; i < ids.length; i++){
  //     bool state = await FirebaseFirestoreController().isFavorite(ids[i]);
  //     favoritee.add(state);
  //   }
  //   setState(() {
  //     favorite = favoritee;
  //   });
  // }

  // Future<void> isFavorite() async {
  //   bool state = await FirebaseFirestoreController().isFavorite(carId);
  //   favorite.add(state);
  // }

  // Future<void> addFavorite(DocumentSnapshot snapshot, int index) async {
  //   bool state = await FirebaseFirestoreController()
  //       .addFavorite(setCarFavorite(snapshot));
  //   if (state) {
  //     setState(() {
  //       favorite[index] = true;
  //     });
  //   }
  // }
  //
  // Future<void> deleteFavorite(int index) async {
  //   print('id = ' + id);
  //   bool state = await FirebaseFirestoreController().deleteFavorite(path: id);
  //   if (state) {
  //     setState(() {
  //       favorite[index] = false;
  //     });
  //   }
  // }
  //
  // Future<void> favoriteProses(DocumentSnapshot snapshot, int index) async {
  //   if (favorite[index]) {
  //     await deleteFavorite(index);
  //   } else {
  //     await addFavorite(snapshot, index);
  //   }
  // }
  //
  // Future<void> getFavoriteId() async {
  //   print(carId);
  //   String state = await FirebaseFirestoreController().getFavoriteId(carId);
  //   setState(() {
  //     id = state;
  //   });
  // }

  // Favorite setCarFavorite(DocumentSnapshot snapshot) {
  //   Favorite favorite = Favorite();
  //   favorite.name = snapshot.get('name');
  //   favorite.merchant = snapshot.get('merchant');
  //   favorite.color = List.from(snapshot.get('color'));
  //   favorite.state = snapshot.get('state');
  //   favorite.price = snapshot.get('price');
  //   favorite.description = snapshot.get('description');
  //   favorite.model = snapshot.get('model');
  //   favorite.images = List.from(snapshot.get('images'));
  //   favorite.email = FirebaseAuth.instance.currentUser!.email!;
  //   favorite.carId = snapshot.id;
  //   return favorite;
  // }

  Future<void> getUserName() async {
    String namee = await FirebaseFirestoreController().getName();
    setState(() {
      name = namee;
      print(name);
    });
  }

  Future<void> getMarchentName() async {
    String namee = await FirebaseFirestoreController().getMarchentName();
    setState(() {
      name2 = namee;
      print(name2);
    });
  }
  Future<void> signOut() async {
    await FirebaseAuth.instance.signOut();
    Navigator.pushReplacementNamed(context, 'login_screen');
  }
  Future<void> check() async {
    String state2 =
    await FirebaseFirestoreController().getBlockStateUser2(FirebaseAuth.instance.currentUser!.email.toString());
    if (state2 == '1') {
      showSnackBar(
          context: context, message: 'للأسف، تم حظر حسابك', error: true);
      print('go out');
      signOut();
    } else {
      print('opend');
    }
  }

  Future<void> getAllAds() async{
    List<Ads> ads = await FirebaseFirestoreController().getAllAds();
    for(int i = 0; i < ads.length; i++){
      adss.add(GestureDetector(
        onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => AdsDetailsScreen(ads[i]),));},
        child: adsWidgetShow(
          tittle: ads[i].name,
          des: ads[i].description,
          url: ads[i].image,
        ),
      ));
    }
    setState(() {
      adss2 = adss;
    });
  }

  Future<void> pageSelected() async{
    List<Ads> ads = await FirebaseFirestoreController().getAllAds();
    for(int i = 0; i < ads.length; i++){
      selectedPage.add(CurrentPage(selected: _currentPage == i));
    }
    setState(() {
      selectedPage2 = selectedPage;
    });
  }
}
