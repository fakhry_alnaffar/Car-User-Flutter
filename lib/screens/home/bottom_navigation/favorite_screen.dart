import 'package:car_user_project/firebase/firebase_auth_controller.dart';
import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/car.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:car_user_project/widgets/profile_item.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import '../details/detials_screen.dart';

class FavoriteScreen extends StatefulWidget {
  const FavoriteScreen({Key? key}) : super(key: key);

  @override
  _FavoriteScreenState createState() => _FavoriteScreenState();
}

class _FavoriteScreenState extends State<FavoriteScreen> {
  String id = '';
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'المفضلة',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Colors.white,
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: Colors.white,
      body: StreamBuilder<QuerySnapshot>(
        stream: FirebaseFirestoreController().readFavorite(),
        builder: (context, snapshot) {
          if(snapshot.hasData && snapshot.data!.docs.isNotEmpty){
            List<DocumentSnapshot> documents = snapshot.data!.docs;
            return Padding(
              padding: EdgeInsets.symmetric(horizontal: 15),
              child: GridView.builder(
                itemCount: documents.length,
                gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                    crossAxisCount: 2,
                    crossAxisSpacing: 5,
                    mainAxisSpacing: 5,
                    childAspectRatio: 100 / 115),
                itemBuilder: (context, index) {
                  id = documents[index].id;
                  return GestureDetector(
                    onTap: () {
                      Navigator.push(
                          context,
                          MaterialPageRoute(
                              builder: (context) =>
                                  DetailsCarScreen(getCar(documents[index]))));
                    },
                    child: Card(
                      clipBehavior: Clip.antiAlias,
                      color: Colors.white,
                      elevation: 2,
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                      child: Column(
                        children: [
                          Container(
                            width: double.infinity,
                            height: 140,
                            child: Image.network(
                              List.from(documents[index].get('images'))[0],
                              fit: BoxFit.cover,
                              height: double.infinity,
                              width: double.infinity,
                            ),
                          ),
                          Padding(
                            padding: EdgeInsets.symmetric(horizontal: 10),
                            child: Container(
                              child: Column(
                                children: [
                                  Row(
                                    children: [
                                      Column(
                                        mainAxisAlignment:
                                        MainAxisAlignment.center,
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        textBaseline:
                                        TextBaseline.ideographic,
                                        children: [
                                          SizedBox(
                                            height: 5,
                                          ),
                                          Text(
                                            documents[index].get('name'),
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                fontSize: 17,
                                                fontWeight: FontWeight.bold,
                                                textBaseline:
                                                TextBaseline.ideographic),
                                          ),
                                          Text(
                                            '${documents[index].get('price').toString()}\$',
                                            maxLines: 1,
                                            overflow: TextOverflow.ellipsis,
                                            style: TextStyle(
                                                color: Color(0xff026b28),
                                                fontSize: 16,
                                                fontWeight: FontWeight.bold,
                                                textBaseline:
                                                TextBaseline.ideographic),
                                          ),
                                        ],
                                      ),
                                      Spacer(),
                                      GestureDetector(
                                        onTap: () {
                                          deleteFavorite();
                                        },
                                        child: Icon(
                                          Icons.favorite,
                                          color: Colors.red,
                                        ),
                                      ),
                                    ],
                                  ),
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),
                  );
                },
              ),
            );
          } else if (FirebaseAuthController().isLoggedIn == false) {
            return ProfileSetting(
              title: 'تسجيل الدخول',
              firstIcon: Icons.login,
              onPressed: () {
                Navigator.pushReplacementNamed(context, 'login_screen');
              },
            );
          } else if (snapshot.connectionState == ConnectionState.waiting){
            return Loading();
          }else {
            return NoData();
          }
        }
      ),
    );
  }

  Future<void> deleteFavorite() async{
    await FirebaseFirestoreController().deleteFavorite(path: id);
  }

  Car getCar(DocumentSnapshot snapshot) {
    Car car = Car();
    car.images = List.from(snapshot.get('images'));
    car.name = snapshot.get('name');
    car.price = snapshot.get('price');
    car.description = snapshot.get('description');
    car.model = snapshot.get('model');
    car.state = snapshot.get('state');
    car.path = snapshot.get('carId');
    car.color = List.from(snapshot.get('color'));
    car.merchant = snapshot.get('merchant');
    return car;
  }

}
