import 'package:car_user_project/models/navgation_bottom.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'chat/chat_screen.dart';
import 'bottom_navigation/favorite_screen.dart';
import 'bottom_navigation/home_screen.dart';
import 'profile/edit_profile_screen.dart';
import 'bottom_navigation/profile_screen.dart';


class UserMainScreen extends StatefulWidget {
  const UserMainScreen({Key? key}) : super(key: key);

  @override
  _UserMainScreenState createState() => _UserMainScreenState();
}

class _UserMainScreenState extends State<UserMainScreen> {
  int _currentIndex = 0;

  final List<OnTabNavigation> list = <OnTabNavigation>[
    OnTabNavigation(screen: HomeScreen()),
    OnTabNavigation(screen:FavoriteScreen ()),
    OnTabNavigation(screen: ChatScreen()),
    OnTabNavigation(screen: ProfileScreen()),
  ];

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      bottomNavigationBar: BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        elevation: 0,
        selectedItemColor: Color(0xff1DB854),
        onTap: (int currentIndex){
          setState(() {
            _currentIndex = currentIndex;
          });
        },
        currentIndex: _currentIndex,
        items: [
          BottomNavigationBarItem(icon: Icon(Icons.home_outlined,), label: 'الرئيسية'),
          BottomNavigationBarItem(icon: Icon(Icons.favorite_border,), label: 'المفضلة'),
          BottomNavigationBarItem(icon: Icon(Icons.message_outlined,), label: 'الرسائل'),
          BottomNavigationBarItem(icon: Icon(Icons.person_outline_rounded,), label: 'الحساب'),
        ],
      ),
      body: list[_currentIndex].screen,
    );
  }
}