import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/favorite.dart';
import 'package:car_user_project/screens/home/details/shop_detials.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
class North extends StatefulWidget {
  const North({Key? key}) : super(key: key);

  @override
  _NorthState createState() => _NorthState();
}

class _NorthState extends State<North> {
  DocumentSnapshot? documentSnapshot;
  String id = '';
  List<bool> favorite = <bool>[];
  String carId = '';

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: Color(0xff1DB854),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
          iconTheme: IconThemeData(color: Colors.black),
          title: Text(
            'شمال غزة',
            style: TextStyle(color: Colors.black),
          ),
          backgroundColor: Colors.transparent,
          elevation: 0,
          centerTitle: true,
        ),
        backgroundColor: Colors.white,

        body: SafeArea(
          child: Padding(
            padding: EdgeInsets.symmetric(horizontal: 15, vertical: 15),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                SizedBox(
                  height: 15,
                ),
                StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestoreController().readMerchantNorth(),
                    builder: (context, snapshot) {
                      if(snapshot.hasData && snapshot.data!.docs.length > 0){
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        return Expanded(
                          child: GridView.builder(
                            scrollDirection: Axis.vertical,
                            physics: BouncingScrollPhysics(),
                            clipBehavior: Clip.antiAlias,
                            itemCount: documents.length,
                            gridDelegate:
                            SliverGridDelegateWithFixedCrossAxisCount(
                              crossAxisCount: 2,
                              mainAxisSpacing: 5,
                            ),
                            itemBuilder: (context, index) {
                              id = documents[index].id;
                              getMerchantData();
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DetailsShopScreen(
                                                  (documents[index]))));
                                },
                                child: Card(
                                  color: Colors.white,
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  clipBehavior: Clip.antiAlias,
                                  child: Column(
                                    children: [
                                      Container(
                                        width: double.infinity,
                                        height: 120,
                                        child: Image.network(
                                          documents[index].get('image'),
                                          fit: BoxFit.cover,
                                          height: double.infinity,
                                          width: double.infinity,
                                        ),
                                      ),
                                      Padding(
                                        padding: EdgeInsetsDirectional.only(
                                            start: 10, end: 5, top: 0, bottom: 5),
                                        child: Container(
                                          color: Colors.white,
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Column(
                                                    mainAxisAlignment: MainAxisAlignment
                                                        .center,
                                                    crossAxisAlignment: CrossAxisAlignment
                                                        .start,
                                                    textBaseline: TextBaseline
                                                        .ideographic,
                                                    children: [
                                                      Text(
                                                        documents[index].get(
                                                            'marketName'),
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            fontSize: 14,
                                                            fontWeight: FontWeight
                                                                .bold,
                                                            textBaseline: TextBaseline
                                                                .ideographic),
                                                      ),
                                                      Text(
                                                        documents[index].get(
                                                            'place').toString(),
                                                        maxLines: 1,
                                                        overflow: TextOverflow
                                                            .ellipsis,
                                                        style: TextStyle(
                                                            color: Color(
                                                                0xff026b28),
                                                            fontSize: 14,
                                                            fontWeight: FontWeight
                                                                .bold,
                                                            textBaseline: TextBaseline
                                                                .ideographic),
                                                      ),
                                                    ],
                                                  ),
                                                  Spacer(),
                                                  // IconButton(
                                                  //   onPressed: () async {
                                                  //     if (favorite[index]) {
                                                  //       await getFavoriteId();
                                                  //     }
                                                  //     print(index);
                                                  //     await favoriteProses(
                                                  //         documents[index],
                                                  //         index);
                                                  //   },
                                                  //   icon: favorite[index] == true
                                                  //       ? Icon(
                                                  //     Icons.favorite,
                                                  //     size: 23,
                                                  //     color: Colors.red,
                                                  //   )
                                                  //       : Icon(
                                                  //     Icons.favorite_border,
                                                  //     size: 23,
                                                  //     color: Colors.red,
                                                  //   ),
                                                  //   padding: EdgeInsetsDirectional
                                                  //       .zero,
                                                  //   alignment: AlignmentDirectional
                                                  //       .centerEnd,
                                                  // )
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                            //         );
                          ),
                        );
                      }else if(snapshot.connectionState == ConnectionState.waiting){
                        return Loading();
                      }else{
                        return Center(child: NoData());
                      }

                    }
                ),
              ],
            ),
          ),
        ));
  }
  Future<void> getMerchantData() async {
    DocumentSnapshot ds =
    await FirebaseFirestoreController().getMerchantData(id);
    setState(() {
      documentSnapshot = ds;
    });
  }
  Future<void> addFavorite(DocumentSnapshot snapshot, int index) async {
    bool state = await FirebaseFirestoreController()
        .addFavorite(setCarFavorite(snapshot));
    if (state) {
      setState(() {
        favorite[index] = true;
      });
    }
  }

  Favorite setCarFavorite(DocumentSnapshot snapshot) {
    Favorite favorite = Favorite();
    favorite.name = snapshot.get('name');
    favorite.merchant = snapshot.get('merchant');
    favorite.color = List.from(snapshot.get('color'));
    favorite.state = snapshot.get('state');
    favorite.price = snapshot.get('price');
    favorite.description = snapshot.get('description');
    favorite.model = snapshot.get('model');
    favorite.images = List.from(snapshot.get('images'));
    favorite.email = FirebaseAuth.instance.currentUser!.email!;
    favorite.carId = snapshot.id;
    return favorite;
  }
  Future<void> deleteFavorite(int index) async {
    print('id = ' + id);
    bool state = await FirebaseFirestoreController().deleteFavorite(path: id);
    if (state) {
      setState(() {
        favorite[index] = false;
      });
    }
  }

  Future<void> favoriteProses(DocumentSnapshot snapshot, int index) async {
    if (favorite[index]) {
      await deleteFavorite(index);
    } else {
      await addFavorite(snapshot, index);
    }
  }

  Future<void> getFavoriteId() async {
    print(carId);
    String state = await FirebaseFirestoreController().getFavoriteId(carId);
    setState(() {
      id = state;
    });
  }
}
