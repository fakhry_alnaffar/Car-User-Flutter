import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/car.dart';
import 'package:car_user_project/models/cart.dart';
import 'package:car_user_project/models/favorite.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/screens/home/details/shop_detials.dart';
import 'package:car_user_project/utils/helpers.dart';
import 'package:car_user_project/widgets/app_text_filed.dart';
import 'package:car_user_project/widgets/app_text_filed_des.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/current_page.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

class DetailsCarScreen extends StatefulWidget {
  Car car;

  DetailsCarScreen(this.car);

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsCarScreen> with Helpers {
  String colorss = '';
  DocumentSnapshot? documentSnapshot;
  DocumentSnapshot? documentSnapshotUser;
  IconData icon = Icons.favorite_outline_rounded;
  bool selectedIcon = false;
  bool favorite = false;
  int _currentPage = 0;
  String image1 = '';
  String image2 = '';
  String image3 = '';
  String type = '';
  String id = '';
  late PageController _pageController;
  late TextEditingController _name;
  late TextEditingController _model;
  late TextEditingController _color;
  late TextEditingController _description;
  late TextEditingController _price;
  late TextEditingController _type ;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getSelectedColor();
    getMerchantData();
    isFavorite();
    getUserType();
    // getFavoriteId();
    // getUserData();
    image1 = widget.car.images[0];
    image2 = widget.car.images[1];
    image3 = widget.car.images[2];
    _pageController = PageController();
    _name = TextEditingController(text: widget.car.name);
    _model = TextEditingController(text: widget.car.model);
    _description = TextEditingController(text: widget.car.description);
    _price = TextEditingController(text: widget.car.price.toString());
    _type = TextEditingController(text: widget.car.state);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController.dispose;
    _name.dispose();
    _model.dispose();
    //_color.dispose();
    _description.dispose();
    _price.dispose();
    _type.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8,vertical: 10),
            child: IconButton(
              onPressed: ()async  {
                if(favorite){
                  await getFavoriteId();
                }
                await favoriteProses();
              },
              icon: favorite == true ? Icon(
                Icons.favorite,
                size: 28,
                color: Colors.red,
              ) : Icon(
                Icons.favorite_border,
                size: 28,
              ),
              padding: EdgeInsetsDirectional.zero,
              alignment: AlignmentDirectional.centerEnd,
            ),
          ),
        ],
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          widget.car.name,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color(0xffF1F2F3),
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: Color(0xffF1F2F3),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 7),
        child: Column(
          children: [
            Expanded(
              flex: 8,
              child: SingleChildScrollView(
                physics: BouncingScrollPhysics(),
                clipBehavior: Clip.antiAliasWithSaveLayer,
                child: Column(
                  children: [
                    SizedBox(
                      width: double.infinity,
                      height: 220,
                      child: Stack(
                        children: [
                          PageView(
                            controller: _pageController,
                            onPageChanged: (int page) {
                              setState(() {
                                _currentPage = page;
                              });
                            },
                            scrollDirection: Axis.horizontal,
                            children: [
                              Image.network(
                                image1,
                                fit: BoxFit.cover,
                                height: double.infinity,
                              ),
                              Image.network(
                                image2,
                                fit: BoxFit.cover,
                                height: double.infinity,
                              ),
                              Image.network(
                                image3,
                                fit: BoxFit.cover,
                                height: double.infinity,
                              ),
                            ],
                          ),
                          Align(
                            alignment: Alignment.bottomCenter,
                            child: Container(
                                alignment: AlignmentDirectional.centerEnd,
                                height:40,
                                color: Colors.black45,
                                child: Padding(
                                  padding:
                                  const EdgeInsets.symmetric(horizontal: 10),
                                  child: Column(
                                    mainAxisAlignment: MainAxisAlignment.center,
                                    crossAxisAlignment:
                                    CrossAxisAlignment.stretch,
                                    textDirection: TextDirection.rtl,
                                    children: [
                                      Text(
                                        '',
                                        style: TextStyle(
                                            fontSize: 18,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xff012911)),
                                      ),
                                      Text(
                                        '',
                                        style: TextStyle(
                                            fontSize: 16,
                                            fontWeight: FontWeight.bold,
                                            color: Color(0xff2f2f2f)),
                                      ),
                                    ],
                                  ),
                                )),
                          ),
                          Positioned(
                            bottom: 10,
                            right: 0,
                            left: 0,
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: [
                                CurrentPage(selected: _currentPage == 0),
                                SizedBox(width: 10),
                                CurrentPage(selected: _currentPage == 1),
                                SizedBox(width: 10),
                                CurrentPage(selected: _currentPage == 2),
                              ],
                            ),
                          ),

                        ],
                      ),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    AppTextFiled(
                      labelText: 'اسم السيارة',
                      controller: _name,
                      readOnly: true,
                      showCursor: false,
                      prefix: Icons.directions_car_outlined,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    AppTextFiled(
                      labelText: 'الموديل',
                      controller: _model,
                      readOnly: true,
                      showCursor: false,
                      prefix: Icons.model_training,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    AppTextFiled(
                      labelText: 'اللون',
                      controller: TextEditingController(text: colorss),
                      prefix: Icons.color_lens_outlined,
                      readOnly: true,
                      showCursor: false,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    AppTextFiled(
                      labelText: 'السعر',
                      textInputType: TextInputType.number,
                      controller: _price,
                      prefix: Icons.price_change_rounded,
                      readOnly: true,
                      showCursor: false,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    AppTextFiled(
                      labelText: 'الحالة',
                      controller: _type,
                      showCursor: false,
                      readOnly: true,
                      onTap: (){
                        //SHOW BOOTM SHEET METHOD
                      },
                      prefix: Icons.autorenew,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    AppTextFiledDescription(
                      maxLe: 5,
                      labelText: 'وصف',
                      controller: _description,
                      prefix: Icons.task_rounded,
                      readOnly: true,
                      showCursor: false,
                    ),
                  ],
                ),
              ),
            ),
            Expanded(
              flex: 1,
              child: Container(
                color: Colors.transparent,

                child:Row(children: [
                  Container(
                    margin: EdgeInsetsDirectional.only(
                        start: 0, bottom: 0, end: 0, top: 0),
                  ),
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {documentSnapshot != null ? Navigator.push(context, MaterialPageRoute(builder: (context) => DetailsShopScreen(documentSnapshot!),)) : print('waite');},
                      child: Text('المعرض'),
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(double.infinity,
                            SizeConfig().scaleHeight(60)),
                        primary: Color(0xff1DB854),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 20,
                  ),
                  Expanded(
                    child: ElevatedButton(
                      onPressed: () {
                        addCart();
                        showDialogWidget(context2: context,
                            message: 'تم إرسال طلبك بنجاح',
                            sub: 'يرجى تتبع الطلب عبر رسائل التطبيق',
                            type: 'رجوع',
                            function: (){
                              Navigator.pop(context);
                            }
                        );
                        },
                      child: Text('ارسل طلب'),
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(double.infinity,
                            SizeConfig().scaleHeight(60)),
                        primary: Color(0xff1DB854),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(25),
                        ),
                      ),
                    ),
                  ),
                ]),
              ),
            )
          ],
        ),
      ),
    );
  }

  void getSelectedColor() {
    String name = '';
    for(int i = 0; i < widget.car.color.length; i++){
      name += widget.car.color[i] + ' ';
    }
    setState(() {
      colorss = name;
    });
  }

  Future<void> addCart() async{
    await getUserData();
    bool state = await FirebaseFirestoreController().addCart(getCart());
    if(state){
      // showSnackBar(context: context, message: 'تم ارسال طلبك بنجاح', error: false);
    }
  }

  Cart getCart(){
    Cart cart = Cart();
    cart.carName = widget.car.name;
    cart.merchant = widget.car.merchant;
    cart.color = widget.car.color;
    cart.state = widget.car.state;
    cart.price = widget.car.price;
    cart.description = widget.car.description;
    cart.model = widget.car.model;
    cart.images = widget.car.images;
    cart.carId = widget.car.path;
    cart.email = FirebaseAuth.instance.currentUser!.email!;
    cart.image = documentSnapshotUser!.get('image');
    cart.name = documentSnapshotUser!.get('name');
    print(documentSnapshotUser!.get('name'));
    cart.phoneNumber = documentSnapshotUser!.get('phoneNumber');
    cart.place = documentSnapshotUser!.get('place');
    return cart;
  }

  Favorite setCarFavorite(){
    Favorite favorite = Favorite();
    favorite.name = widget.car.name;
    favorite.merchant = widget.car.merchant;
    favorite.color = widget.car.color;
    favorite.state = widget.car.state;
    favorite.price = widget.car.price;
    favorite.description = widget.car.description;
    favorite.model = widget.car.model;

    favorite.images = widget.car.images;
    favorite.email = FirebaseAuth.instance.currentUser!.email!;
    favorite.carId = widget.car.path;
    return favorite;
  }

  Future<void> isFavorite() async{
    bool state = await FirebaseFirestoreController().isFavorite(widget.car.path);
    setState(() {
      favorite = state;
    });
  }

  Future<void> addFavorite() async{
    bool state = await FirebaseFirestoreController().addFavorite(setCarFavorite());
    if(state){
      setState(() {
        favorite = true;
      });
    }
  }

  Future<void> deleteFavorite() async{
    print('id = ' + id);
    bool state = await FirebaseFirestoreController().deleteFavorite(path: id);
    if(state){
      setState(() {
        favorite = false;
      });
    }
  }

  Future<void> favoriteProses() async{
    if(favorite){
      await deleteFavorite();
    }else{
      await addFavorite();
    }
  }

  Future<void> getFavoriteId() async{
    String state = await FirebaseFirestoreController().getFavoriteId(widget.car.path);

    setState(() {
      id = state;
    });
  }

  Future<void> getMerchantData() async{
    DocumentSnapshot ds = await FirebaseFirestoreController().getMerchantData(widget.car.merchant);
    setState(() {
      documentSnapshot = ds;
    });
  }

  Future<void> getUserData() async{
    DocumentSnapshot ds = await FirebaseFirestoreController().getUserData(type);
    print('snapshot = ' + ds.toString());
    setState(() {
      documentSnapshotUser = ds;
    });
  }

  Future<void> getUserType() async{
    String ds = await FirebaseFirestoreController().getUserType();
    print(ds);
    setState(() {
      type = ds;
    });
  }

}