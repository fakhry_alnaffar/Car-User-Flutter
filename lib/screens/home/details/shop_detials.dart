import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/car.dart';
import 'package:car_user_project/models/user.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

import '../chat/chat_detailes_screen.dart';
import 'detials_screen.dart';

class DetailsShopScreen extends StatefulWidget {
  DocumentSnapshot snapshot;
  DetailsShopScreen(this.snapshot);

  @override
  _DetailsShopScreenState createState() => _DetailsShopScreenState();
}

class _DetailsShopScreenState extends State<DetailsShopScreen> {
  IconData icon = Icons.favorite_outline_rounded;
  bool selectedIcon = false;
  late PageController _pageController;
  int _currentPage = 0;
  String type = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _pageController = PageController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController.dispose;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      appBar: AppBar(
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16, vertical: 10),
            child: IconButton(
              onPressed: () {
                //add to fav
                setState(() {
                  if (selectedIcon == false) {
                    icon = Icons.favorite_rounded;
                    selectedIcon = true;
                    print('object');
                  } else if (selectedIcon == true) {
                    icon = Icons.favorite_outline_rounded;
                    selectedIcon = false;
                  }
                });
              },
              icon: Icon(
                icon,
                size: 28,
                color: Colors.red,
              ),
              padding: EdgeInsetsDirectional.zero,
              alignment: AlignmentDirectional.centerEnd,
            ),
          ),
        ],
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          widget.snapshot.get('marketName'),
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color(0xffF1F2F3),
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: Color(0xffF1F2F3),
      body: SafeArea(
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 220,
                child: Container(
                  margin:
                  EdgeInsetsDirectional.only(start: 8, end: 8, bottom: 10),
                  clipBehavior: Clip.antiAlias,
                  decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(10)),
                  child: PageView(
                    controller: _pageController,
                    onPageChanged: (int page) {
                      setState(() {
                        _currentPage = page;
                      });
                    },
                    scrollDirection: Axis.horizontal,
                    children: [
                      Image.network(
                        widget.snapshot.get('image'),
                        fit: BoxFit.cover,
                        height: double.infinity,
                        width: double.infinity,
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: 50,
                child: SingleChildScrollView(
                  scrollDirection: Axis.horizontal,
                  child: Center(
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: [
                        SizedBox(
                          width: 5,
                        ),
                        IconButton(
                            onPressed: () {launch("tel://${widget.snapshot.get('phoneNumber')}");},

                            icon: Icon(
                              Icons.call,
                              color: Color(0xff1DB854),
                            )),
                        IconButton(
                            onPressed: () {
                              Navigator.push(
                                  context,
                                  MaterialPageRoute(
                                    builder: (context) =>
                                        ChatScreenDetails(getUser()),
                                  ));
                            },
                            icon: Icon(
                              Icons.message,
                              color: Color(0xff1DB854),
                            )),
                        Icon(
                          Icons.location_on_outlined,
                          color: Color(0xff1DB854),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Text(
                          widget.snapshot.get('place'),
                          style:
                          TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
                        ),
                        SizedBox(
                          width: 5,
                        ),
                      ],
                    ),
                  ),
                ),
              ),
              SizedBox(
                height: 15,
              ),
              SizedBox(
                height: 40,
                child: Row(
                  children: [
                    SizedBox(
                      width: 16,
                    ),
                    Container(
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(20),
                      ),
                      child: Row(
                        children: [
                          SizedBox(
                            width: 5,
                          ),
                          TextButton(
                            onPressed: () {
                              showNameBottomSheet();
                            },
                            child: Text(
                              type != '' ? type : 'تحديد نوع السيارة',
                              style: TextStyle(
                                  color: Colors.black,
                                  fontWeight: FontWeight.w300),
                            ),
                          ),
                          Icon(
                            Icons.arrow_drop_down,
                            color: Colors.grey,
                          ),
                          SizedBox(
                            width: 5,
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
              Expanded(
                child: StreamBuilder<QuerySnapshot>(
                    stream: type != '' ? FirebaseFirestoreController().searchByName(type) : FirebaseFirestoreController().readMerchantCar(widget.snapshot.id),
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        return Container(
                          margin: EdgeInsets.only(
                              bottom: 10, left: 8, right: 8, top: 10),
                          child: GridView.builder(
                            padding: EdgeInsetsDirectional.zero,
                            clipBehavior: Clip.antiAlias,
                            physics: BouncingScrollPhysics(),
                            itemCount: documents.length,
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisSpacing: 15,
                                crossAxisSpacing: 10),
                            itemBuilder: (context, index) {
                              return GestureDetector(
                                onTap: () {
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DetailsCarScreen(
                                                  getCar(documents[index]))));
                                },
                                child: Card(
                                  clipBehavior: Clip.antiAlias,
                                  color: Colors.white,
                                  elevation: 2,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10),
                                  ),
                                  child: Column(
                                    children: [
                                      Container(
                                        width: double.infinity,
                                        height: 120,
                                        child: Image.network(
                                          List.from(
                                              documents[index].get(
                                                  'images'))[0],
                                          fit: BoxFit.cover,
                                          height: double.infinity,
                                          width: double.infinity,
                                        ),
                                      ),
                                      Padding(
                                        padding:
                                        EdgeInsets.symmetric(horizontal: 10),
                                        child: Container(
                                          child: Column(
                                            children: [
                                              Row(
                                                children: [
                                                  Column(
                                                    mainAxisAlignment:
                                                    MainAxisAlignment.center,
                                                    crossAxisAlignment:
                                                    CrossAxisAlignment.start,
                                                    textBaseline:
                                                    TextBaseline.ideographic,
                                                    children: [
                                                      SizedBox(
                                                        height: 5,
                                                      ),
                                                      Text(
                                                        documents[index]
                                                            .get('name'),
                                                        maxLines: 1,
                                                        overflow:
                                                        TextOverflow.ellipsis,
                                                        style: TextStyle(
                                                            fontSize: 17,
                                                            fontWeight:
                                                            FontWeight.bold,
                                                            textBaseline:
                                                            TextBaseline
                                                                .ideographic),
                                                      ),
                                                      Text(
                                                        '${documents[index]
                                                            .get('price')
                                                            .toString()}\$',
                                                        maxLines: 1,
                                                        overflow:
                                                        TextOverflow.ellipsis,
                                                        style: TextStyle(
                                                            color:
                                                            Color(0xff026b28),
                                                            fontSize: 16,
                                                            fontWeight:
                                                            FontWeight.bold,
                                                            textBaseline:
                                                            TextBaseline
                                                                .ideographic),
                                                      ),
                                                    ],
                                                  ),
                                                  Spacer(),
                                                  GestureDetector(
                                                    onTap: () {
                                                      // deleteFavorite();
                                                    },
                                                    child: Icon(
                                                      Icons.favorite,
                                                      color: Colors.red,
                                                    ),
                                                  ),
                                                ],
                                              ),
                                            ],
                                          ),
                                        ),
                                      )
                                    ],
                                  ),
                                ),
                              );
                            },
                          ),
                        );
                      } else if (snapshot.connectionState ==
                          ConnectionState.waiting) {
                        return Loading();
                      } else {
                        return NoData();
                      }
                    }),
              ),
            ],
          )),
    );
  }

  Car getCar(DocumentSnapshot snapshot) {
    Car car = Car();
    car.images = List.from(snapshot.get('images'));
    car.name = snapshot.get('name');
    car.price = snapshot.get('price');
    car.description = snapshot.get('description');
    car.model = snapshot.get('model');
    car.state = snapshot.get('state');
    car.path = snapshot.id;
    car.color = List.from(snapshot.get('color'));
    return car;
  }

  Users getUser() {
    Users users = Users();
    users.email = widget.snapshot.get('email');
    users.image = widget.snapshot.get('image');
    users.name = widget.snapshot.get('name');
    users.phoneNumber = widget.snapshot.get('phoneNumber');
    return users;
  }

  void showNameBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return StreamBuilder<QuerySnapshot>(
          stream: FirebaseFirestoreController().readCarName(),
          builder: (context, snapshot) {
            if(snapshot.hasData){
              List<DocumentSnapshot> documents = snapshot.data!.docs;
              return Padding(
                padding: EdgeInsets.symmetric(vertical: 15),
                child: ListView.builder(
                  itemCount: documents.length,
                  itemBuilder: (context, index) {
                    return RadioListTile(
                      title: Text(documents[index].get('name')),
                      value: documents[index].get('name').toString(),
                      groupValue: type,
                      onChanged: (String? value) {
                        if (value != null)
                          setState( () {
                            type = value;
                            Navigator.pop(context);
                          },
                          );
                      },
                    );
                  },
                ),
              );
            }else{
              return Container();
            }
          },
        );
      },

    );
  }

}
