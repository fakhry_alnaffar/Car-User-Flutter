import 'package:car_user_project/preferences/student_pref.dart';
import 'package:car_user_project/utils/helpers.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class MapScreen extends StatefulWidget {
  const MapScreen({Key? key}) : super(key: key);

  @override
  _MapScreenState createState() => _MapScreenState();
}

class _MapScreenState extends State<MapScreen>with Helpers {

  late CameraPosition _cameraPosition;
  late GoogleMapController _googleMapController;
  Set<Marker> _marker = <Marker>[].toSet();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _cameraPosition = CameraPosition(target: LatLng(31.350556, 34.452679), zoom: 16);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Map'),
        actions: [
          IconButton(onPressed: (){showSnackBar(context: context, message: 'تم تحديد العنوان'); Navigator.pop(context);}, icon: Icon(Icons.check))
        ],
      ),
      body: GoogleMap(
        myLocationEnabled: true,
        myLocationButtonEnabled: true,
        initialCameraPosition: _cameraPosition,
        mapType: MapType.satellite,
        onTap: (LatLng latLng){
          print(latLng);
          addMarker(latLng);
          AppPreferences().setPlace(latLng.latitude.toString() + ',' + latLng.longitude.toString());
          _googleMapController.animateCamera(CameraUpdate.newCameraPosition(CameraPosition(target: latLng, zoom: 16)));
        },
        onMapCreated: (GoogleMapController controller){
          _googleMapController = controller;
        },
        markers: _marker,
      ),
    );
  }

  void addMarker(LatLng latLng){
    _marker.clear();
    var marker = Marker(markerId: MarkerId('Marker ${_marker.length}'), position: latLng, infoWindow: InfoWindow(title: 'Marker'));
    setState(() {
      _marker.add(marker);
    });
  }

}
