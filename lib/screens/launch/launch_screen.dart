import 'package:car_user_project/firebase/firebase_auth_controller.dart';
import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:flutter/material.dart';

class LaunchScreen extends StatefulWidget {

  @override
  _LaunchScreenState createState() => _LaunchScreenState();
}

class _LaunchScreenState extends State<LaunchScreen> {
  bool active=false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    singInProses();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Spacer(),

            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: [
                Container(
                    margin:
                        EdgeInsets.only(bottom: SizeConfig().scaleHeight(0)),
                    child: Image(
                      image: AssetImage("images/carlogo.png"),
                    )),
              ],
            ),
            Text(
              'سيارتي',
              style: TextStyle(
                fontSize: 28,
                fontWeight: FontWeight.bold,
                color: Colors.grey,
                letterSpacing: 2,
                wordSpacing: 2,
              ),
            ),
            Spacer(),
            Visibility(
              visible: active,
              child: Text(
                'تعذر استخدام التطبيق',
                style: TextStyle(
                  fontSize: 28,
                  fontWeight: FontWeight.bold,
                  color: Colors.red.shade800,
                  letterSpacing: 2,
                  wordSpacing: 2,
                ),
              ),
            ),
SizedBox(height: 40,)
          ],
        ),
      ),
    );
  }
  Future<void> singInProses() async{
    String state = await FirebaseFirestoreController().getActive();
    if(state == '1'){
      setState(() {
        active = false;
      });
      Future.delayed(Duration(seconds: 3), () {
        var route;
        if(FirebaseAuthController().isLoggedIn){
          route = 'user_main_screen';
        }else{
          route = 'login_screen';
        }
        Navigator.pushReplacementNamed(context, route);
      });
    }else if (state == '0'){
      setState(() {
        active = true;
      });
    }
  }
}
