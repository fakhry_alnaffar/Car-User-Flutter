import 'package:car_user_project/firebase/firebase_auth_controller.dart';
import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/screens/auth/agreement_user_screen.dart';
import 'package:car_user_project/utils/helpers.dart';
import 'package:car_user_project/widgets/app_text_filed.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> with Helpers {
  late TextEditingController _email;
  late TextEditingController _password;
  late bool isPassword = true;
  String ar = '';
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _email = TextEditingController();
    _password = TextEditingController();
    getUserAgreement();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _email.dispose();
    _password.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      body: SingleChildScrollView(
        clipBehavior: Clip.antiAlias,
        padding: EdgeInsetsDirectional.zero,
        physics: BouncingScrollPhysics(),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            SafeArea(
              child: Row(
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: IconButton(
                      onPressed: () {
                        Navigator.pushReplacementNamed(context, 'user_main_screen');
                      },
                      icon: Icon(
                        Icons.cancel_outlined,
                        size: 30,
                        color: Colors.grey,
                      ),
                    ),
                  )
                ],
              ),
            ),
            SizedBox(
              height: 30,
            ),
            Row(
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: [
                Container(
                    margin:
                        EdgeInsets.only(bottom: SizeConfig().scaleHeight(0)),
                    child: Image(
                      image: AssetImage("images/logologin.png"),
                    )),
              ],
            ),
            Text(
              'سيارتي',
              style: TextStyle(
                fontSize: 24,
                fontWeight: FontWeight.bold,
                color: Colors.grey,
                letterSpacing: 2,
                wordSpacing: 2,
              ),
            ),
            SizedBox(
              height: 40,
            ),
            Center(
              child: Padding(
                padding: const EdgeInsets.symmetric(horizontal: 35),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: [
                    AppTextFiled(
                      labelText: 'البريد الالكتروني\n',
                      controller: _email,
                      textInputType: TextInputType.emailAddress,
                      prefix: Icons.email,
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    AppTextFiled(
                      labelText: 'كلمة المرور\n',
                      controller: _password,
                      obscureText: isPassword,
                      prefix: Icons.lock,
                      suffix: isPassword
                          ? Icons.visibility
                          : Icons.visibility_off,
                      functionSuffixPressed: () {
                        setState(() {
                          isPassword = !isPassword;
                        });
                      },
                    ),
                    SizedBox(
                      height: SizeConfig().scaleHeight(30),
                    ),
                    ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize: Size(
                            double.infinity, SizeConfig().scaleHeight(60)),
                        primary: Color(0xff1DB854),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(50),
                        ),
                      ),
                      onPressed: () {
                        signIn();
                      },
                      child: Text(
                        'تسجيل الدخول',
                        style: TextStyle(
                            fontSize: SizeConfig().scaleTextFont(18)),
                      ),
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: [
                        Text('مستخدم جديد؟'),
                        TextButton(
                          onPressed: () {
                            Navigator.pushNamed(context, 'select_sign');
                          },
                          child: Text(
                            'انشاء حساب',
                            style: TextStyle(color: Color(0xff1DB854)),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(220),
            ),
            Align(
              alignment: FractionalOffset.bottomCenter,
              child: Container(
                alignment: AlignmentDirectional.bottomCenter,
                child: TextButton(
                  onPressed: () {
                    Navigator.push(
                        context, MaterialPageRoute(builder: (context)=>AgreementUserScreen(ar)));
                  },
                  child: Text(
                    'اتفاقية الاستخدام',
                    style: TextStyle(
                      color: Color(0xff1DB854),
                    ),
                  ),
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  bool checkData(){
    if(_email.text.isNotEmpty && _password.text.isNotEmpty){
      return true;
    }else{
      return false;
    }
  }

  Future<void> signIn() async{
    if(checkData()){
      await singInProses();
    }
  }

  Future<void> singInProses() async{
    String state = await FirebaseFirestoreController().getMerchantState(_email.text);
    String state2 = await FirebaseFirestoreController().getBlockStateMerchant(_email.text);
    if(state == '1'){
      bool statee = await FirebaseAuthController().signIn(context, email: _email.text, password: _password.text);
      if(statee){
        if(state2=='1'){
          showSnackBar(context: context, message: 'للأسف، تم حظر حسابك', error: true);
        }else{
          Navigator.pushReplacementNamed(context, 'user_main_screen');
        }
      }
    }else if(state == 'للاسف، تم رفض طلبك'){
      bool stateee = await FirebaseFirestoreController().getUserState(_email.text);
      String state2 = await FirebaseFirestoreController().getBlockStateUser(_email.text);
      if(stateee){
        bool statee = await FirebaseAuthController().signIn(context, email: _email.text, password: _password.text);
        if(statee){
          if(state2=='1'){
            showSnackBar(context: context, message: 'للأسف، تم حظر حسابك', error: true);
          }else{
            Navigator.pushReplacementNamed(context, 'user_main_screen');
          }
        }
      }else{
        showSnackBar(context: context, message: state, error: true);
      }
    }
    else{
      showSnackBar(context: context, message: 'لم يتم قبول طلبك حتى الان، يرجى الانتظار', error: true);
    }
  }
  Future<void> getUserAgreement() async{
    String data = await FirebaseFirestoreController().getUserAgreement();
    setState(() {
      ar = data;
    });
  }
}
