import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/car.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/screens/controller_panel/car/add_car.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'detials_car_controller_screen.dart';

class HomeController extends StatefulWidget {
  String url;

  HomeController(this.url);

  @override
  _HomeControllerState createState() => _HomeControllerState();
}

class _HomeControllerState extends State<HomeController> {
  int _currentPage = 0;
  bool selected = false;
  late PageController _pageController;
  IconData icon = Icons.favorite_outline_rounded;
  bool selectedIcon = false;
  String id = '';
  String image = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMerchantId();
    getMerchantId2();
    _pageController = PageController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _pageController.dispose;
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      backgroundColor: Color(0xffF1F2F3),
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                Navigator.push(
                    context,
                    MaterialPageRoute(
                      builder: (context) => AddCarScreen(null),
                    ));
              },
              icon: Icon(
                Icons.add_circle_rounded,
                color: Color(0xff1DB854),
                size: 30,
              )),
          SizedBox(
            width: 5,
          ),
        ],
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'المعرض',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
          child: Column(
            children: [
              SizedBox(
                height: 10,
              ),
              SizedBox(
                height: 220,
                child: Container(
                  margin:
                  EdgeInsetsDirectional.only(start: 14, end: 14, bottom: 10),
                  clipBehavior: Clip.antiAlias,
                  decoration:
                  BoxDecoration(borderRadius: BorderRadius.circular(10)),
                  child: PageView(
                    controller: _pageController,
                    onPageChanged: (int page) {
                      setState(() {
                        _currentPage = page;
                      });
                    },
                    scrollDirection: Axis.horizontal,
                    children: [
                      Image.network(
                        widget.url,
                        fit: BoxFit.cover,
                        height: double.infinity,
                        width: double.infinity,
                      ),
                    ],
                  ),
                ),
              ),
              Expanded(
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestoreController().readCustomCar(id),
                    builder: (context, snapshot) {
                      if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                        List<DocumentSnapshot> documents = snapshot.data!.docs;
                        return Container(
                          margin: EdgeInsets.only(
                              bottom: 10, left: 14, right: 14, top: 0),
                          child: GridView.builder(
                            padding: EdgeInsetsDirectional.zero,
                            clipBehavior: Clip.antiAlias,
                            physics: BouncingScrollPhysics(),
                            itemCount: documents.length,
                            gridDelegate: SliverGridDelegateWithFixedCrossAxisCount(
                                crossAxisCount: 2,
                                mainAxisSpacing: 15,
                                crossAxisSpacing: 10),
                            itemBuilder: (context, index) {
                              return InkWell(
                                onTap: () {
                                  print('clicked');
                                  Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) =>
                                              DetailsCarScreenConroller(
                                                  getCar(documents[index]))));
                                },
                                child: showItemHomeController(
                                  name: documents[index].get('name'),
                                  adders: documents[index].get('price').toString() +
                                      '\$',
                                  widget: IconButton(
                                    onPressed: () async {
                                      //add to fav
                                      setState(() {
                                        if (selectedIcon == false) {
                                          icon = Icons.favorite_rounded;
                                          selectedIcon = true;
                                        } else if (selectedIcon == true) {
                                          icon = Icons.favorite_outline_rounded;
                                          selectedIcon = false;
                                        }
                                      });
                                    },
                                    icon: Icon(
                                      icon,
                                      size: 22,
                                      color: Colors.red,
                                    ),
                                    padding: EdgeInsetsDirectional.zero,
                                    alignment: AlignmentDirectional.centerEnd,
                                  ),
                                  image:
                                  List.from(documents[index].get('images'))[0],
                                  onPressedDelete: () async {
                                    showDialog(
                                        barrierColor:
                                        Colors.black.withOpacity(0.16),
                                        context: context,
                                        builder: (context) {
                                          return AlertDialog(
                                            backgroundColor: Color(0xff1DB854),
                                            clipBehavior: Clip.antiAlias,
                                            contentPadding:
                                            EdgeInsetsDirectional.zero,
                                            actionsPadding:
                                            EdgeInsetsDirectional.zero,
                                            title: Center(
                                                child: Text(
                                                  'هل انت متاكد ؟',
                                                  style: TextStyle(color: Colors.white),
                                                )),
                                            actions: [
                                              Container(
                                                alignment: AlignmentDirectional
                                                    .bottomStart,
                                                margin: EdgeInsetsDirectional.only(
                                                    start: 0,
                                                    end: 0,
                                                    bottom: 0,
                                                    top: 0),
                                                padding: EdgeInsetsDirectional.zero,
                                                height: 40,
                                                color: Colors.white70,
                                                child: Stack(children: [
                                                  Column(
                                                    children: [
                                                      SizedBox(
                                                        height: 40,
                                                        child: Align(
                                                          alignment: Alignment
                                                              .bottomCenter,
                                                          child: Container(
                                                            alignment:
                                                            AlignmentDirectional
                                                                .centerEnd,
                                                            height: 50,
                                                            padding:
                                                            EdgeInsetsDirectional
                                                                .zero,
                                                            width: double.infinity,
                                                            color:
                                                            Colors.transparent,
                                                            child: Row(children: [
                                                              Expanded(
                                                                child: TextButton(
                                                                  onPressed:
                                                                      () async {
                                                                    Navigator.pop(
                                                                        context);
                                                                    await deleteCar(documents[index]);
                                                                  },
                                                                  child: Text(
                                                                    'نعم',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontWeight:
                                                                        FontWeight
                                                                            .bold,
                                                                        fontSize:
                                                                        18),
                                                                  ),
                                                                  style:
                                                                  ElevatedButton
                                                                      .styleFrom(
                                                                    minimumSize: Size(
                                                                        double
                                                                            .infinity,
                                                                        SizeConfig()
                                                                            .scaleHeight(
                                                                            60)),
                                                                    primary: Color(
                                                                        0xff1DB854),
                                                                    shape:
                                                                    RoundedRectangleBorder(
                                                                      borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                          0),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                              SizedBox(
                                                                width: 1,
                                                              ),
                                                              Expanded(
                                                                child: TextButton(
                                                                  onPressed: () {
                                                                    Navigator.pop(
                                                                        context);
                                                                  },
                                                                  child: Text(
                                                                    'لا',
                                                                    style: TextStyle(
                                                                        color: Colors
                                                                            .white,
                                                                        fontSize:
                                                                        18,
                                                                        fontWeight:
                                                                        FontWeight
                                                                            .bold),
                                                                  ),
                                                                  style:
                                                                  ElevatedButton
                                                                      .styleFrom(
                                                                    minimumSize: Size(
                                                                        double
                                                                            .infinity,
                                                                        SizeConfig()
                                                                            .scaleHeight(
                                                                            60)),
                                                                    primary: Color(
                                                                        0xff1DB854),
                                                                    shape:
                                                                    RoundedRectangleBorder(
                                                                      borderRadius:
                                                                      BorderRadius
                                                                          .circular(
                                                                          0),
                                                                    ),
                                                                  ),
                                                                ),
                                                              ),
                                                            ]),
                                                          ),
                                                        ),
                                                      ),
                                                    ],
                                                  ),
                                                ]),
                                              ),
                                            ],
// actionsPadding: EdgeInsets.symmetric(horizontal: 50),
                                            shape: RoundedRectangleBorder(
                                              borderRadius:
                                              BorderRadius.circular(15),
                                            ),
                                          );
                                        });
                                  },
                                  onPressedUpdate: () {
                                    Navigator.push(
                                        context,
                                        MaterialPageRoute(
                                          builder: (context) => AddCarScreen(
                                              getCar(documents[index])),
                                        ));
                                  },
                                ),
                              );
                            },
                          ),
                        );
                      } else if (snapshot.connectionState ==
                          ConnectionState.waiting) {
                        return Loading();
                      } else {
                        return NoData();
                      }
                    }),
              ),
            ],
          )),
    );
  }

  Car getCar(DocumentSnapshot snapshot) {
    Car car = Car();
    car.name = snapshot['name'];
    car.state = snapshot['state'];
    car.model = snapshot['model'];
    car.description = snapshot['description'];
    car.price = snapshot['price'];
    car.images = List.from(snapshot['images']);
    car.path = snapshot.id;
    car.color = List.from(snapshot['color']);
    return car;
  }

  Future<void> getMerchantId() async {
    String ids = await FirebaseFirestoreController()
        .getMerchantId(FirebaseAuth.instance.currentUser!.email!);
    setState(() {
      id = ids;
    });
    getMerchantImage();
  }

  Future<void> getMerchantId2() async {
    String ids = await FirebaseFirestoreController().getMId();
    setState(() {
      id = ids;
    });
    getMerchantImage();
  }

  Future<void> deleteCar(DocumentSnapshot snapshot) async {
    bool ids = await FirebaseFirestoreController().deleteCar(path: snapshot.id);
    if(ids){
      List<String> list = await FirebaseFirestoreController().getCartId2(snapshot.id);
      List<String> list2 = await FirebaseFirestoreController().getFavoriteId2(snapshot.id);

      for(int i = 0; i < list.length; i++){
        await FirebaseFirestoreController().deleteFavorite(path: list[i]);
      }

      for(int i = 0; i < list2.length; i++){
        await FirebaseFirestoreController().deleteFavorite(path: list2[i]);
      }
    }
  }

  Future<void> getMerchantImage() async {
    String ids = await FirebaseFirestoreController().getMerchantImage(id);
    setState(() {
      image = ids;
    });
  }
}
