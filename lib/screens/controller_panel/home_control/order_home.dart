import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/cart.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';

import 'order_details.dart';

class OrderHome extends StatefulWidget {
  const OrderHome({Key? key}) : super(key: key);

  @override
  _OrderHomeState createState() => _OrderHomeState();
}

class _OrderHomeState extends State<OrderHome> {
  String id = '';
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMerchantId();
  }
  
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      backgroundColor: Color(0xffF1F2F3),
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),

        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'الحجوزات',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15, vertical: 15),
        child: Column(
          children: [
            Expanded(
              child: StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestoreController().readOrder(id),
                builder: (context, snapshot) {
                  if(snapshot.hasData && snapshot.data!.docs.length > 0){
                    List<DocumentSnapshot> documents = snapshot.data!.docs;
                    return ListView.separated(
                      itemCount: documents.length,
                      clipBehavior: Clip.antiAliasWithSaveLayer,
                      physics: BouncingScrollPhysics(),
                      itemBuilder: (context, index) {
                        return GestureDetector(
                          onTap: (){Navigator.push(context, MaterialPageRoute(builder: (context) => OrderDetails(getCart(documents[index])),));},
                          child: Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(15),
                              ),
                              child: Padding(
                                padding: const EdgeInsets.only(top: 3.0, bottom: 3),
                                child: Container(
                                  height: SizeConfig().scaleHeight(90),
                                  width: SizeConfig().scaleWidth(366),
                                  decoration: BoxDecoration(
                                    color: Colors.white,
                                    borderRadius: BorderRadius.circular(14),
                                  ),
                                  child: Row(
                                    children: [
                                      SizedBox(
                                        width: 0,
                                      ),
                                      Expanded(
                                        child: Column(
                                          crossAxisAlignment:
                                          CrossAxisAlignment.baseline,
                                          mainAxisAlignment: MainAxisAlignment.center,
                                          mainAxisSize: MainAxisSize.min,
                                          textBaseline: TextBaseline.ideographic,
                                          children: [
                                            SizedBox(
                                              height: 3,
                                            ),
                                            Text(
                                              documents[index].get('name'),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              softWrap: false,
                                              style: TextStyle(
                                                  color: Color(0xff0B204C),
                                                  fontWeight: FontWeight.w400,
                                                  fontSize: 16,
                                                  textBaseline:
                                                  TextBaseline.ideographic),
                                            ),

                                            Text(
                                              documents[index].get('price').toString(),
                                              maxLines: 1,
                                              overflow: TextOverflow.ellipsis,
                                              style: TextStyle(
                                                  color: Color(0xff1DB854),
                                                  fontSize: 15,
                                                  fontWeight: FontWeight.bold,
                                                  textBaseline:
                                                  TextBaseline.ideographic),
                                            ),
                                            Row(
                                              children: [
                                                SizedBox(width: 20,),
                                                Text(documents[index].get('created').toString().substring(0, 16),),
                                                Spacer(),
                                              ],
                                            )
                                          ],
                                        ),
                                      ),
                                      Padding(
                                        padding: const EdgeInsets.only(
                                            top: 0, bottom:10, left: 10,right: 10),
                                        child: CircleAvatar(
                                          radius: 25,
                                          backgroundColor: Color(0xff5A55CA),
                                          backgroundImage: NetworkImage(
                                            documents[index].get('image'),),
                                        ),
                                      ),
                                    ],
                                  ),
                                ),
                              )),
                        );
                      },
                      separatorBuilder: (BuildContext context, int index) {
                        return Divider(color: Colors.transparent,height: 10,);
                      },
                    );
                  }else if(snapshot.connectionState == ConnectionState.waiting){
                    return Loading();
                  }else{
                    return NoData();
                  }
                  
                }
              ),
            ),
          ],
        ),
      ),
    );
  }
  
  Future<void> getMerchantId() async{
    String ids = await FirebaseFirestoreController().getMerchantId(FirebaseAuth.instance.currentUser!.email!);
    setState(() {
      id = ids;
    });
  }

  Cart getCart(DocumentSnapshot snapshot){
    Cart cart = Cart();
    cart.state = snapshot.get('state');
    cart.price = snapshot.get('price');
    cart.model = snapshot.get('model');
    cart.carName = snapshot.get('carName');
    cart.image = snapshot.get('image');
    cart.path = snapshot.id;
    cart.phoneNumber = snapshot.get('phoneNumber');
    cart.name = snapshot.get('name');
    return cart;
  }

}
