import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/cart.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class OrderDetails extends StatefulWidget {
  Cart cart;

  OrderDetails(this.cart);

  @override
  _OrderDetailsState createState() => _OrderDetailsState();
}

class _OrderDetailsState extends State<OrderDetails> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(
              Icons.done,
              color: Color(0xff1DB854),
            ),
            onPressed: () async {
              Navigator.pop(context);

              await FirebaseFirestoreController()
                  .deleteCart(path: widget.cart.path);
            },
          ),
        ],
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'تفاصيل الحجز',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: Column(
        children: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20, vertical: 0),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.end,
              children: [
                Column(
                  textBaseline: TextBaseline.ideographic,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    SizedBox(
                      height: 10,
                    ),
                    Text(
                      widget.cart.name,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          fontSize: 18,
                          fontWeight: FontWeight.bold,
                          textBaseline: TextBaseline.ideographic),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Text(
                      widget.cart.phoneNumber,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 1,
                      style: TextStyle(
                          fontSize: 16,
                          fontWeight: FontWeight.bold,
                          color: Colors.grey.shade600,
                          textBaseline: TextBaseline.ideographic),
                    ),
                  ],
                ),
                SizedBox(
                  width: 10,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: 16, bottom: 10, left: 0),
                  child: CircleAvatar(
                    radius: 35,
                    backgroundColor: Color(0xff5A55CA),
                    backgroundImage: NetworkImage(widget.cart.image),
                  ),
                ),
              ],
            ),
          ),
          Divider(
            height: 0,
            color: Colors.grey,
            thickness: 0,
          ),
          SizedBox(
            height: 10,
          ),
          SizedBox(
            height: 5,
          ),
          Text(
            'معلومات السيارة',
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 22, color: Colors.black),
          ),
          SizedBox(
            height: 20,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              children: [
                Column(
                  textBaseline: TextBaseline.ideographic,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'اسم : ',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  textBaseline: TextBaseline.ideographic),
                              textAlign: TextAlign.start,
                              textDirection: TextDirection.rtl,
                            ),
                            Text(
                              widget.cart.carName,
                              style: TextStyle(
                                  fontSize: 18,
                                  textBaseline: TextBaseline.ideographic),
                              textAlign: TextAlign.start,
                              textDirection: TextDirection.rtl,
                            ),
                          ],
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: Colors.grey.shade400,
                          borderRadius: BorderRadius.circular(15)),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              children: [
                Column(
                  textBaseline: TextBaseline.ideographic,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'الموديل : ',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  textBaseline: TextBaseline.ideographic),
                              textAlign: TextAlign.start,
                              textDirection: TextDirection.rtl,
                            ),
                            Text(
                              widget.cart.model,
                              style: TextStyle(
                                  fontSize: 18,
                                  textBaseline: TextBaseline.ideographic),
                              textAlign: TextAlign.start,
                              textDirection: TextDirection.rtl,
                            ),
                          ],
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: Colors.grey.shade400,
                          borderRadius: BorderRadius.circular(15)),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              children: [
                Column(
                  textBaseline: TextBaseline.ideographic,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'السعر : ',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  textBaseline: TextBaseline.ideographic),
                              textAlign: TextAlign.start,
                              textDirection: TextDirection.rtl,
                            ),
                            Text(
                              widget.cart.price.toString() + '\$',
                              style: TextStyle(
                                  fontSize: 18,
                                  textBaseline: TextBaseline.ideographic),
                              textAlign: TextAlign.start,
                              textDirection: TextDirection.rtl,
                            ),
                          ],
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: Colors.grey.shade400,
                          borderRadius: BorderRadius.circular(15)),
                    ),
                  ],
                ),
              ],
            ),
          ),
          SizedBox(
            height: 10,
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 20.0),
            child: Column(
              children: [
                Column(
                  textBaseline: TextBaseline.ideographic,
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  children: [
                    Container(
                      child: Padding(
                        padding: const EdgeInsets.all(8.0),
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          crossAxisAlignment: CrossAxisAlignment.baseline,
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Text(
                              'الحالة : ',
                              style: TextStyle(
                                  fontSize: 18,
                                  fontWeight: FontWeight.w700,
                                  textBaseline: TextBaseline.ideographic),
                              textAlign: TextAlign.start,
                              textDirection: TextDirection.rtl,
                            ),
                            Text(
                              widget.cart.state,
                              style: TextStyle(
                                  fontSize: 18,
                                  textBaseline: TextBaseline.ideographic),
                              textAlign: TextAlign.start,
                              textDirection: TextDirection.rtl,
                            ),
                          ],
                        ),
                      ),
                      decoration: BoxDecoration(
                          color: Colors.grey.shade400,
                          borderRadius: BorderRadius.circular(15)),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
