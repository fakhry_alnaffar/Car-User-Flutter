import 'dart:io';

import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/firebase/firebase_storage.dart';
import 'package:car_user_project/models/car.dart';
import 'package:car_user_project/models/color.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/utils/helpers.dart';
import 'package:car_user_project/widgets/app_text_filed.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:image_picker/image_picker.dart';

class AddCarScreen extends StatefulWidget {
  final Car? car;

  AddCarScreen(this.car);

  @override
  _AddCarScreenState createState() => _AddCarScreenState();
}

class _AddCarScreenState extends State<AddCarScreen> with Helpers {
  String colorss = '';
  String id = '';
  List<Colore> colore = <Colore>[
    Colore(name: 'اسود', states: false),
    Colore(name: 'سكني', states: false),
    Colore(name: 'بني', states: false),
    Colore(name: 'فضي', states: false),
    Colore(name: 'احمر', states: false),
    Colore(name: 'ازرق', states: false),
    Colore(name: 'برتقالي', states: false),
    Colore(name: 'اخضر', states: false),
    Colore(name: 'اصفر', states: false),
  ];
  List<Colore> colorSelected = <Colore>[];
  XFile? pickedImage;
  XFile? pickedImage2;
  XFile? pickedImage3;
  XFile? pickedImage4;
  ImagePicker imagePicker = ImagePicker();
  String selectedImage = '';
  List<String> images = <String>[];
  int _currentPage = 0;
  bool selected = false;
  late TextEditingController _name;
  late TextEditingController _model;
  late TextEditingController _color;
  late TextEditingController _description;
  late TextEditingController _price;
  late TextEditingController _type;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMerchantId();
    if (widget.car != null) {
      for (int i = 0; i < widget.car!.color.length; i++) {
        colorSelected.add(Colore(name: widget.car!.color[i], states: true));
      }
    }
    getSelectedColor();
    widget.car != null ? images = widget.car!.images : images = <String>[];
    _name =
        TextEditingController(text: widget.car != null ? widget.car!.name : '');
    _model = TextEditingController(
        text: widget.car != null ? widget.car!.model : '');
    // _color = TextEditingController(text: widget.car != null ? widget.car!.color.toString() : '');
    _description = TextEditingController(
        text: widget.car != null ? widget.car!.description : '');
    _price = TextEditingController(
        text: widget.car != null ? widget.car!.price.toString() : '');
    _type = TextEditingController(
        text: widget.car != null ? widget.car!.state : '');
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _name.dispose();
    _model.dispose();
    _color.dispose();
    _description.dispose();
    _price.dispose();
    _type.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      backgroundColor: Color(0xffF1F2F3),
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        actions: [
          IconButton(
              onPressed: () {
                if (widget.car != null) {
                  updateCar();
                } else {
                  addCar();
                }
              },
              icon: Icon(
                Icons.check_circle,
                color: Color(0xff1DB854),
                size: 30,
              ))
        ],
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            widget.car != null ? 'تعديل معلومات السيارة' : 'اضافة سيارة',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          clipBehavior: Clip.antiAlias,
          physics: BouncingScrollPhysics(),
          padding: EdgeInsets.zero,
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              SingleChildScrollView(
                scrollDirection: Axis.horizontal,
                child: Row(
                  children: [
                    SizedBox(
                      width: 8,
                    ),
                    Stack(
                      alignment: AlignmentDirectional.bottomEnd,
                      children: [
                        GestureDetector(
                          onTap: () {
                            pickImage();
                          },
                          child: Container(
                            clipBehavior: Clip.antiAlias,
                            width: 120,
                            height: 80,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: widget.car != null
                                  ? Colors.white
                                  : Color(0xff1DB854),
                            ),
                            child: widget.car != null
                                ? pickedImage2 != null
                                ? Image.file(
                              File(pickedImage2!.path),
                              fit: BoxFit.cover,
                            )
                                : Image.network(
                              widget.car!.images[0],
                              fit: BoxFit.cover,
                            )
                                : pickedImage2 != null
                                ? Image.file(
                              File(pickedImage2!.path),
                              fit: BoxFit.cover,
                            )
                                : Icon(
                              Icons.add_photo_alternate_outlined,
                              color: Colors.white,
                              size: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Stack(
                      alignment: AlignmentDirectional.bottomEnd,
                      children: [
                        GestureDetector(
                          onTap: () {
                            pickImage2();
                          },
                          child: Container(
                            clipBehavior: Clip.antiAlias,
                            width: 120,
                            height: 80,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: widget.car != null
                                  ? Colors.white
                                  : Color(0xff1DB854),
                            ),
                            child: widget.car != null
                                ? pickedImage3 != null
                                ? Image.file(
                              File(pickedImage3!.path),
                              fit: BoxFit.cover,
                            )
                                : Image.network(
                              widget.car!.images[1],
                              fit: BoxFit.cover,
                            )
                                : pickedImage3 != null
                                ? Image.file(
                              File(pickedImage3!.path),
                              fit: BoxFit.cover,
                            )
                                : Icon(
                              Icons.add_photo_alternate_outlined,
                              color: Colors.white,
                              size: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 8,
                    ),
                    Stack(
                      alignment: AlignmentDirectional.bottomEnd,
                      children: [
                        GestureDetector(
                          onTap: () {
                            pickImage3();
                          },
                          child: Container(
                            clipBehavior: Clip.antiAlias,
                            width: 120,
                            height: 80,
                            decoration: BoxDecoration(
                              borderRadius: BorderRadius.circular(8),
                              color: widget.car != null
                                  ? Colors.white
                                  : Color(0xff1DB854),
                            ),
                            child: widget.car != null
                                ? pickedImage4 != null
                                ? Image.file(
                              File(pickedImage4!.path),
                              fit: BoxFit.cover,
                            )
                                : Image.network(
                              widget.car!.images[2],
                              fit: BoxFit.cover,
                            )
                                : pickedImage4 != null
                                ? Image.file(
                              File(pickedImage4!.path),
                              fit: BoxFit.cover,
                            )
                                : Icon(
                              Icons.add_photo_alternate_outlined,
                              color: Colors.white,
                              size: 50,
                            ),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      width: 8,
                    ),
                  ],
                ),
              ),
              SizedBox(
                height: 20,
              ),
              Padding(
                padding: const EdgeInsets.symmetric(horizontal: 15),
                child: Column(
                  children: [
                    AppTextFiled(
                      labelText: 'اسم السيارة',
                      controller: _name,
                      prefix: Icons.directions_car_outlined,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    AppTextFiled(
                      labelText: 'الموديل',
                      controller: _model,
                      prefix: Icons.model_training,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    AppTextFiled(
                      labelText: 'اللون',
                      controller: TextEditingController(text: colorss),
                      showCursor: false,
                      readOnly: true,
                      onTap: () {
                        showColorBottomSheet();
                      },
                      prefix: Icons.color_lens_outlined,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    AppTextFiled(
                      labelText: 'السعر',
                      textInputType: TextInputType.number,
                      controller: _price,
                      prefix: Icons.price_change_rounded,
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    AppTextFiled(
                      labelText: 'الحالة',
                      controller: _type,
                      showCursor: false,
                      readOnly: true,
                      onTap: () {
                        showStateBottomSheet();
                      },
                      prefix: Icons.autorenew,
                    ),
                    SizedBox(
                      height: 10,
                    ),
                    AppTextFiled(
                      labelText: 'وصف',
                      controller: _description,
                      prefix: Icons.task_rounded,
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool checkData() {
    if (_name.text.isNotEmpty &&
        _description.text.isNotEmpty &&
        _price.text.isNotEmpty &&
        _model.text.isNotEmpty &&
        _type.text.isNotEmpty &&
        colorSelected.length != 0 &&
        images.length >= 3) {
      return true;
    } else {
      showSnackBar(
          context: context, message: 'يجب ادخال جميع البيانات', error: true);
      return false;
    }
  }

  Future<void> addCarProses() async {
    bool state = await FirebaseFirestoreController().addCar(getCar());
    if (state) {
      Navigator.pop(context);
    }
  }

  Future<void> addCar() async {
    if (checkData()) {
      showDialogWidget(
          context2: context,
          message: 'تم إضافة السيارة بنجاح',
          sub: '',
          type: 'رجوع',
          function: () {
            Navigator.pop(context);
          });
      await addCarProses();
    }
  }

  Future<void> updateCarProses() async {
    bool state = await FirebaseFirestoreController()
        .updateCar(path: widget.car!.path, car: getCar());
    if (state) {
      Navigator.pop(context);
    }
  }

  Future<void> updateCar() async {
    if (checkData()) {
      await updateCarProses();
    }
  }

  Future<void> pickImage() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage1(),
            message: Text('جاري تحميل الصورة...')),
      );
      setState(() {
        pickedImage2 = pickedImage;
      });
    }
  }

  Future<void> pickImage2() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage2(),
            message: Text('جاري تحميل الصورة...')),
      );
      setState(() {
        pickedImage3 = pickedImage;
      });
    }
  }

  Future<void> pickImage3() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage3(),
            message: Text('جاري تحميل الصورة...')),
      );
      setState(() {
        pickedImage4 = pickedImage;
      });
    }
  }

  Future<void> uploadImage1() async {
    selectedImage = await FirebaseStorageController()
        .uploadImage(File(pickedImage!.path), 'car');
    if (widget.car != null) {
      images.removeAt(0);
      images.add(selectedImage);
    } else {
      images.add(selectedImage);
    }
  }

  Future<void> uploadImage2() async {
    selectedImage = await FirebaseStorageController()
        .uploadImage(File(pickedImage!.path), 'car');
    if (widget.car != null) {
      images.removeAt(1);
      images.add(selectedImage);
    } else {
      images.add(selectedImage);
    }
  }

  Future<void> uploadImage3() async {
    selectedImage = await FirebaseStorageController()
        .uploadImage(File(pickedImage!.path), 'car');
    if (widget.car != null) {
      images.removeAt(2);
      images.add(selectedImage);
    } else {
      images.add(selectedImage);
    }
  }

  Car getCar() {
    Car car = Car();
    car.state = _type.text;
    car.name = _name.text;
    car.price = double.parse(_price.text.toString());
    car.description = _description.text;
    for (int i = 0; i < colorSelected.length; i++) {
      car.color.add(colorSelected[i].name);
    }
    for (int i = 0; i < images.length; i++) {
      car.images.add(images[i]);
    }
    car.model = _model.text;
    car.merchant = id;
    return car;
  }

  void showStateBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RadioListTile(
                  title: Text('جديدة'),
                  value: 'جديدة',
                  groupValue: _type.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _type.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('مستعملة'),
                  value: 'مستعملة',
                  groupValue: _type.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _type.text = value;
                      });

                    Navigator.pop(context);
                  }),
            ],
          ),
        );
      },
    );
  }

  void showColorBottomSheet() {
    showModalBottomSheet(
        barrierColor: Colors.black.withOpacity(0.25),
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(15),
        ),
        context: context,
        builder: (context) {
          return Padding(
              padding: EdgeInsets.symmetric(vertical: 15),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Expanded(
                    child: StatefulBuilder(
                      builder: (BuildContext context,
                          void Function(void Function()) setState) {
                        return ListView.builder(
                          itemCount: colore.length,
                          itemBuilder: (context, index) {
                            return CheckboxListTile(
                              title: Text(colore[index].name),
                              value: colore[index].states,
                              onChanged: (status) {
                                setState(() {
                                  colore[index].states = status!;
                                });
                                print(status);
                              },
                            );
                          },
                        );
                      },
                    ),
                  ),
                  Padding(
                    padding: EdgeInsets.all(16),
                    child: ElevatedButton(
                      style: ElevatedButton.styleFrom(
                        minimumSize:
                        Size(double.infinity, SizeConfig().scaleHeight(60)),
                        primary: Color(0xff1DB854),
                        shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(10),
                        ),
                      ),
                      onPressed: () {
                        setState(() {
                          widget.car != null ? colorSelected : colorSelected.clear();
                          for (Colore colore in colore) {
                            if (colore.states) {
                              colorSelected.add(colore);
                            }
                          }
                          getSelectedColor();
                        });
                        Navigator.pop(context);
                      },
                      child: Text(
                        'حسنا',
                        style:
                        TextStyle(fontSize: SizeConfig().scaleTextFont(20)),
                      ),
                    ),
                  ),
                ],
              ));
        });
  }

  void getSelectedColor() {
    String name = '';
    for (int i = 0; i < colorSelected.length; i++) {
      name += colorSelected[i].name + ' ';
    }
    setState(() {
      colorss = name;
    });
  }

  Future<void> getMerchantId() async {
    String ids = await FirebaseFirestoreController()
        .getMerchantId(FirebaseAuth.instance.currentUser!.email!);
    setState(() {
      id = ids;
    });
  }
}
