import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/utils/helpers.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

class AddManger extends StatefulWidget {
  const AddManger({Key? key}) : super(key: key);

  @override
  _AddMangerState createState() => _AddMangerState();
}

class _AddMangerState extends State<AddManger> with Helpers {
  String id = '';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMarchentId();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'اضافة مشرف',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      //HERE WORK FIREBASE
      body: Column(
        children: [
          SizedBox(
            height: 5,
          ),
          SafeArea(
            child: Container(
              margin: EdgeInsetsDirectional.zero,
              padding: EdgeInsetsDirectional.zero,
              // alignment: AlignmentDirectional.topStart,
              child: Padding(
                padding:
                const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                child: Card(
                  // margin: EdgeInsetsDirectional.zero,
                  shape: RoundedRectangleBorder(
                    borderRadius: BorderRadius.circular(50),
                  ),
                  child: ListTile(
                    shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                    ),
                    leading: Icon(Icons.search),
                    title: TextField(
                      // controller: controller,
                      decoration: InputDecoration(
                          hintText: 'البحث', border: InputBorder.none),
                      // onChanged: onSearchTextChanged,
                    ),
                    trailing: IconButton(
                      padding: EdgeInsetsDirectional.zero,
                      icon: new Icon(Icons.cancel),
                      onPressed: () {
                        // controller.clear();
                        // onSearchTextChanged('');
                      },
                    ),
                  ),
                ),
              ),
            ),
          ),
          Expanded(
            child: StreamBuilder<QuerySnapshot>(
                stream: FirebaseFirestoreController().readUserToManger(),
                builder: (context, snapshot) {
                  if (snapshot.hasData && snapshot.data!.docs.length > 0) {
                    List<DocumentSnapshot> documents = snapshot.data!.docs;
                    return Container(
                      margin: EdgeInsetsDirectional.zero,
                      padding: EdgeInsetsDirectional.zero,
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: 24.0),
                        child: ListView.builder(
                            itemCount: documents.length,
                            physics: BouncingScrollPhysics(),
                            padding: EdgeInsetsDirectional.zero,
                            itemBuilder: (context, index) {
                              return InkWell(
                                child: showItem(
                                  // documents: documents,
                                  widget1: IconButton(
                                      padding: EdgeInsetsDirectional.zero,
                                      onPressed: () {
                                        showDialog(
                                            barrierColor:
                                            Colors.black.withOpacity(0.16),
                                            context: context,
                                            builder: (context) {
                                              return AlertDialog(
                                                title: Text('هل انت متاكد ؟'),
                                                actions: [
                                                  TextButton(
                                                      onPressed: () {
                                                        Navigator.pop(context);
                                                        setAsManger(documents[index]);
                                                      },
                                                      child: Text('نعم')),
                                                  TextButton(
                                                      onPressed: () =>
                                                          Navigator.pop(context),
                                                      child: Text('لا')),
                                                ],
// actionsPadding: EdgeInsets.symmetric(horizontal: 50),
                                                shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                  BorderRadius.circular(15),
                                                ),
                                              );
                                            });
                                        print(index);

                                      },
                                      icon: Icon(
                                        Icons.check_circle_rounded,
                                        color: Color(0xff1DB854),
                                        size: 25,
                                      )),

                                  widget2: IconButton(
                                      padding: EdgeInsetsDirectional.zero,
                                      onPressed: () {},
                                      icon: Icon(
                                        Icons.cancel_outlined,
                                        color: Colors.transparent,
                                        size: 25,
                                      )),
                                  index: index,
                                  context: context,
                                  title: documents[index].get('name'),
                                  subtitle: documents[index].get('phoneNumber').toString(),
                                  url:
                                  documents[index].get('image'),
                                  documents: documents,
                                ),
                                onTap: () {
                                  print(index);
                                },
                              );
                            }),
                      ),
                    );
                  } else if (snapshot.connectionState ==
                      ConnectionState.waiting) {
                    return Loading();
                  } else {
                    return NoData();
                  }
                }),
          ),
        ],
      ),
    );
  }

  Future<void> setAsManger(DocumentSnapshot snapshot) async {
    bool state = await FirebaseFirestoreController().addManger(snapshot.id, id);
    if (state) {
      showSnackBar(
          context: context,
          message: 'تم تعيين هذا المستخدم كمسؤول بنجاح',
          error: false);
    }
  }

  Future<void> getMarchentId() async {
    String state = await FirebaseFirestoreController().getMarchentId();
    setState(() {
      id = state;
    });
  }
}
