import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/widgets/order_item_list.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';
class SubscribeScreen extends StatefulWidget {
  DocumentSnapshot snapshot;

  SubscribeScreen(this.snapshot);

  @override
  _SubscribeScreenState createState() => _SubscribeScreenState();
}

class _SubscribeScreenState extends State<SubscribeScreen> {
  String price = '0';
  String price2 = '0';
  String price3 = '0';
  int mCount = 0;
  int adsCount = 0;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMerchantPrice();
    getMerchantPrice2();
    getMerchantCount();
    getMerchantPrice3();
    getAdsCount();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        iconTheme: IconThemeData(color: Color(0xff1DB854)),
        title: Text(
          'اشتراكي',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color(0xffF1F2F3),
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: Color(0xffF1F2F3),
      body: Stack(
        children: [
          Align(
            child: Container(
              clipBehavior: Clip.antiAlias,
              margin: EdgeInsets.only(top: 0),
              width: double.infinity,
              height: double.infinity,
              alignment: Alignment.bottomCenter,
              decoration: BoxDecoration(
                color: Color(0xffF1F2F3),
                borderRadius: BorderRadius.only(
                  topLeft: Radius.circular(40),
                  topRight: Radius.circular(40),
                ),
              ),
              child: Align(
                alignment: Alignment.topCenter,
                child: Padding(
                  padding:
                  const EdgeInsets.symmetric(horizontal: 10, vertical: 0),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    mainAxisSize: MainAxisSize.max,
                    children: [
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'اسم المعرض :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //name
                              widget.snapshot.get('marketName'),
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'تاريخ بداية الاشتراك :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //date
                              widget.snapshot.get('created'),
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 5,
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'تاريخ نهاية الاشتراك :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //date
                              widget.snapshot.get('date'),
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 30,
                        child: Row(
                          textBaseline: TextBaseline.ideographic,
                          mainAxisSize: MainAxisSize.max,
                          children: [
                            SizedBox(
                              width: 10,
                            ),
                            Text(
                              'اجمالي المبلغ :',
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                            SizedBox(
                              width: 5,
                            ),
                            Text(
                              //name
                              (int.parse(price2) + int.parse(price) + int.parse(price3)).toString(),
                              style: TextStyle(
                                  color: Color(0xff0B204C),
                                  fontSize: 18,
                                  fontWeight: FontWeight.bold),
                            ),
                          ],
                        ),
                      ),

                      SizedBox(
                        height: 10,
                      ),
                      itemTITLE(),
                      SizedBox(
                        height: 5,
                      ),
                      Expanded(
                        // child: ListView.separated(
                        //   itemCount: 3,
                        //   itemBuilder: (context, index) {
                        //     return OrderItemList('name', 5, 10);
                        //   },
                        //   padding: EdgeInsetsDirectional.only(top: 5),
                        //   separatorBuilder: (BuildContext context, int index) { return SizedBox(height: 5,);
                        //   },
                        // ),
                        child: ListView(
                          children: [
                            OrderItemList('اشتراك شهري', '-', price),
                            SizedBox(height: 5,),
                            OrderItemList('اشتراك مشرفين', mCount.toString(), (mCount * int.parse(price2)).toString()),
                            SizedBox(height: 5,),
                            OrderItemList('اشتراك اعلانات', adsCount.toString(), (adsCount * int.parse(price3)).toString()),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ],
      ),
    );
  }
  Widget itemTITLE() {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(5.0),
          topRight: const Radius.circular(5.0),
          bottomRight: const Radius.circular(5.0),
          bottomLeft: const Radius.circular(5.0),
        ),
        gradient: LinearGradient(
          begin: AlignmentDirectional.topStart,
          end: AlignmentDirectional.bottomEnd,
          colors: [
            Color(0xff1DB854),
            Color(0xe21db854),
          ],
        ),
      ),
      width: double.infinity,
      margin: EdgeInsetsDirectional.only(start: 2, end: 2),
      height: 50,
      child: Row(
        children: [
          SizedBox(
            width: 20,
          ),
          Text(
            'نوع الاشتراك',
            style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
          ),
          SizedBox(
            width: 60,
          ),
          Text(
            'عدد',
            style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
          ),
          Spacer(),
          SizedBox(
            width: 20,
          ),
          Text(
            'تكلفة الاشتراك',
            style: TextStyle(color: Colors.white,fontWeight: FontWeight.bold),
          ),
          Spacer(),
          SizedBox(
            width: 20,
          ),
        ],
      ),
    );
  }

  Future<void> getMerchantPrice() async{
    String state = await FirebaseFirestoreController().getMerchantPrice();
    setState(() {
      price = state;
    });
  }

  Future<void> getMerchantPrice2() async{
    String state = await FirebaseFirestoreController().getMerchantPrice3();
    setState(() {
      price2 = state;
    });
  }

  Future<void> getMerchantPrice3() async{
    String state = await FirebaseFirestoreController().getMerchantPrice2();
    setState(() {
      price3 = state;
    });
  }

  Future<void> getMerchantCount() async{
    int state = await FirebaseFirestoreController().getMerchantCount();
    setState(() {
      mCount = state;
    });
  }

  Future<void> getAdsCount() async{
    int state = await FirebaseFirestoreController().getAdsCount();
    setState(() {
      adsCount = state;
    });
  }

}
