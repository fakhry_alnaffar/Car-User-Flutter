import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/screens/controller_panel/subscribe/subscribe_home.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'home_control/home_controller.dart';

class ControllerSelect extends StatefulWidget {
  final String url;

  ControllerSelect(this.url);

  @override
  _ControllerSelectState createState() => _ControllerSelectState();
}

class _ControllerSelectState extends State<ControllerSelect> {
  String id = '';
  String date = '0000-00-00';
  DocumentSnapshot? documentSnapshot;
  DocumentSnapshot? documentSnapshot2;
  bool isMana = true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getMerchantDate();
    getMerchantSub();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    if(DateTime.now().isAfter(DateTime.parse(date))){
      return Scaffold(
        body: SafeArea(
          child: Center(
            child: Container(
              child: Column(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Icon(
                    Icons.info,
                    color: Color(0xff1DB854),
                    size: 80,
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(horizontal: 48.0),
                    child: Text(
                      'نأسف لقد انتهى اشتراكك لهذا الشهر يرجى تجديد الاشتراك',
                      textAlign: TextAlign.center,
                      style: TextStyle(
                          color: Color(0xff1DB854),
                          fontSize: 24,
                          fontWeight: FontWeight.bold
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      );
    }else{
      return Scaffold(
        backgroundColor: Colors.white,
        extendBodyBehindAppBar: true,
        appBar: AppBar(
          title: Container(
            margin: EdgeInsetsDirectional.only(top: 10),
            child: Text(
              'لوحة التحكم',
              style: TextStyle(
                  color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
            ),
          ),
          centerTitle: true,
          backgroundColor: Colors.transparent,
          elevation: 0,
          leading: IconButton(
            icon: Icon(
              Icons.arrow_back_ios_outlined,
              color: Color(0xff1DB854),
            ),
            onPressed: () {
              Navigator.pop(context);
            },
          ),
        ),
        body: SafeArea(
          child: Align(
            alignment: AlignmentDirectional.topCenter,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.max,
              children: [
                SizedBox(
                  height: SizeConfig().scaleHeight(50),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 15,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(
                            context,
                            MaterialPageRoute(
                              builder: (context) => HomeController(widget.url),
                            ));
                        // Navigator.pushNamed(context, 'home_controller_screen');
                      },
                      child: selectWidget(
                          widget: Icon(
                            Icons.directions_car_outlined,
                            size: 60,
                            color: Colors.white,
                          ),
                          tittle: 'المعرض'),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, 'ads_home_screen');
                      },
                      child: selectWidget(
                          widget: Icon(
                            Icons.speaker_group_outlined,
                            size: 50,
                            color: Colors.white,
                          ),
                          tittle: 'الاعلانات'),
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig().scaleHeight(40),
                ),
                Row(
                  children: [
                    SizedBox(
                      width: 15,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.push(context, MaterialPageRoute(builder: (context) => SubscribeScreen(documentSnapshot2!),));
                      },
                      child: selectWidget(
                          widget: Icon(
                            Icons.monetization_on_outlined,
                            size: 60,
                            color: Colors.white,
                          ),
                          tittle: 'الاشتراك'),
                    ),
                    SizedBox(
                      width: 20,
                    ),
                    Visibility(
                      visible: isMana,
                      child: InkWell(
                        onTap: () {
                          Navigator.pushNamed(context, 'manger_screen');
                        },
                        child: selectWidget(
                            widget: Icon(
                              Icons.person_outline_rounded,
                              size: 50,
                              color: Colors.white,
                            ),
                            tittle: 'الادارة'),
                      ),
                    ),
                  ],
                ),
                SizedBox(
                  height: SizeConfig().scaleHeight(40),
                ),
                Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    SizedBox(
                      width: 15,
                    ),
                    InkWell(
                      onTap: () {
                        Navigator.pushNamed(context, 'order_home');
                      },
                      child: selectWidget(
                          widget: Icon(
                            Icons.monetization_on_outlined,
                            size: 60,
                            color: Colors.white,
                          ),
                          tittle: 'الطلبات'),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
      );
    }

  }

  Future<void> isManager() async{
    bool state = await FirebaseFirestoreController().isManager();
    setState(() {
      isMana = state;
    });
  }

  Future<void> getMerchantDate() async{
    String state = await FirebaseFirestoreController().getMerchantDate();
    setState(() {
      date = state;
    });
  }

  Future<void> getMerchantSub() async{
    DocumentSnapshot state = await FirebaseFirestoreController().getMerchantSub();
    setState(() {
      documentSnapshot2 = state;
    });
  }

}
