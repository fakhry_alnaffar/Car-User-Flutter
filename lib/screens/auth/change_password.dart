import 'package:car_user_project/firebase/firebase_auth_controller.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/utils/helpers.dart';
import 'package:car_user_project/widgets/app_text_filed.dart';
import 'package:flutter/material.dart';

class ChangePasswordScreen extends StatefulWidget {
  const ChangePasswordScreen({Key? key}) : super(key: key);

  @override
  _ChangePasswordScreenState createState() => _ChangePasswordScreenState();
}

class _ChangePasswordScreenState extends State<ChangePasswordScreen> with Helpers {
  late TextEditingController _newPasswordController;
  late bool isPassword=true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _newPasswordController=TextEditingController();
  }
  @override
  void dispose() {
    // TODO: implement dispose
    _newPasswordController.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      backgroundColor: Color(0xffF1F2F3),
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'تغير كلمة المرور',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
      ),
      body: SafeArea(
        child: Padding(
          padding: EdgeInsets.all(SizeConfig().scaleWidth(24)),
          child: Align(
            alignment: Alignment.topCenter,
            child: SingleChildScrollView(
              padding: EdgeInsetsDirectional.only(top: 10),
              clipBehavior: Clip.antiAlias,
              physics: BouncingScrollPhysics(),
              child: Column(
                children: [
                  SizedBox(
                    height: SizeConfig().scaleHeight(40),
                  ),
                  SizedBox(
                    height: SizeConfig().scaleHeight(20),
                  ),
                  AppTextFiled(
                    labelText: 'كلمة المرور الجديدة',
                    controller: _newPasswordController,
                    obscureText: isPassword,
                    prefix: Icons.lock,
                    suffix: isPassword
                        ? Icons.visibility
                        : Icons.visibility_off,
                    functionSuffixPressed: () {
                      setState(() {
                        isPassword = !isPassword;
                      });
                    },
                  ),
                  SizedBox(
                    height: SizeConfig().scaleHeight(30),
                  ),
                  ElevatedButton(
                    style: ElevatedButton.styleFrom(
                      minimumSize: Size(
                          double.infinity, SizeConfig().scaleHeight(60)),
                      primary: Color(0xff1DB854),
                      shape: RoundedRectangleBorder(
                        borderRadius: BorderRadius.circular(10),
                      ),
                    ),
                    onPressed: () {
                      performUpdatePassword();
                    },
                    child: Text(
                      'تاكيد العملية',
                      style: TextStyle(
                          fontSize: SizeConfig().scaleTextFont(20)),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Future<void> performUpdatePassword() async {
    if (checkData()) {
      await  updatePassword();
    }
  }

  bool checkData() {
    if (_newPasswordController.text.isNotEmpty) {
      return true;
    }
    showSnackBar(context: context, message: 'الرجاء ادخال كلمة المرور الجديدة', error: true);
    return false;
  }

  Future<void> updatePassword() async {
    bool updated = await FirebaseAuthController()
        .updatePassword(context, password: _newPasswordController.text);
    if (updated) {
      setState(() {
        showSnackBar(
            context: context, message: 'تم تحديث كلمة المرور', error: false);
      });
      Navigator.pop(context);
    } else {
      showSnackBar(context: context, message: 'فشلت العملية', error: true);
    }
  }

}