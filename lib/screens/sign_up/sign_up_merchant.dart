import 'package:car_user_project/firebase/firebase_auth_controller.dart';
import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/merchant.dart';
import 'package:car_user_project/models/room.dart';
import 'package:car_user_project/preferences/student_pref.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/utils/helpers.dart';
import 'package:car_user_project/widgets/app_text_filed.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:flutter/material.dart';
import 'package:uuid/uuid.dart';
class SignUpMerchant extends StatefulWidget {
  const SignUpMerchant({Key? key}) : super(key: key);

  @override
  _SignUpMerchantState createState() => _SignUpMerchantState();
}

class _SignUpMerchantState extends State<SignUpMerchant> with Helpers{
  late TextEditingController _email;
  late TextEditingController _password;
  late TextEditingController _phone;
  late TextEditingController _name;
  late TextEditingController _rePassword;
  late TextEditingController _country;
  late TextEditingController _shopName;
  late TextEditingController _adders;
  late bool isPassword = true;
  String? place;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    _email = TextEditingController();
    _password = TextEditingController();
    _phone = TextEditingController();
    _name = TextEditingController();
    _rePassword = TextEditingController();
    _country = TextEditingController();
    _shopName = TextEditingController();
    _adders = TextEditingController();
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _email.dispose();
    _password.dispose();
    _phone.dispose();
    _name.dispose();
    _rePassword.dispose();
    _country.dispose();
    _shopName.dispose();
    _adders.dispose();
    super.dispose();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'انشاء حساب',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: SingleChildScrollView(
          clipBehavior: Clip.antiAlias,
          padding: EdgeInsetsDirectional.zero,
          physics: BouncingScrollPhysics(),
          child: Column(
            children: [
              SizedBox(
                height: 20,
              ),
              Center(
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 35),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      AppTextFiled(
                        labelText: 'الاسم ثلاثي\n',
                        controller: _name,
                        prefix: Icons.person,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AppTextFiled(
                        labelText: 'اسم المعرض\n',
                        controller: _shopName,
                        prefix: Icons.shop,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AppTextFiled(
                        labelText: 'رقم الجوال \n',
                        controller: _phone,
                        textInputType: TextInputType.number,
                        prefix: Icons.phone_android_outlined,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AppTextFiled(
                        labelText: ' البردي الاكتروني \n',
                        controller: _email,
                        textInputType: TextInputType.emailAddress,
                        prefix: Icons.phone_android_outlined,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AppTextFiled(
                        labelText: 'المنطقة \n',
                        readOnly: true,
                        showCursor: false,
                        onTap: (){
                          showBottomSheet();
                        },
                        controller: _country,
                        textInputType: TextInputType.streetAddress,
                        prefix: Icons.language_outlined,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AppTextFiled(
                        labelText: 'العنوان \n',
                        readOnly: true,
                        showCursor: false,
                        onTap: (){
                          Navigator.pushNamed(context, 'map_screen');
                        },
                        controller: _adders,
                        textInputType: TextInputType.emailAddress,
                        prefix: Icons.map_rounded,
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AppTextFiled(
                        labelText: 'كلمة المرور\n',
                        controller: _password,
                        obscureText: isPassword,
                        prefix: Icons.lock,
                        suffix: isPassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                        functionSuffixPressed: () {
                          setState(() {
                            isPassword = !isPassword;
                          });
                        },
                      ),
                      SizedBox(
                        height: 15,
                      ),
                      AppTextFiled(
                        labelText: 'تاكيد كلمة المرور\n',
                        controller: _rePassword,
                        obscureText: isPassword,
                        prefix: Icons.lock,
                        suffix: isPassword
                            ? Icons.visibility
                            : Icons.visibility_off,
                        functionSuffixPressed: () {
                          setState(() {
                            isPassword = !isPassword;
                          });
                        },
                      ),
                      SizedBox(
                        height: SizeConfig().scaleHeight(30),
                      ),
                      ElevatedButton(
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(
                              double.infinity, SizeConfig().scaleHeight(60)),
                          primary: Color(0xff1DB854),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(50),
                          ),
                        ),
                        onPressed: ()async {
                          await signUp();
                        },
                        child: Text(
                          'انشاء الحساب',
                          style: TextStyle(
                              fontSize: SizeConfig().scaleTextFont(18)),
                        ),
                      ),
                      SizedBox(
                        height: 10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: [
                          Text('لديك حساب بالفعل؟'),
                          TextButton(
                            onPressed: () {
                              Navigator.pushNamed(context, 'select_sign');
                            },
                            child: Text(
                              ' تسجيل دخول ',
                              style: TextStyle(color: Color(0xff1DB854)),
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(
                height: SizeConfig().scaleHeight(220),
              ),
            ],
          ),
        ),
      ),
    );
  }

  bool checkData(){
    if(_email.text.isNotEmpty && _password.text.isNotEmpty && _name.text.isNotEmpty && _phone.text.isNotEmpty && _country.text.isNotEmpty && _rePassword.text.isNotEmpty){
      if(_password.text == _rePassword.text){
        return true;
      }else{
        showSnackBar(context: context, message: 'كلمة المرور غير متطابقة', error: true);
        return false;
      }
    }else{
      showSnackBar(context: context, message: 'يجب ادخال جميع البيانات المطلوبة', error: true);
      return false;
    }
  }

  Future<void> signUp() async{
    if(checkData()){
      await singUpProses();
    }
  }

  Future<void> singUpProses() async{
    bool state = await FirebaseAuthController().createAccount(context, email: _email.text, password: _password.text);
    if(state){
      await FirebaseFirestoreController().addMerchant(getMerchant());
      await FirebaseFirestoreController().addAdminOrder(getMerchant());
      await FirebaseFirestoreController().addRoom(getRoom());
      showDialogWidget(context2: context,
          message: 'تم إنشاء حسابك بنجاح',
          sub: 'الرجاء تفعيل الحساب عبر ال Gmail',
          type: 'تصفح التطبيق',
          function: (){
            Navigator.pop(context);
          }
      );
      Navigator.pop(context);
      Navigator.pop(context);
    }
  }

  Merchant getMerchant(){
    Merchant merchant = Merchant();
    merchant.email = _email.text;
    merchant.name = _name.text;
    merchant.phoneNumber = _phone.text;
    merchant.accept = '0';
    merchant.place = _country.text;
    merchant.marketName = _shopName.text;
    merchant.isBlock = '0'.toString();
    merchant.address = AppPreferences().getPlace();
    merchant.roomId = Uuid().v4();
    merchant.image = 'https://firebasestorage.googleapis.com/v0/b/carproject-flutter.appspot.com/o/image.gif?alt=media&token=09ff484d-8c5f-4180-a233-048a6d612248';
    AppPreferences().setPlace('');
    return merchant;
  }

  Room getRoom(){
    Room room = Room();
    room.email = _email.text;
    room.name = _name.text;
    room.image = 'https://firebasestorage.googleapis.com/v0/b/carproject-flutter.appspot.com/o/image.gif?alt=media&token=09ff484d-8c5f-4180-a233-048a6d612248';
    return room;
  }

  void showBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RadioListTile(
                  title: Text('شمال غزة'),
                  value: 'شمال غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('غرب غزة'),
                  value: 'غرب غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('شرق غزة'),
                  value: 'شرق غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('جنوب غزة'),
                  value: 'جنوب غزة',
                  groupValue: _country.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _country.text = value;
                      });

                    Navigator.pop(context);
                  }),
            ],
          ),
        );
      },
    );
  }

}
