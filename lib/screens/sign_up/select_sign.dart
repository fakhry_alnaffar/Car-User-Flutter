import 'package:car_user_project/responsive/size_config.dart';
import 'package:flutter/material.dart';

class SelectSign extends StatefulWidget {
  const SelectSign({Key? key}) : super(key: key);

  @override
  _SelectSignState createState() => _SelectSignState();
}

class _SelectSignState extends State<SelectSign> {
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.14).designHeight(8.96).init(context);
    return Scaffold(
      backgroundColor: Colors.white,
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        title: Container(
          margin: EdgeInsetsDirectional.only(top: 10),
          child: Text(
            'نوع الحساب',
            style: TextStyle(
                color: Colors.black, fontWeight: FontWeight.bold, fontSize: 20),
          ),
        ),
        centerTitle: true,
        backgroundColor: Colors.transparent,
        elevation: 0,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
      ),
      body: SafeArea(
        child: Align(
          alignment: AlignmentDirectional.topCenter,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            children: [
              SizedBox(
                height: SizeConfig().scaleHeight(120),
              ),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, 'sign_up_user');
                  print('user');
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Color(0xff1DB854),
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(40.0),
                      topRight: const Radius.circular(40.0),
                      bottomRight: const Radius.circular(40.0),
                      bottomLeft: const Radius.circular(40.0),
                    ),
                  ),
                  height: 150,
                  width: 200,
                  child: Align(
                      alignment: AlignmentDirectional.center,
                      child: Text(
                        'مستخدم',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                      )),
                ),
              ),
              SizedBox(
                height: 60,
              ),
              InkWell(
                onTap: () {
                  Navigator.pushNamed(context, 'sign_up_merchant');
                  print('merchant');
                },
                child: Container(
                  decoration: BoxDecoration(
                    color: Color(0xff1DB854),
                    borderRadius: new BorderRadius.only(
                      topLeft: const Radius.circular(40.0),
                      topRight: const Radius.circular(40.0),
                      bottomRight: const Radius.circular(40.0),
                      bottomLeft: const Radius.circular(40.0),
                    ),
                  ),
                  height: 150,
                  width: 200,
                  child: Align(
                      alignment: AlignmentDirectional.center,
                      child: Text(
                        'صاحب معرض',
                        style: TextStyle(
                            color: Colors.white,
                            fontSize: 24,
                            fontWeight: FontWeight.bold),
                      )),
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
