import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/ads.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/screens/ads/add_ads.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/loading.dart';
import 'package:car_user_project/widgets/no_data.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/material.dart';

import 'ads_details.dart';

class AdsHomeScreen extends StatefulWidget {

  @override
  _AdsHomeScreenState createState() => _AdsHomeScreenState();
}

class _AdsHomeScreenState extends State<AdsHomeScreen> {

  List<DocumentSnapshot> list2 = <DocumentSnapshot>[];
  List<DocumentSnapshot> list3 = <DocumentSnapshot>[];
  String state = 'فعالة';
  String price2='';

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getOldAds();
    getNewAds();
    getPrice();
  }
  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      appBar: AppBar(
        actions: [
          IconButton(
            icon: Icon(
              Icons.add_circle_rounded,
              color: Color(0xff1DB854),
              size: 30,
            ),
            onPressed: () {
              Navigator.push(context, MaterialPageRoute(builder: (context) => AddAdsScreen(price2),));
            },
          ),
        ],
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          'اعلاناتي',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color(0xffF1F2F3),
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: Color(0xffF1F2F3),
      body: SafeArea(
        child: Column(
          children: [
            Row(
              children: [
                SizedBox(
                  width: 5,
                ),
                TextButton(
                  onPressed: () {
                    showStateBottomSheet();
                  },
                  child: Text(
                    state != '' ? state : 'الحالة',
                    //expire
                    //continue
                    style: TextStyle(
                        color: Colors.black, fontWeight: FontWeight.w300),
                  ),
                ),
                Icon(
                  Icons.arrow_drop_down,
                  color: Colors.grey,
                ),
                SizedBox(
                  width: 5,
                ),
              ],
            ),
            Expanded(
                child: StreamBuilder<QuerySnapshot>(
                    stream: FirebaseFirestoreController().readAds(),
                    builder: (context, snapshot) {
                      if(snapshot.hasData && snapshot.data!.docs.length > 0){
                        List<DocumentSnapshot> documents = state != '' ? state == 'فعالة' ? list3 : list2 : snapshot.data!.docs;
                        return ListView.separated(
                          itemCount: documents.length,
                          clipBehavior: Clip.antiAlias,
                          physics: BouncingScrollPhysics(),
                          itemBuilder: (context, index) {
                            return GestureDetector(
                              onTap: (){
                                Navigator.push(context, MaterialPageRoute(builder: (context) => AdsDetailsScreen(getAds(documents[index])),));
                              },
                              child: adsWidgetShow(
                                tittle: documents[index].get('name'),
                                des: documents[index].get('description'),
                                url: documents[index].get('image'),
                              ),
                            );
                          }, separatorBuilder: (BuildContext context, int index) {
                          return SizedBox(height: 10,);
                        },
                        );
                      }else if(snapshot.connectionState == ConnectionState.waiting){
                        return Loading();
                      }else{
                        return NoData();
                      }
                    }
                )),
            SizedBox(height: 20,),
          ],
        ),
      ),
    );
  }

  Ads getAds(DocumentSnapshot snapshot){
    Ads ads = Ads();
    ads.path = snapshot.id;
    ads.time = snapshot.get('time');
    ads.image = snapshot.get('image');
    ads.description = snapshot.get('description');
    ads.name = snapshot.get('name');
    return ads;
  }

  Future<void> getOldAds() async{
    List<DocumentSnapshot> list = await FirebaseFirestoreController().getOldAds();
    setState(() {
      list2 = list;
    });
    print(list2.length.toString());
  }


  Future<void> getNewAds() async{
    List<DocumentSnapshot> list = await FirebaseFirestoreController().getNewAds();
    setState(() {
      list3 = list;
    });
    print(list3.length.toString());
  }

  void showStateBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RadioListTile(
                  title: Text('فعالة'),
                  value: 'فعالة',
                  groupValue: state,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        state = value;
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('منتهية'),
                  value: 'منتهية',
                  groupValue: state,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        state = value;
                      });

                    Navigator.pop(context);
                  }),
            ],
          ),
        );
      },
    );
  }
  Future<void> getPrice() async{
    String price = await FirebaseFirestoreController().getPrice();
    setState(() {
      price2 = price;
    });
  }

}
