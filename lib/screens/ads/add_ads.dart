import 'dart:io';

import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/firebase/firebase_storage.dart';
import 'package:car_user_project/models/ads.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/widgets/app_text_filed.dart';
import 'package:car_user_project/widgets/app_text_filed_des.dart';
import 'package:car_user_project/widgets/component.dart';
import 'package:car_user_project/widgets/product_count.dart';
import 'package:flutter/material.dart';
import 'package:future_progress_dialog/future_progress_dialog.dart';
import 'package:image_picker/image_picker.dart';

class AddAdsScreen extends StatefulWidget {
String finalPrice;

AddAdsScreen(this.finalPrice);

  @override
  _AddAdsScreenState createState() => _AddAdsScreenState();
}

class _AddAdsScreenState extends State<AddAdsScreen> {
  XFile? pickedImage;
  XFile? pickedImage2;
  ImagePicker imagePicker = ImagePicker();
  String selectedImage = '';
  String type = '';
  String type2 = '';
  String price2 = '';
  String endTime = '';
  late TextEditingController _tittle;
  late TextEditingController _des;
  late TextEditingController _time;
  bool _home = false;
  bool _default = false;
  static const IconData minimize =
  IconData(0xe3e8, fontFamily: 'MaterialIcons');

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserType();
    _tittle = TextEditingController();
    _des = TextEditingController();
    _time = TextEditingController(text: '3');
    print(widget.finalPrice.toString());
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _tittle.dispose();
    _des.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        iconTheme: IconThemeData(color: Color(0xff1DB854)),
        title: Text(
          'اضافة اعلان',
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color(0xffF1F2F3),
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: Color(0xffF1F2F3),
      body: Padding(
        padding: const EdgeInsets.symmetric(horizontal: 15),
        child: ListView(
          physics: BouncingScrollPhysics(),
          children: [
            SizedBox(
              height: 200,
              child:  Card(
                color: Color(0xffF1F2F3),
                elevation: 0,
                clipBehavior: Clip.antiAlias,
                child:pickedImage2 != null ? Image.file(File(pickedImage2!.path), fit: BoxFit.cover,) :IconButton(onPressed: (){pickImage();},icon: Icon(Icons.cloud_upload_outlined,size: 150,color: Color(0xff1DB854),),),
                shape: BeveledRectangleBorder(
                  borderRadius: BorderRadius.circular(5.0),
                ),
              ),
            ),
            SizedBox(
              height: 10,
            ),
            AppTextFiled(
              labelText: 'عنوان الاعلان',
              controller: _tittle,
              prefix: Icons.title,
            ),
            SizedBox(
              height: 10,
            ),
            AppTextFiledDescription(
              maxLe: 50,
              labelText: 'وصف الاعلان',
              controller: _des,
              prefix: Icons.task_rounded,
            ),
            SizedBox(
              height: 15,
            ),
            AppTextFiledDescription(
              onTap: (){showTimeBottomSheet();},
              readOnly: true,
              maxLe: 50,
              labelText: 'مدة الاعلان',
              controller: _time,
              prefix: Icons.access_time,
            ),
            SizedBox(height: 195,),
            Container(
              alignment: AlignmentDirectional.bottomCenter,
              color: Colors.transparent,
              child: Column(
                children: [
                  Row(
                    children: [
                      SizedBox(
                        width: 20,
                      ),
                      Text(
                        'المجموع الكلي',
                        style: TextStyle(
                            fontWeight: FontWeight.w600, fontSize: 18),
                      ),
                      Spacer(),
                      Text(
                        (int.tryParse(widget.finalPrice.toString())! * int.parse(_time.text.toString())).toString() + "\$",
                        style: TextStyle(
                            fontSize: 18,
                            color: Color(0xff1DB854),
                            fontWeight: FontWeight.bold),
                      ),
                      SizedBox(
                        width: 20,
                      ),
                    ],
                  ),
                  SizedBox(
                    height: 25,
                  ),
                  Row(children: [
                    Container(
                      margin: EdgeInsetsDirectional.only(
                          start: 0, bottom: 0, end: 0, top: 20),
                    ),
                    Expanded(
                      child: ElevatedButton(
                        onPressed: () {
                          addAds();
                        },
                        child: Text(
                          'تاكيد',
                          style: TextStyle(
                              fontWeight: FontWeight.bold, fontSize: 20),
                        ),
                        style: ElevatedButton.styleFrom(
                          minimumSize: Size(
                              double.infinity, SizeConfig().scaleHeight(60)),
                          primary: Color(0xff1DB854),
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(25),
                          ),
                        ),
                      ),
                    ),
                  ]),
                ],
              ),
            ),
            SizedBox(
              height: 10,
            ),
          ],
        ),
      ),
    );
  }

  void showTimeBottomSheet() {
    showModalBottomSheet(
      barrierColor: Colors.black.withOpacity(0.25),
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(15),
      ),
      context: context,
      builder: (context) {
        return Padding(
          padding: EdgeInsets.symmetric(vertical: 15),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: [
              RadioListTile(
                  title: Text('3 ايام'),
                  value: '3',
                  groupValue: _time.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _time.text = value;
                        endTime = DateTime.now().add(Duration(days: 3)).toString().substring(0, 10);
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('10 ايام'),
                  value: '10',
                  groupValue: _time.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _time.text = value;
                        endTime = DateTime.now().add(Duration(days: 10)).toString().substring(0, 10);
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('20 ايام'),
                  value: '20',
                  groupValue: _time.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _time.text = value;
                        endTime = DateTime.now().add(Duration(days: 20)).toString().substring(0, 10);
                      });

                    Navigator.pop(context);
                  }),
              RadioListTile(
                  title: Text('30 ايام'),
                  value: '30',
                  groupValue: _time.text,
                  onChanged: (String? value) {
                    if (value != null)
                      setState(() {
                        _time.text = value;
                        endTime = DateTime.now().add(Duration(days: 30)).toString().substring(0, 10);
                      });

                    Navigator.pop(context);
                  }),
            ],
          ),
        );
      },
    );
  }

  bool checkData() {
    if(_tittle.text.isNotEmpty && _des.text.isNotEmpty && _time.text.isNotEmpty){
      return true;
    }else{
      return false;
    }
  }

  Future<void> addAdsProses() async{
    bool state = await FirebaseFirestoreController().addAds(getAds());
    if(state){
      Navigator.pop(context);
    }
  }

  Future<void> addAds() async{
    if(checkData()){
      showDialogWidget(context2: context,
          message: 'تم إضافة الاعلان بنجاح',
          sub: '',
          type: 'رجوع',
          function: (){
            Navigator.pop(context);
          }
      );
      await addAdsProses();
    }
  }

  Ads getAds(){
    Ads ads = Ads();
    ads.name = _tittle.text;
    ads.description = _des.text;
    ads.image = selectedImage;
    ads.time = _time.text;
    ads.price = (widget.finalPrice.toString())* int.parse(_time.text.toString());
    ads.end = endTime;
    return ads;
  }

  Future<void> pickImage() async {
    pickedImage = await imagePicker.pickImage(source: ImageSource.gallery);
    if (pickedImage != null) {
      showDialog(
        context: context,
        builder: (context) => FutureProgressDialog(uploadImage(),
            message: Text('جاري تحميل الصورة...')),
      );
      setState(() {
        pickedImage2 = pickedImage;
      });
    }
  }

  Future<void> uploadImage() async {
    selectedImage = await FirebaseStorageController().uploadImage(File(pickedImage!.path), 'car');
  }

  Future<void> getUserType() async{
    String ds = await FirebaseFirestoreController().getUserType();
    print(ds);
    setState(() {
      type2 = ds;
    });
  }

}