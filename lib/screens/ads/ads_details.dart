import 'package:car_user_project/firebase/firebase_firestore.dart';
import 'package:car_user_project/models/ads.dart';
import 'package:car_user_project/models/user.dart';
import 'package:car_user_project/responsive/size_config.dart';
import 'package:car_user_project/screens/home/chat/chat_detailes_screen.dart';
import 'package:car_user_project/screens/home/details/detials_screen.dart';
import 'package:car_user_project/widgets/app_text_filed.dart';
import 'package:car_user_project/widgets/app_text_filed_des.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class AdsDetailsScreen extends StatefulWidget {

  Ads ads;

  AdsDetailsScreen(this.ads);

  @override
  _AdsDetailsScreenState createState() => _AdsDetailsScreenState();
}

class _AdsDetailsScreenState extends State<AdsDetailsScreen> {
  late TextEditingController _tittle;
  late TextEditingController _des;
  String type = '';
  String phone = '';
  DocumentSnapshot? documentSnapshotUser;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserType();
    _tittle = TextEditingController(text: widget.ads.name);
    _des = TextEditingController(text: widget.ads.description);
  }

  @override
  void dispose() {
    // TODO: implement dispose
    _tittle.dispose();
    _des.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    SizeConfig().designWidth(4.12).designHeight(8.70).init(context);
    return Scaffold(
      appBar: AppBar(
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back_ios_outlined,
            color: Color(0xff1DB854),
          ),
          onPressed: () {
            Navigator.pop(context);
          },
        ),
        iconTheme: IconThemeData(color: Colors.black),
        title: Text(
          widget.ads.name,
          style: TextStyle(color: Colors.black),
        ),
        backgroundColor: Color(0xffF1F2F3),
        elevation: 0,
        centerTitle: true,
      ),
      backgroundColor: Color(0xffF1F2F3),
      body: SafeArea(
        child: Stack(
          children: [
            Column(
              children: [
                SizedBox(
                  height: 220,
                  child: Stack(
                    alignment: AlignmentDirectional.bottomEnd,
                    children: [
                      Container(
                        height: 220,
                        width: double.infinity,
                        decoration: BoxDecoration(
                          borderRadius: BorderRadius.circular(20),
                          // border: Border.all(width: 0, color: Colors.white),
                          color: Colors.transparent,
                        ),
                        child: Card(
                          elevation: 0,
                          clipBehavior: Clip.antiAlias,
                          color: Colors.transparent,
                          child: Image.network(
                            widget.ads.image,
                            fit: BoxFit.cover,
                            height: double.infinity,
                            width: double.infinity,
                          ),
                          shape: BeveledRectangleBorder(
                            borderRadius: BorderRadius.circular(5.0),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
                AppTextFiled(
                  labelText: 'عنوان الاعلان',
                  controller: _tittle,
                  prefix: Icons.title,
                ),
                SizedBox(
                  height: 10,
                ),
                AppTextFiledDescription(
                  maxLe: 50,
                  labelText: 'وصف الاعلان',
                  controller: _des,
                  prefix: Icons.task_rounded,
                ),
                SizedBox(
                  height: 15,
                ),
              ],
            ),
            Positioned(
              bottom: 10,
              left: 0,
              right: 0,
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                mainAxisSize: MainAxisSize.min,
                children: [
                  SizedBox(
                    width: 5,
                  ),
                  Visibility(
                    visible: FirebaseAuth.instance.currentUser!.email != widget.ads.email,
                    child: IconButton(
                        onPressed: () {launch("tel://${documentSnapshotUser!.get('phoneNumber')}");},
                        icon: Icon(
                          Icons.call,
                          color: Color(0xff1DB854),
                        )),
                  ),
                  Visibility(
                    visible: FirebaseAuth.instance.currentUser!.email != widget.ads.email,
                    child: IconButton(
                        onPressed: ()async {
                          Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) =>
                                      ChatScreenDetails(
                                          getUser())));
                        },
                        icon: Icon(
                          Icons.message,
                          color: Color(0xff1DB854),
                        )),
                  ),
                  Icon(
                    Icons.location_on_outlined,
                    color: Color(0xff1DB854),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                  Text(
                    documentSnapshotUser != null ? documentSnapshotUser!.get('place') : '',
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.w300),
                  ),
                  SizedBox(
                    width: 5,
                  ),
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }

  Users getUser()  {
    Users users = Users();
    if(documentSnapshotUser != null){
      users.email = documentSnapshotUser!.get('email');
      users.image = documentSnapshotUser!.get('image');
      users.name = documentSnapshotUser!.get('name');
      users.phoneNumber = documentSnapshotUser!.get('phoneNumber');
    }
    return users;
  }

  Future<void> getUserType() async{
    String ds = await FirebaseFirestoreController().getUserType2(widget.ads.email);
    print(ds);
    setState(() {
      type = ds;
    });
    getUserData();
  }

  Future<void> getUserData() async{
    print('Type = ' + type);
    DocumentSnapshot ds = await FirebaseFirestoreController().getUserData2(type, widget.ads.email);
    setState(() {
      documentSnapshotUser = ds;
    });
  }

}
