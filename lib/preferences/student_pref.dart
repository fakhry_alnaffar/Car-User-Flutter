import 'package:shared_preferences/shared_preferences.dart';

class AppPreferences {
  static final AppPreferences _instance = AppPreferences._internal();
  late SharedPreferences _sharedPreferences;

  factory AppPreferences() {
    return _instance;
  }

  AppPreferences._internal();

  Future<void> initPreferences() async {
    _sharedPreferences = await SharedPreferences.getInstance();
  }

  Future<void> setPlace(String place) async{
    await _sharedPreferences.setString('place', place);
  }

  String getPlace() {
    return _sharedPreferences.getString('place') ?? '';
  }

}
