import 'package:flutter/material.dart';

class ProductCount extends StatelessWidget {
  final Widget icon;

  ProductCount({required this.icon});

  @override
  Widget build(BuildContext context) {
    return Card(
      child: Padding(
          padding: EdgeInsets.all(3),
          child: icon
      ),
      color: Color(0xff1DB854),
      elevation: 0,
    );
  }
}