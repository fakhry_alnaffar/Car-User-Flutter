import 'package:car_user_project/responsive/size_config.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

Widget homeWidget(
    {required var icon,
    required String tittle,
    required String counterAndType,
    required Color iconColor}) {
  return Stack(
    children: [
      Container(
        width: SizeConfig().scaleWidth(171),
        height: SizeConfig().scaleHeight(171),
        decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(14),
          boxShadow: [
            BoxShadow(
              color: Colors.black.withOpacity(0.16),
              spreadRadius: 0,
              blurRadius: 4,
              offset: Offset(0, 4), // changes position of shadow
            ),
          ],
        ),
      ),
      Center(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: [
            Container(
              height: SizeConfig().scaleHeight(59),
              width: SizeConfig().scaleWidth(59),
              child: Icon(
                icon,
                color: iconColor,
                size: SizeConfig().scaleWidth(40),
              ),
              decoration: BoxDecoration(
                color: Color(0xFFF0F4FD),
                borderRadius: BorderRadius.circular(14),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(16),
            ),
            Text(
              tittle,
              style: TextStyle(
                fontSize: SizeConfig().scaleTextFont(16),
                fontWeight: FontWeight.w500,
                color: Color(0xff0B204C),
              ),
            ),
            SizedBox(
              height: SizeConfig().scaleHeight(8),
            ),
            Text(
              '$counterAndType',
              style: TextStyle(
                  fontSize: SizeConfig().scaleTextFont(16),
                  fontWeight: FontWeight.w500,
                  color: Color(0xFFB2BAC9)),
            ),
          ],
        ),
      )
    ],
  );
}

Widget showDistributors({
  // required List<DocumentSnapshot> documents,
  required int index,
  required BuildContext context,
  required String title,
  required String subtitle,
  required String adders,
  String? url,
  required Widget widget1,
  required Widget widget2,
}) {
  return Container(
      padding: EdgeInsetsDirectional.zero,
      margin: EdgeInsetsDirectional.zero,
      clipBehavior: Clip.antiAlias,
      // alignment: AlignmentDirectional.topCenter,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 10.0, bottom: 20),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            mainAxisAlignment: MainAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            textBaseline: TextBaseline.ideographic,
            textDirection: TextDirection.rtl,
            children: [
              SizedBox(
                width: 15,
              ),
              widget1,
              SizedBox(
                width: 10,
              ),
              widget2,
              Spacer(),
              SizedBox(
                width: 0,
              ),
              SizedBox(
                width: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.start,
                  mainAxisSize: MainAxisSize.max,
                  textBaseline: TextBaseline.alphabetic,
                  textDirection: TextDirection.rtl,
                  children: [
                    Text(
                      title,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                    ),
                    Text(
                      subtitle,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontSize: 14,
                          textBaseline: TextBaseline.ideographic),
                    ),
                    SizedBox(
                      height: 1,
                    ),
                    Text(
                      adders,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Colors.grey.shade600,
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                    ),
                    SizedBox(
                      height: 1,
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 0),
                child: CircleAvatar(
                  radius: 45,
                  child: url == null ? Icon(Icons.person) : null,
                  backgroundColor: Color(0xff1DB854),
                  backgroundImage: url != null ? NetworkImage(url) : null,
                ),
              ),
            ],
          ),
        ),
      ));
}

Widget showItemHome({
  required String name,
  required String adders,
  required String image,
  required Widget widget,
}) {
  return Container(
    clipBehavior: Clip.antiAlias,
    margin: EdgeInsetsDirectional.only(start: 5, end: 5, top: 0, bottom: 0),
    padding: EdgeInsetsDirectional.zero,
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(10),
    ),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: Card(
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            margin: EdgeInsetsDirectional.zero,
            child: Image.network(
              image,
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
            ),
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        ),
        Card(
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsetsDirectional.zero,
          child: Stack(
            clipBehavior: Clip.antiAlias,
            children: [
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  alignment: AlignmentDirectional.bottomStart,
                  margin: EdgeInsetsDirectional.only(
                      start: 5, end: 5, bottom: 0, top: 0),
                  padding: EdgeInsetsDirectional.zero,
                  height: 55,
                  color: Colors.white70,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 20,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsetsDirectional.only(
                                  start: 5, bottom: 0, end: 0, top: 0),
                              padding: EdgeInsetsDirectional.zero,
                              child: Text(
                                name,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                            // Expanded(
                            //   child: Align(
                            //     alignment: AlignmentDirectional.centerEnd,
                            //     child: Container(
                            //       alignment: AlignmentDirectional.centerEnd,
                            //       padding: EdgeInsetsDirectional.zero,
                            //       child: IconButton(
                            //         onPressed: ()async{
                            //           //add to fav
                            //         },
                            //         icon: Icon(Icons.favorite_border,size: 22,color: Colors.red,),
                            //         padding: EdgeInsetsDirectional.zero,
                            //         alignment: AlignmentDirectional.centerEnd,
                            //       ),
                            //     ),
                            //   ),
                            // ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 25,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsetsDirectional.only(
                                  start: 5, bottom: 0, top: 0, end: 0),
                              padding: EdgeInsetsDirectional.zero,
                              child: Text(
                                adders,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff026b28),
                                  fontSize: 16,
                                ),
                              ),
                            ),
                            Expanded(
                              child: Align(
                                alignment: AlignmentDirectional.centerEnd,
                                child: Container(
                                  padding:
                                      EdgeInsetsDirectional.only(bottom: 120),
                                  margin: EdgeInsetsDirectional.only(
                                      bottom: 120, start: 5),
                                  child: widget,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ],
    ),
  );
}

Widget showChat({
  // required List<DocumentSnapshot> documents,
  // required int index,
  required BuildContext context,
  required String title,
  required String subtitle,
  String? url,
}) {
  return Container(
      padding: EdgeInsetsDirectional.zero,
      margin: EdgeInsetsDirectional.zero,
      clipBehavior: Clip.antiAlias,
      // alignment: AlignmentDirectional.topCenter,
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 0.0, bottom: 0),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(50),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            crossAxisAlignment: CrossAxisAlignment.baseline,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.max,
            textBaseline: TextBaseline.ideographic,
            textDirection: TextDirection.rtl,
            children: [
              SizedBox(
                width: 15,
              ),
              Spacer(),
              SizedBox(
                width: 150,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.max,
                  textBaseline: TextBaseline.alphabetic,
                  textDirection: TextDirection.rtl,
                  children: [
                    Text(
                      title,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Colors.black,
                          fontWeight: FontWeight.bold,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.center,
                    ),
                    Text(
                      subtitle,
                      locale: Locale('ar'),
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      textDirection: TextDirection.ltr,
                      textAlign: TextAlign.start,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontSize: 14,
                          textBaseline: TextBaseline.ideographic),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 19, bottom: 16, left: 16),
                child: Container(
                  clipBehavior: Clip.antiAlias,
                  width: 50,
                  height: 50,
                  decoration: BoxDecoration(
                    borderRadius: BorderRadius.circular(100),
                  ),
                  child: url == null
                      ? Icon(Icons.person)
                      : Image.network(
                          url,
                          fit: BoxFit.cover,
                        ),
                ),
              ),
            ],
          ),
        ),
      ));
}

Widget me(BuildContext context, String message, String time) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.end,
        children: [
          SizedBox(
            width: 10,
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Color(0xff1DB854),
            ),
            padding: EdgeInsets.all(8),
            constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  message,
                  style: TextStyle(
                    color: Colors.white,
                  ),
                ),
                SizedBox(
                  width: 75,
                  child: Align(
                    alignment: AlignmentDirectional.topEnd,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      // crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        // isNotRead
                        // Icon(Icons.done,size: 18,color: Colors.white,),
                        // isRead
                        Icon(
                          Icons.done_all,
                          size: 18,
                          color: Colors.white,
                        ),
                        SizedBox(
                          width: 5,
                        ),
                        Spacer(),
                        Text(
                          time,
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
      // SizedBox(height: 5,),
      //
      // Row(
      //   mainAxisAlignment: MainAxisAlignment.start,
      //   children: [
      //     Text('11:30 PM',
      //       style: TextStyle(
      //       color: Colors.grey.shade600,
      //         fontSize: 12
      //     ),)
      //   ],
      // )
    ],
  );
}

Widget other(BuildContext context, String message, String time, String image) {
  return Column(
    children: [
      Row(
        mainAxisAlignment: MainAxisAlignment.start,
        children: [
          CircleAvatar(
            radius: 20,
            backgroundImage: NetworkImage(image),
          ),
          SizedBox(
            width: 10,
          ),
          Container(
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              color: Colors.grey.shade700,
            ),
            padding: EdgeInsets.all(8),
            constraints: BoxConstraints(
                maxWidth: MediaQuery.of(context).size.width * 0.5),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  message,
                  style: TextStyle(color: Colors.white),
                ),
                SizedBox(
                  height: 5,
                ),
                // Spacer(),
                SizedBox(
                  width: 75,
                  child: Align(
                    alignment: AlignmentDirectional.topEnd,
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.start,
                      // crossAxisAlignment: CrossAxisAlignment.end,
                      children: [
                        // isNotRead
                        Icon(
                          Icons.done_all,
                          size: 18,
                          color: Colors.white,
                        ),
                        // isRead
                        // Icon(Icons.done_all,size: 18,color: Colors.white,),
                        SizedBox(
                          width: 5,
                        ),
                        Spacer(),
                        Text(
                          time,
                          textAlign: TextAlign.end,
                          style: TextStyle(
                            color: Colors.white,
                            fontSize: 12,
                          ),
                        ),
                      ],
                    ),
                  ),
                )
              ],
            ),
          ),
        ],
      ),
      // SizedBox(height: 5,),
      //
      // Row(
      //   mainAxisAlignment: MainAxisAlignment.start,
      //   children: [
      //     Text('11:30 PM',
      //       style: TextStyle(
      //       color: Colors.grey.shade600,
      //         fontSize: 12
      //     ),)
      //   ],
      // )
    ],
  );
}

Widget adsWidgetShow({
  required String url,
  required String tittle,
  required String des,
}) {
  return SizedBox(
    height: 200,
    child: Container(
      margin: EdgeInsetsDirectional.only(start: 5, end: 5, bottom: 10),
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(borderRadius: BorderRadius.circular(10)),
      child: Stack(
        children: [
          Image.network(
            url,
            fit: BoxFit.cover,
            height: double.infinity,
            width: double.infinity,
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
                alignment: AlignmentDirectional.centerEnd,
                height: 70,
                color: Colors.white70,
                child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 10),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    textDirection: TextDirection.rtl,
                    children: [
                      SizedBox(
                        height: 4,
                      ),
                      Text(
                        tittle,
                        style: TextStyle(
                            fontSize: 18,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff012911)),
                      ),
                      Text(
                        des,
                        style: TextStyle(
                            fontSize: 16,
                            fontWeight: FontWeight.bold,
                            color: Color(0xff2f2f2f)),
                      ),
                      SizedBox(
                        height: 4,
                      ),
                    ],
                  ),
                )),
          ),
        ],
      ),
    ),
  );
}

Widget selectWidget({
  required String tittle,
  required Widget widget,
}) {
  return Container(
    decoration: BoxDecoration(
      color: Color(0xff1DB854),
      borderRadius: new BorderRadius.only(
        topLeft: const Radius.circular(10.0),
        topRight: const Radius.circular(10.0),
        bottomRight: const Radius.circular(10.0),
        bottomLeft: const Radius.circular(10.0),
      ),
    ),
    height: 180,
    width: 170,
    child: Align(
      alignment: AlignmentDirectional.center,
      child: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          widget,
          Text(
            tittle,
            style: TextStyle(
                color: Colors.white, fontSize: 24, fontWeight: FontWeight.bold),
          ),
        ],
      ),
    ),
  );
}

Widget showItemHomeController({
  required String name,
  required String adders,
  required String image,
  required Widget widget,
  required void Function() onPressedDelete,
  required void Function() onPressedUpdate,
}) {
  return Container(
    clipBehavior: Clip.antiAlias,
    margin: EdgeInsetsDirectional.only(start: 0, end: 0, top: 0, bottom: 0),
    padding: EdgeInsetsDirectional.zero,
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(10),
    ),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: Card(
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            margin: EdgeInsetsDirectional.zero,
            child: Image.network(
              image,
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
            ),
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.only(
                topRight: Radius.circular(10),
                topLeft: Radius.circular(10),
              ),
            ),
          ),
        ),
        Card(
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsetsDirectional.zero,
          child: Stack(
            clipBehavior: Clip.antiAlias,
            children: [
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  alignment: AlignmentDirectional.bottomStart,
                  margin: EdgeInsetsDirectional.only(
                      start: 5, end: 5, bottom: 0, top: 0),
                  padding: EdgeInsetsDirectional.zero,
                  height: 60,
                  color: Colors.white70,
                  child: Column(
                    children: [
                      SizedBox(
                        height: 30,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsetsDirectional.only(
                                  start: 5, bottom: 0, end: 0, top: 0),
                              padding: EdgeInsetsDirectional.zero,
                              child: Text(
                                name,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Colors.black,
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(
                        height: 25,
                        child: Row(
                          children: [
                            Container(
                              margin: EdgeInsetsDirectional.only(
                                  start: 5, bottom: 0, top: 0, end: 0),
                              padding: EdgeInsetsDirectional.zero,
                              child: Text(
                                adders,
                                style: TextStyle(
                                  fontWeight: FontWeight.bold,
                                  color: Color(0xff026b28),
                                  fontSize: 16,
                                ),
                              ),
                            ),
                          ],
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0),
          ),
        ),
        Card(
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsetsDirectional.zero,
          child: Stack(clipBehavior: Clip.antiAlias, children: [
            Align(
              alignment: Alignment.bottomRight,
              child: Container(
                alignment: AlignmentDirectional.bottomStart,
                margin: EdgeInsetsDirectional.only(
                    start: 0, end: 0, bottom: 0, top: 0),
                padding: EdgeInsetsDirectional.zero,
                height: 40,
                color: Colors.white70,
                child: Stack(children: [
                  Column(
                    children: [
                      SizedBox(
                        height: 40,
                        child: Align(
                          alignment: Alignment.bottomCenter,
                          child: Container(
                            alignment: AlignmentDirectional.centerEnd,
                            height: 50,
                            color: Colors.white,
                            child: Row(children: [
                              Container(
                                margin: EdgeInsetsDirectional.only(
                                    start: 0, bottom: 0, end: 0, top: 0),
                              ),
                              Expanded(
                                child: ElevatedButton.icon(
                                  onPressed: onPressedUpdate,
                                  icon: Icon(
                                    Icons.edit,
                                    size: 18,
                                  ),
                                  label: Text('تعديل'),
                                  style: ElevatedButton.styleFrom(
                                    minimumSize: Size(double.infinity,
                                        SizeConfig().scaleHeight(60)),
                                    primary: Color(0xff1DB854),
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                  ),
                                ),
                              ),
                              SizedBox(
                                width: 1,
                              ),
                              Expanded(
                                child: ElevatedButton.icon(
                                  onPressed: onPressedDelete,
                                  icon: Icon(
                                    Icons.delete,
                                    size: 18,
                                  ),
                                  label: Text('حذف'),
                                  style: ElevatedButton.styleFrom(
                                    minimumSize: Size(double.infinity,
                                        SizeConfig().scaleHeight(60)),
                                    primary: Colors.redAccent,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(0),
                                    ),
                                  ),
                                ),
                              ),
                            ]),
                          ),
                        ),
                      ),
                    ],
                  ),
                ]),
              ),
            ),
          ]),
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0),
          ),
        ),
      ],
    ),
  );
}

Widget showItemHomeAdders({
  required String name,
  Widget? widget,
}) {
  return Container(
    clipBehavior: Clip.antiAlias,
    margin: EdgeInsetsDirectional.only(start: 5, end: 5, top: 0, bottom: 0),
    padding: EdgeInsetsDirectional.zero,
    decoration: BoxDecoration(
      color: Colors.white,
      borderRadius: BorderRadius.circular(10),
    ),
    child: Column(
      mainAxisSize: MainAxisSize.min,
      children: [
        Expanded(
          child: Card(
            clipBehavior: Clip.antiAlias,
            color: Colors.white,
            margin: EdgeInsetsDirectional.zero,
            child: Image.network(
              'https://cdn.vox-cdn.com/thumbor/Sk0PlZgKCM6p3YS_4sMW2h6xVlQ=/1400x1050/filters:format(png)/cdn.vox-cdn.com/uploads/chorus_asset/file/19700731/googlemaps.png',
              fit: BoxFit.cover,
              height: double.infinity,
              width: double.infinity,
            ),
            elevation: 0,
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(5),
            ),
          ),
        ),
        Card(
          clipBehavior: Clip.antiAlias,
          margin: EdgeInsetsDirectional.zero,
          child: Stack(
            clipBehavior: Clip.antiAlias,
            children: [
              Align(
                alignment: Alignment.bottomRight,
                child: Container(
                  alignment: AlignmentDirectional.bottomStart,
                  margin: EdgeInsetsDirectional.only(
                      start: 5, end: 5, bottom: 0, top: 0),
                  padding: EdgeInsetsDirectional.zero,
                  height: 40,
                  color: Colors.white70,
                  child: Column(
                    children: [
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: [
                          Spacer(),
                          Text(
                            name,
                            style: TextStyle(
                              fontWeight: FontWeight.bold,
                              color: Colors.black,
                              fontSize: 16,
                            ),
                          ),
                          Spacer(),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ],
          ),
          elevation: 0,
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(10),
          ),
        ),
      ],
    ),
  );
}

Widget showItem({
  required BuildContext context,
  required String title,
  required String subtitle,
  String? url,
  required int index,
  required Widget widget1,
  required Widget widget2,
  // Locale  = Locale('ar'),
  required List<DocumentSnapshot> documents,
}) {
  return Container(
      decoration: BoxDecoration(
        color: Colors.white,
        borderRadius: BorderRadius.circular(15),
      ),
      child: Padding(
        padding: const EdgeInsets.only(top: 3.0, bottom: 3),
        child: Container(
          height: SizeConfig().scaleHeight(90),
          width: SizeConfig().scaleWidth(366),
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.circular(14),
          ),
          child: Row(
            children: [
              widget1,
              widget2,
              Expanded(
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.baseline,
                  mainAxisAlignment: MainAxisAlignment.center,
                  mainAxisSize: MainAxisSize.min,
                  textBaseline: TextBaseline.ideographic,
                  children: [
                    Text(
                      title,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      softWrap: false,
                      style: TextStyle(
                          color: Color(0xff0B204C),
                          fontWeight: FontWeight.w400,
                          fontSize: 16,
                          textBaseline: TextBaseline.ideographic),
                      textDirection: TextDirection.rtl,
                      textAlign: TextAlign.start,
                      locale: Locale('ar'),
                    ),
                    Text(
                      subtitle,
                      maxLines: 1,
                      overflow: TextOverflow.ellipsis,
                      style: TextStyle(
                          color: Color(0xff919BB3),
                          fontSize: 13,
                          textBaseline: TextBaseline.ideographic),
                    ),
                  ],
                ),
              ),
              SizedBox(
                width: 10,
              ),
              Padding(
                padding: const EdgeInsets.only(top: 16, bottom: 16, left: 0),
                child: CircleAvatar(
                  backgroundImage: url != null ? NetworkImage(url) : null,
                  radius: 25,
                  child: url == null ? Icon(Icons.person) : null,
                  backgroundColor: Color(0xff5A55CA),
                ),
              ),
            ],
          ),
        ),
      ));
}

void showDialogWidget(
    {required BuildContext context2,
    required String message,
    required String sub,
    required Function function,
    required String type}) {
  showDialog(
      barrierColor: Colors.black.withOpacity(0.16),
      context: context2,
      builder: (context2) {
        return AlertDialog(
          backgroundColor: Color(0xff1DB854),
          clipBehavior: Clip.antiAlias,
          contentPadding: EdgeInsetsDirectional.zero,
          actionsPadding: EdgeInsetsDirectional.only(bottom: 20,top: 10),
          title: Center(
              child: Text(
                message,
                textAlign: TextAlign.center,
                style: TextStyle(color: Colors.white),
              )),
          content: SizedBox(
            width: 50,
            child: Text(
              sub,
              textAlign: TextAlign.center,
              style: TextStyle(color: Colors.white,fontWeight: FontWeight.w300),
            ),
          ),
          actions: [
            Container(
              alignment: AlignmentDirectional.bottomStart,
              margin: EdgeInsetsDirectional.only(
                  start: 0, end: 0, bottom: 0, top: 0),
              padding: EdgeInsetsDirectional.zero,
              height: 60,
              color: Color(0xff1DB854),
              child: Stack(children: [
                Column(
                  children: [
                    SizedBox(
                      height: 60,
                      child: Align(
                        alignment: Alignment.bottomCenter,
                        child: Container(
                          alignment: AlignmentDirectional.centerEnd,
                          height: 60,
                          padding: EdgeInsetsDirectional.zero,
                          width: double.infinity,
                          color: Colors.transparent,
                          child: Row(children: [
                            Expanded(
                              child: Padding(
                                padding: const EdgeInsets.symmetric(horizontal: 60.0),
                                child: TextButton(
                                  onPressed: (){
                                    function();
                                  },
                                  child: Text(
                                    type,
                                    textAlign: TextAlign.center,
                                    style: TextStyle(
                                        color: Color(0xff1DB854),
                                        fontWeight: FontWeight.w600,
                                        fontSize: 18),
                                  ),
                                  style: ElevatedButton.styleFrom(
                                    minimumSize: Size(double.infinity,
                                        SizeConfig().scaleHeight(80)),
                                    primary: Colors.white,
                                    shape: RoundedRectangleBorder(
                                      borderRadius: BorderRadius.circular(50),
                                    ),
                                  ),
                                ),
                              ),
                            ),
                          ]),
                        ),
                      ),
                    ),
                  ],
                ),
              ]),
            ),
          ],
// actionsPadding: EdgeInsets.symmetric(horizontal: 50),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(15),
          ),
        );
      });
}
