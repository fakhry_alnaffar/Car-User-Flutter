import 'package:car_user_project/responsive/size_config.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AppTextFiledDescription extends StatelessWidget {
  final TextInputType textInputType;
  final String labelText;
  final int? maxLength;
  final int? maxLe;
  final TextEditingController controller;
  final bool obscureText;
  final bool readOnly;
  final bool showCursor;
  Function? functionSuffixPressed;
  final IconData? suffix;
  final IconData? prefix;
  Function()? onTap;

  AppTextFiledDescription(
      {this.textInputType = TextInputType.text,
        required this.labelText,
        this.maxLength,
        this.maxLe,
        this.suffix = null,
        this.prefix = null,
        this.obscureText = false,
        this.readOnly = false,
        this.showCursor = true,
        required this.controller,
        this.onTap,
        this.functionSuffixPressed});

  @override
  Widget build(BuildContext context) {
    return TextFormField(
      controller: controller,
      maxLength: maxLength,
      maxLines: maxLe,
      minLines: 1,
      showCursor: showCursor,
      textDirection: TextDirection.rtl,
      cursorColor: Color(0xff1DB854),
      autocorrect: true,
      enableSuggestions: true,
      readOnly: readOnly,
      keyboardType: textInputType,
      obscureText: obscureText,
      onTap: onTap,
      decoration: InputDecoration(
        labelText: labelText,
        labelStyle: TextStyle(color: Color(0xff8E8E93)),
        counterText: '',
        enabledBorder: borderEnable,
        focusedBorder: borderFocused,
        suffixIcon: suffix != null
            ? IconButton(
            onPressed: () {
              functionSuffixPressed!();
            },
            icon: Icon(
              suffix,
              color: Color(0xff1DB854),
            ))
            : null,
        prefixIcon: Icon(
          prefix,
          color: Color(0xff1DB854),
        ),
      ),
    );
  }

  UnderlineInputBorder get borderEnable => UnderlineInputBorder(
    borderRadius: BorderRadius.circular(
      SizeConfig().scaleWidth(10),
    ),
    borderSide: BorderSide(
      color: Color(0xffD1D1D6),
      width: SizeConfig().scaleWidth(2),
    ),
  );

  UnderlineInputBorder get borderFocused => UnderlineInputBorder(
    borderRadius: BorderRadius.circular(
      SizeConfig().scaleWidth(10),
    ),
    borderSide: BorderSide(
      color: Color(0xff1DB854),
      width: SizeConfig().scaleWidth(2),
    ),
  );
}