import 'package:flutter/material.dart';

// ignore: must_be_immutable
class OrderItemList extends StatelessWidget {
  String name = '';
  String qty = '';
  String price = '';

  OrderItemList(this.name, this.qty, this.price);

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
        borderRadius: new BorderRadius.only(
          topLeft: const Radius.circular(5.0),
          topRight: const Radius.circular(5.0),
          bottomRight: const Radius.circular(5.0),
          bottomLeft: const Radius.circular(5.0),
        ),
        gradient: LinearGradient(
          begin: AlignmentDirectional.topStart,
          end: AlignmentDirectional.bottomEnd,
          colors: [
            Color(0xff1DB854),
            Color(0xe21db854),
          ],
        ),
      ),
      width: double.infinity,
      margin: EdgeInsetsDirectional.only(start: 2, end: 2),
      height: 50,
      child: Row(
        children: [
          SizedBox(
            width: 20,
          ),
          SizedBox(
            width: 100,
            child: Text(
              name,
              style: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(
            width: 40,
          ),
          SizedBox(
            width: 40,
            child: Text(
              qty,
              style: TextStyle(color: Colors.white),
            ),
          ),
          SizedBox(
            width: 80,
          ),
          Text(
            price,
            style: TextStyle(color: Colors.white),
          ),
          SizedBox(
            width: 10,
          ),
        ],
      ),
    );
  }
}

