import 'package:car_user_project/responsive/size_config.dart';
import 'package:flutter/material.dart';

// ignore: must_be_immutable
class AppTextFiledProfile extends StatelessWidget {
  final TextInputType textInputType;
  final String labelText;
  final int? maxLength;
  final TextEditingController controller;
  final bool obscureText;
  final bool readOnly;
  final bool showCursor;
  void Function()? functionSuffixPressed;
  final IconData? suffix;
  final IconData? prefix;
  Function()? onTap;
  AppTextFiledProfile({
    this.textInputType = TextInputType.text,
    required this.labelText,
    this.maxLength,
    this.suffix = null,
    this.prefix = null,
    this.obscureText = false,
    this.readOnly = false,
    this.showCursor = true,
    required this.controller,
    this.onTap,
    this.functionSuffixPressed
  });

  @override
  Widget build(BuildContext context) {
    return Container(
      clipBehavior: Clip.antiAlias,
      decoration: BoxDecoration(
          color: Colors.white,
          borderRadius: BorderRadius.circular(10)
      ),
      child: TextFormField(
        controller: controller,
        maxLength: maxLength,
        showCursor: showCursor,
        textDirection: TextDirection.rtl,
        cursorColor: Color(0xff1DB854),
        autocorrect: true,
        enableSuggestions: true,
        readOnly: readOnly,
        keyboardType: textInputType,
        obscureText: obscureText,
        onTap: onTap,
        decoration: InputDecoration(
          labelText: labelText,
          labelStyle: TextStyle(color: Color(0xff8E8E93)),
          counterText: '',
          enabledBorder: borderEnable,
          focusedBorder: borderFocused,
          suffixIcon: suffix != null ? IconButton(onPressed: functionSuffixPressed, icon: Icon(suffix, color: Color(0xff1DB854),)) : null,
          prefixIcon: Icon(prefix, color: Colors.black,),
        ),
      ),
    );
  }
  OutlineInputBorder get borderEnable =>
      OutlineInputBorder(
        borderRadius: BorderRadius.circular(SizeConfig().scaleWidth(10),),
        borderSide: BorderSide(
          color: Color(0xff1DB854),
          width: SizeConfig().scaleWidth(2),
        ),
      );
  OutlineInputBorder get borderFocused =>
      OutlineInputBorder(
        borderRadius: BorderRadius.circular(SizeConfig().scaleWidth(10),),
        borderSide: BorderSide(
          color: Color(0xff1DB854),
          width: SizeConfig().scaleWidth(2),
        ),
      );
}