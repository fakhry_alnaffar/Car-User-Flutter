import 'package:car_user_project/models/ads.dart';
import 'package:car_user_project/models/car.dart';
import 'package:car_user_project/models/cart.dart';
import 'package:car_user_project/models/chat.dart';
import 'package:car_user_project/models/favorite.dart';
import 'package:car_user_project/models/merchant.dart';
import 'package:car_user_project/models/room.dart';
import 'package:car_user_project/models/user.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';

class FirebaseFirestoreController{
  FirebaseFirestore _firebaseFirestore = FirebaseFirestore.instance;

  Future<bool> addUser(Users user) async {
    return await _firebaseFirestore
        .collection('user')
        .add(user.toMap())
        .then((value) {
      return true;
    }).catchError((error) {
      print(error);
      return false;
    });
  }
  Future<String> getBlockStateUser2(String email) async {
    return await _firebaseFirestore.collection('user').where('email', isEqualTo: email).get().then((value) {if(value.docs.length > 0){return value.docs[0].get('isBlock');}else{return 'للاسف، تم حظر حسابك';}});
  }
  Future<bool> addManger(String path, id) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'isManger': '1', 'merchant' : id})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> removeManger(String path) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'isManger': '0'})
        .then((value) => true)
        .catchError((error) => false);
  }
  Future<String> getMarchentId() async {
    return _firebaseFirestore
        .collection('merchant')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].id);
  }
  Future<String> getMerchantDate() async{
    return _firebaseFirestore
        .collection('merchant')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get().then((value) => value.docs[0].get('date'));
  }

  Future<String> getUserType2(String email) async{
    return _firebaseFirestore
        .collection('user')
        .where('email', isEqualTo: email)
        .get().then((value) {if(value.docs.length > 0){return 'user';}else{return 'merchant';}});
  }
  Future<String> getName() async {
    return _firebaseFirestore
        .collection('user')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('name'));
  }
  Future<String> getMarchentName() async {
    return _firebaseFirestore
        .collection('merchant')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('name'));
  }
  Future<bool> updateUser({required String? path, required Users user}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update(user.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateUserName({required String? path, required String name}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'name' : name})
        .then((value) => true)
        .catchError((error) => false);
  }
  Future<bool> updateUserImage({required String? path, required String image}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'image' : image})
        .then((value) => true)
        .catchError((error) => false);
  }
  Future<bool> updateUserPhone({required String? path, required String phone}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'phoneNumber' : phone})
        .then((value) => true)
        .catchError((error) => false);
  }
  Future<bool> updateUserPlace({required String? path, required String place}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .update({'place' : place})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteUser({required String path, required Users user}) async {
    return await _firebaseFirestore
        .collection('user')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<List<String>> getUser() async {
    return await _firebaseFirestore
        .collection('user')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) {String name = value.docs[0].get('name'); String image = value.docs[0].get('image'); List<String> data = <String>[name, image]; return data; });
  }

  Future<String> getUserType() async{
    return _firebaseFirestore
        .collection('user')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get().then((value) {if(value.docs.length > 0){return 'user';}else{return 'merchant';}});
  }


  Stream<QuerySnapshot> readUser(String collection) async*{
    yield* _firebaseFirestore.collection(collection).where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).snapshots();
  }

  // ***************************************************************************************** //

  Future<bool> addMerchant(Merchant merchant) async {
    return await _firebaseFirestore
        .collection('merchant')
        .add(merchant.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readMerchant() async*{
    yield* _firebaseFirestore.collection('merchant').snapshots();
  }

  Stream<QuerySnapshot> readAllMerchant() async*{
    yield* _firebaseFirestore.collection('merchant').snapshots();
  }

  Stream<QuerySnapshot> readAllUser() async*{
    yield* _firebaseFirestore.collection('user').snapshots();
  }
  Stream<QuerySnapshot> readManger() async* {
    yield* _firebaseFirestore
        .collection('user')
        .where('isManger', isEqualTo: '1').where('merchant', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .snapshots();
  }
  Future<bool> isManager() async {
    return _firebaseFirestore
        .collection('user')
        .where('isManger', isEqualTo: '1')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs.length > 0 ? true : false);
  }

  Future<String> getMId() async {
    return _firebaseFirestore
        .collection('user')
        .where('isManger', isEqualTo: '1')
        .where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email)
        .get()
        .then((value) => value.docs[0].get('merchant'));
  }

  Future<String> getMerchantImage(String id) async {
    return await _firebaseFirestore.collection('merchant').where(FieldPath.documentId, isEqualTo: id).get().then((value) => value.docs[0].get('image'));
  }
  Future<List<DocumentSnapshot>> getOldAds() async{
    List<DocumentSnapshot> list = <DocumentSnapshot>[];
    return await _firebaseFirestore.collection('ads').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).get().then((value) {for(int i = 0; i < value.docs.length; i++){
      if(DateTime.now().isAfter(DateTime.parse(value.docs[0].get('end')))){
        print('old');
        list.add(value.docs[i]);
      }
    }
    return list;
    });
  }
  Future<List<DocumentSnapshot>> getNewAds() async{
    List<DocumentSnapshot> list = <DocumentSnapshot>[];
    return await _firebaseFirestore.collection('ads').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).get().then((value) {for(int i = 0; i < value.docs.length; i++){
      if(DateTime.now().isBefore(DateTime.parse(value.docs[0].get('end')))){
        print('new');
        list.add(value.docs[i]);
      }
    }
    return list;
    });
  }
  Future<DocumentSnapshot> getMerchantSub() async{
    return await _firebaseFirestore.collection('merchant').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).get().then((value) => value.docs[0]);
  }

  Future<String> getMerchantPrice() async{
    return await _firebaseFirestore.collection('subscription').get().then((value) => value.docs[0].get('merchant'));
  }

  Future<String> getMerchantPrice2() async{
    return await _firebaseFirestore.collection('subscription').get().then((value) => value.docs[0].get('main'));
  }

  Future<String> getMerchantPrice3() async{
    return await _firebaseFirestore.collection('subscription').get().then((value) => value.docs[0].get('user'));
  }

  Future<int> getMerchantCount() async{
    return await _firebaseFirestore.collection('user').where('isManger', isEqualTo: '1').get().then((value) => value.docs.length);
  }

  Future<int> getAdsCount() async{
    return await _firebaseFirestore.collection('ads').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).get().then((value) => value.docs.length);
  }

  Future<DocumentSnapshot> getUserData2(String type, String email) async{
    return await _firebaseFirestore.collection(type).where('email', isEqualTo: email).get().then((value) => value.docs[0]);
  }
  Stream<QuerySnapshot> readCar2() async*{
    yield* _firebaseFirestore.collection('car').snapshots();
  }
  Future<List<String>> getFavoriteId2(String id) async {
    List<String> list = <String>[];
    return await _firebaseFirestore.collection('favorite').where('carId', isEqualTo: id).get().then((value) {for(int i = 0; i < value.docs.length; i++){
      list.add(value.docs[i].id);
    }
    return list;
    });
  }

  Future<List<String>> getCartId2(String id) async {
    List<String> list = <String>[];
    return await _firebaseFirestore.collection('cart').where('carId', isEqualTo: id).get().then((value) {for(int i = 0; i < value.docs.length; i++){
      list.add(value.docs[i].id);
    }
    return list;
    });
  }

  Stream<QuerySnapshot> readUserToManger() async* {
    yield* _firebaseFirestore
        .collection('user')
        .where('isManger', isEqualTo: '0')
        .snapshots();
  }

  Future<bool> updateMerchant({required String? path, required Merchant merchant}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update(merchant.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateMerchantName({required String? path, required String name}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({'name' : name})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateMerchantImage({required String? path, required String image}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({'image' : image})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateMerchantMarketName({required String? path, required String marketName}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({'marketName' : marketName})
        .then((value) => true)
        .catchError((error) => false);
  }
  Future<String> getUserAgreement() async {
    return _firebaseFirestore
        .collection('using')
        .get()
        .then((value) => value.docs[0].get('using'));
  }
  Future<String> aboutApp() async {
    return _firebaseFirestore
        .collection('about')
        .get()
        .then((value) => value.docs[0].get('aboutApp'));
  }
  Future<String> adders() async {
    return _firebaseFirestore
        .collection('about')
        .get()
        .then((value) => value.docs[0].get('address'));
  }
  Future<String> email() async {
    return _firebaseFirestore
        .collection('about')
        .get()
        .then((value) => value.docs[0].get('email'));
  }
  Future<String> phone() async {
    return _firebaseFirestore
        .collection('about')
        .get()
        .then((value) => value.docs[0].get('phone'));
  }
  Future<bool> updateMerchantPlace({required String? path, required String place}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({'place' : place})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateMerchantAddress({required String? path, required String address}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({'address' : address})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateMerchantEmail({required String? path, required String email}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({'email' : email})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateMerchantPhone({required String? path, required String phone}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .update({'phoneNumber' : phone})
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteMerchant({required String path, required Merchant merchant}) async {
    return await _firebaseFirestore
        .collection('merchant')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<String> getMerchantState(String email) async {
    return await _firebaseFirestore.collection('merchant').where('email', isEqualTo: email).get().then((value) {if(value.docs.length > 0){return value.docs[0].get('accept');}else{return 'للاسف، تم رفض طلبك';}});
  }
  Future<String> getBlockStateMerchant(String email) async {
    return await _firebaseFirestore.collection('merchant').where('email', isEqualTo: email).get().then((value) {if(value.docs.length > 0){return value.docs[0].get('isBlock');}else{return 'للاسف، تم حظر حسابك';}});
  }
  Future<String> getBlockStateUser(String email) async {
    return await _firebaseFirestore.collection('user').where('email', isEqualTo: email).get().then((value) {if(value.docs.length > 0){return value.docs[0].get('isBlock');}else{return 'للاسف، تم حظر حسابك';}});
  }

  Future<String> getActive() async {
    return await _firebaseFirestore
        .collection('active')
        .get()
        .then((value) => value.docs[0].get('active_app'));
  }

  Future<bool> getUserState(String email) async {
    return await _firebaseFirestore.collection('user').where('email', isEqualTo: email).get().then((value) {if(value.docs.length > 0){return true;}else{return false;}});
  }
  Stream<QuerySnapshot> readMerchantSouth() async*{
    yield* _firebaseFirestore.collection('merchant').where('place',isEqualTo: 'جنوب غزة').snapshots();
  }
  Stream<QuerySnapshot> readMerchantNorth() async*{
    yield* _firebaseFirestore.collection('merchant').where('place',isEqualTo: 'شمال غزة').snapshots();
  }
  Stream<QuerySnapshot> readMerchantWest() async*{
    yield* _firebaseFirestore.collection('merchant').where('place',isEqualTo: 'غرب غزة').snapshots();
  }  Stream<QuerySnapshot> readMerchantEast() async*{
    yield* _firebaseFirestore.collection('merchant').where('place',isEqualTo: 'شرق غزة').snapshots();
  }
  Future<bool> addAdminOrder(Merchant merchant) async {
    return await _firebaseFirestore
        .collection('admin order')
        .add(merchant.toMap())
        .then((value) {
      return true;
    }).catchError((error) {
      print(error);
      return false;
    });
  }

  Future<DocumentSnapshot> getMerchantData(String id) async{
    return await _firebaseFirestore.collection('merchant').where(FieldPath.documentId, isEqualTo: id).get().then((value) => value.docs[0]);
  }

  Future<String> getMerchantId(String email) async{
    return await _firebaseFirestore.collection('merchant').where('email', isEqualTo: email).get().then((value) => value.docs[0].id);
  }

  Future<DocumentSnapshot> getUserData(String type) async{
    return await _firebaseFirestore.collection(type).where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).get().then((value) => value.docs[0]);
  }

  Future<DocumentSnapshot> getAdminData() async{
    return await _firebaseFirestore.collection('admin').get().then((value) => value.docs[0]);
  }

  Future<bool> addCar(Car car) async {
    return await _firebaseFirestore
        .collection('car')
        .add(car.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> updateCar({required String? path, required Car car}) async {
    return await _firebaseFirestore
        .collection('car')
        .doc(path)
        .update(car.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteCar({required String path}) async {
    return await _firebaseFirestore
        .collection('car')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }
  Future<bool> deleteCarFav({required String path}) async {
    return await _firebaseFirestore
        .collection('favorite')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readCar() async*{
    yield* _firebaseFirestore.collection('car').orderBy('created', descending: true).snapshots();
  }

  Stream<QuerySnapshot> readMerchantCar(String id) async*{
    yield* _firebaseFirestore.collection('car').where('merchant', isEqualTo: id).snapshots();
  }

  Stream<QuerySnapshot> readCustomCar(String id) async*{
    yield* _firebaseFirestore.collection('car').where('merchant', isEqualTo: id).snapshots();
  }

  Future<bool> addCart(Cart cart) async {
    return await _firebaseFirestore
        .collection('cart')
        .add(cart.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteCart({required String path}) async {
    return await _firebaseFirestore
        .collection('cart')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> addFavorite(Favorite car) async {
    return await _firebaseFirestore
        .collection('favorite')
        .add(car.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteFavorite({required String path}) async {
    return await _firebaseFirestore
        .collection('favorite')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> isFavorite(String id) async{
    return await _firebaseFirestore.collection('favorite').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).where('carId', isEqualTo: id).get().then((value) => value.docs.length > 0);
  }

  Future<String> getFavoriteId(String id) async {
    return await _firebaseFirestore.collection('favorite').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).where('carId', isEqualTo: id).get().then((value) => value.docs[0].id);
  }

  Stream<QuerySnapshot> readFavorite() async*{
    yield* _firebaseFirestore.collection('favorite').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).snapshots();
  }

  Future<bool> addAds(Ads ads) async {
    return await _firebaseFirestore
        .collection('ads')
        .add(ads.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> deleteAds({required String path}) async {
    return await _firebaseFirestore
        .collection('ads')
        .doc(path)
        .delete()
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> readAds() async*{
    yield* _firebaseFirestore.collection('ads').where('email', isEqualTo: FirebaseAuth.instance.currentUser!.email).snapshots();
  }

  Stream<QuerySnapshot> readOrder(String merchant) async*{
    yield* _firebaseFirestore.collection('cart').where('merchant', isEqualTo: merchant).snapshots();
  }

  Stream<QuerySnapshot> readChat(String between, String between2) async*{
    yield* _firebaseFirestore
        .collection('chat')
        .where('between', whereIn: [between, between2])
        .orderBy('created', descending: true)
        .snapshots();
  }

  Stream<QuerySnapshot> readChat2(String path) async*{
    yield* _firebaseFirestore.collection('room').doc(path).collection('chat').orderBy('created', descending: true).snapshots();
  }

  Stream<QuerySnapshot> readRoom() async*{
    yield* _firebaseFirestore.collection('room').snapshots();
  }
  Future<bool> addChat(Chat chat) async {
    return await _firebaseFirestore
        .collection('chat')
        .add(chat.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> addRoom(Room room) async {
    return await _firebaseFirestore
        .collection('room')
        .add(room.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Future<bool> addChat2(Chat chat, String roomId) async {
    return await _firebaseFirestore
        .collection('room')
        .doc(roomId)
        .collection('chat')
        .add(chat.toMap())
        .then((value) => true)
        .catchError((error) => false);
  }

  Stream<QuerySnapshot> searchCar(
      {String? name,
        String? merchant,
        String? state,
        String? model,
        String? color,
        double? price,
        double? price2
      }) async*{
    yield* _firebaseFirestore.collection('car').where('name', isEqualTo: name).where('state', isEqualTo: state).where('merchant', isEqualTo: merchant).where('model', isEqualTo: model).where('price', isEqualTo: price).snapshots();
  }

  Future<List<Ads>> getAllAds() async{
    List<Ads> list = <Ads>[];
    return _firebaseFirestore.collection('ads').get().then((value) {
      for(int i = 0; i < value.docs.length; i++){
        if(DateTime.now().isBefore(DateTime.parse(value.docs[0].get('end')))){
          Ads ads = Ads();
          ads.image =  value.docs[i].get('image');
          ads.name =  value.docs[i].get('name');
          ads.description =  value.docs[i].get('description');
          ads.email = value.docs[i].get('email');
          list.add(ads);
        }
      }
      return list;
    });
  }

  Stream<QuerySnapshot> search(String name) async* {
    yield* _firebaseFirestore.collection('car').where('name', isEqualTo: name).where('marketName', isEqualTo: name).snapshots();
  }

  Stream<QuerySnapshot> searchByName(String name) async* {
    yield* _firebaseFirestore.collection('car').where('name', isEqualTo: name).snapshots();
  }

  // Future<String> getPrice(String type) async {
  //   return await _firebaseFirestore.collection('subscription').get().then((value) => value.docs[0].get(type));
  // }
  Future<String> getPrice() async {
    return _firebaseFirestore
        .collection('subscription')
        .get()
        .then((value) => value.docs[0].get('main'));
  }

  Stream<QuerySnapshot> readCarName() async*{
    yield* _firebaseFirestore.collection('car').snapshots();
  }
}