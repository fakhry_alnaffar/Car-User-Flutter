import 'package:car_user_project/utils/helpers.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter/cupertino.dart';

class FirebaseAuthController with Helpers{
  FirebaseAuth _firebaseAuth = FirebaseAuth.instance;

  Future<bool> createAccount(BuildContext context, {required String email, required String password}) async {
    try {
      UserCredential userCredential = await _firebaseAuth.createUserWithEmailAndPassword(email: email, password: password);
      await _controlEmailValidation(context, credential: userCredential);
      return true;
    } on FirebaseException catch (e) {
      _controllerErrorCode(context, e);
    } catch (e) {
      print('Exception :  $e');
    }
    return false;
  }

  Future<bool> signIn(BuildContext context, {required String email, required String password}) async {
    try {
      UserCredential userCredential = await _firebaseAuth
          .signInWithEmailAndPassword(email: email, password: password);
      return await _controlEmailValidation(context, credential: userCredential);
    } on FirebaseAuthException catch (e) {
      _controllerErrorCode(context, e);
    } catch (e) {
      //
      print('Exception : $e');
    }
    return false;
  }

  Future<bool> updatePassword(BuildContext context,{String? password}) async {
    // if (password != null && password.isNotEmpty) {
    try {
      await _firebaseAuth.currentUser!
          .updatePassword(password!);
      return true;
    } on FirebaseAuthException catch (e) {

      _controllerErrorCode(context, e);
      return false;
    } catch (e) {
      print(e);
      return false;
    }
    // }
    // return true;
  }

  bool get isLoggedIn => _firebaseAuth.currentUser != null;

  Future<bool> _controlEmailValidation(BuildContext context,
      {required UserCredential credential}) async {
    if (!credential.user!.emailVerified) {
      await credential.user!.sendEmailVerification();
      await _firebaseAuth.signOut();
      showSnackBar(
          context: context,
          message: 'تم ارسال رسالة تاكيد الايميل');
      return false;
    }
    return true;
  }

  void _controllerErrorCode(
      BuildContext context, FirebaseException authException) {
    showSnackBar(
        context: context, message: authException.message ?? '', error: true);
    switch (authException.code) {
      case 'email-already-in-use':
        break;
      case 'invalid-email':
        break;
      case 'operation-not-allowed':
        break;
      case 'weak-password':
        break;
      case 'user-not-found':
        break;
      case 'requires-recent-login':
        break;
    }
  }

}