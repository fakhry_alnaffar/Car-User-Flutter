class Room{
  String path = '';
  String name = '';
  String email = '';
  String image = '';
  String reseverImage = '';
  String reseverName = '';
  String reseverEmail = '';
  // String type = '';

  Room();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['name'] = name;
    map['email'] = email;
    map['image'] = image;
    map['reseverImage'] = reseverImage;
    map['reseverName'] = reseverName;
    map['reseverEmail'] = reseverEmail;
    return map;
  }
}