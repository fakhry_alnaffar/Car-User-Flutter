class Chat{
  String path = '';
  String senderName = '';
  String receverName = '';
  String sender = '';
  String recever = '';
  String senderImage = '';
  String receverImage = '';
  String message = '';
  String between = '';
  String created = DateTime.now().toString();

  Chat();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['senderName'] = senderName;
    map['receverName'] = receverName;
    map['sender'] = sender;
    map['recever'] = recever;
    map['between'] = between;
    map['senderImage'] = senderImage;
    map['receverImage'] = receverImage;
    map['message'] = message;
    map['created'] = created;
    return map;
  }
}