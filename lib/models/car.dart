import 'package:car_user_project/firebase/firebase_auth_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Car{
  List<String> images = <String>[];
  String name = '';
  String path = '';
  String model = '';
  List<String> color = <String>[];
  double price = 0;
  String state = '';
  String description = '';
  String merchant = '';
  String created = DateTime.now().toString();

  Car();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['images'] = images;
    map['name'] = name;
    map['model'] = model;
    map['color'] = color;
    map['price'] = price;
    map['state'] = state;
    map['created'] = created;
    map['merchant'] = merchant;
    map['description'] = description;
    return map;
  }

}