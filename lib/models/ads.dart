import 'package:firebase_auth/firebase_auth.dart';

class Ads {
  String name = '';
  String path = '';
  String image = '';
  String description = '';
  String price = '';
  String time = '';
  String email = FirebaseAuth.instance.currentUser!.email!;
  String created = DateTime.now().toString().substring(0, 10);
  String end = DateTime.now().toString();

  Ads();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['image'] = image;
    map['name'] = name;
    map['price'] = price;
    map['created'] = created;
    map['email'] = email;
    map['time'] = time;
    map['description'] = description;
    map['end'] = end;
    return map;
  }
}
