class Merchant{
  String path = '';
  String name = '';
  String marketName = '';
  String phoneNumber = '';
  String place = '';
  String address = '';
  String email = '';
  String image = '';
  String accept = '0';
  String roomId = '0';
  String isBlock = '0';
  String type = '';

  Merchant();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['phoneNumber'] = phoneNumber;
    map['place'] = place;
    map['name'] = name;
    map['marketName'] = marketName;
    map['address'] = address;
    map['roomId'] = roomId;
    map['email'] = email;
    map['image'] = image;
    map['accept'] = accept;
    map['isBlock'] = isBlock;
    return map;
  }

}