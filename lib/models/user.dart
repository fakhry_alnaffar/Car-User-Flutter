class Users{
  String path = '';
  String name = '';
  String phoneNumber = '';
  String place = '';
  String email = '';
  String image = '';
  String isAdmin = '0';
  String isManger = '0';
  String isBlock = '0';
  String roomId = '';
  // String type = '';

  Users();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['phoneNumber'] = phoneNumber;
    map['place'] = place;
    map['name'] = name;
    map['email'] = email;
    map['image'] = image;
    map['roomId'] = roomId;
    map['isAdmin'] = isAdmin;
    map['isManger'] = isManger;
    map['isBlock'] = isBlock;
    return map;
  }
}