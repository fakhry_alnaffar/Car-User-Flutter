import 'package:car_user_project/firebase/firebase_auth_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Cart{
  List<String> images = <String>[];
  String carName = '';
  String carId = '';
  String path = '';
  String model = '';
  List<String> color = <String>[];
  double price = 0;
  String state = '';
  String description = '';
  String merchant = '';
  String created = DateTime.now().toString();

  String name = '';
  String phoneNumber = '';
  String place = '';
  String email = FirebaseAuth.instance.currentUser!.email.toString();
  String image = '';

  Cart();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['images'] = images;
    map['carName'] = carName;
    map['carId'] = carId;
    map['model'] = model;
    map['color'] = color;
    map['price'] = price;
    map['state'] = state;
    map['created'] = created;
    map['merchant'] = merchant;
    map['description'] = description;

    map['phoneNumber'] = phoneNumber;
    map['place'] = place;
    map['name'] = name;
    map['email'] = email;
    map['image'] = image;
    return map;
  }

}