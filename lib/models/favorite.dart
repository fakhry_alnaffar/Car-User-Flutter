import 'package:car_user_project/firebase/firebase_auth_controller.dart';
import 'package:firebase_auth/firebase_auth.dart';

class Favorite{
  List<String> images = <String>[];
  String name = '';
  String carId = '';
  String model = '';
  List<String> color = <String>[];
  double price = 0;
  String state = '';
  String description = '';
  String email = FirebaseAuth.instance.currentUser!.email.toString();
  String merchant = '';
  String created = '';

  Favorite();

  Map<String, dynamic> toMap() {
    Map<String, dynamic> map = Map<String, dynamic>();
    map['images'] = images;
    map['name'] = name;
    map['model'] = model;
    map['color'] = color;
    map['price'] = price;
    map['state'] = state;
    map['created'] = created;
    map['email'] = email;
    map['carId'] = carId;
    map['merchant'] = merchant;
    map['description'] = description;
    return map;
  }

}